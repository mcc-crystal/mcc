#include "../src/energy/EmeraldEnergy.hpp"
#include "../src/peak_generation/AllowedPeakGenerator.hpp"
#include "../src/peak_list/DynamicCalculatedList.hpp"
#include "../src/peak_list/DynamicObservedList.hpp"
#include "../src/core/DTwoThetaConverter.hpp"

#include <vector>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>



class EmeraldEnergyFixture
{
public:
  EmeraldEnergyFixture()
  {
    peakGenerator_ = new mcc::AllowedPeakGenerator(globalSettings_);
    calculatedList_ = new mcc::DynamicCalculatedList(globalSettings_);
    observedList_ = new mcc::DynamicObservedList(globalSettings_);
    emeraldEnergy_ = new mcc::EmeraldEnergy(globalSettings_,
                                            *calculatedList_,
                                            *observedList_);
  }

  ~EmeraldEnergyFixture()
  {
    delete emeraldEnergy_;
    delete observedList_;
    delete calculatedList_;
  }

protected:
  mcc::GlobalSettings globalSettings_;
  mcc::EmeraldEnergy* emeraldEnergy_;
  mcc::PeakGenerator* peakGenerator_;
  mcc::ObservedPeakList* observedList_;
  mcc::CalculatedPeakList* calculatedList_;
  mcc::UnitCell* unitCell_; // NOT DELETED IN DESTRUCTOR!
};



BOOST_FIXTURE_TEST_SUITE(EmeraldEnergyTests, EmeraldEnergyFixture)



BOOST_AUTO_TEST_CASE(ModuleName)
{
  BOOST_CHECK_EQUAL(emeraldEnergy_->getModuleName(), "EmeraldEnergy");
}



BOOST_AUTO_TEST_CASE(HexagonalTest)
{
  /* La₂O₃
   * a = 3.9373
   * c = 6.1299
   */

   unitCell_ = new mcc::HexagonalCell;
   unitCell_->a = 3.9373;
   unitCell_->c = 6.1299;

   mcc::DTwoThetaConverter::setWavelength(1.5406);
   globalSettings_.peakGeneration_twoThetaMax = 150.0;
   peakGenerator_->setUnitCell(unitCell_);

   std::vector<double> observedPeaks = {
      26.1103, 29.1301, 29.9602, 39.5270, 46.0836, 52.1321, 53.7155, 55.4392,
      55.9532, 60.3700, 62.2580, 66.8695, 72.0941, 73.3935, 75.3018, 79.1550,
      80.8479, 83.7646, 85.0515, 85.3199, 89.9202, 92.5598, 97.8236, 101.4280,
      103.0362, 103.8203, 109.0434, 110.5499, 111.0290, 115.0419, 116.2628,
      120.2558, 122.9750, 127.6494, 130.5564, 131.6586, 136.8550, 146.2221,
      148.3118 };
  observedList_->copyPeakPositions(observedPeaks);

  // Exact cell test
  peakGenerator_->populatePeakList(*calculatedList_);
  calculatedList_->sortPeaks();
  calculatedList_->removeDuplicates();

  double energy = emeraldEnergy_->calculateEnergy();
  BOOST_CHECK_LT(energy, 0.0);

  // Worse cell test
  unitCell_->a = 3.9500;

  peakGenerator_->populatePeakList(*calculatedList_);
  calculatedList_->sortPeaks();
  calculatedList_->removeDuplicates();

  double newEnergy1 = emeraldEnergy_->calculateEnergy();
  BOOST_CHECK_LT(energy, newEnergy1);

  // Even worse cell test
  unitCell_->a = 4.0000;
  unitCell_->c = 6.0000;

  peakGenerator_->populatePeakList(*calculatedList_);
  calculatedList_->sortPeaks();
  calculatedList_->removeDuplicates();

  double newEnergy2 = emeraldEnergy_->calculateEnergy();
  BOOST_CHECK_LT(energy, newEnergy2);

  delete unitCell_;
}



BOOST_AUTO_TEST_SUITE_END()
