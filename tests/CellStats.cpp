#include <vector>

#include "../src/core/CellStats.hpp"
#include "../src/peak_generation/AllowedPeakGenerator.hpp"
#include "../src/peak_list/DynamicCalculatedList.hpp"
#include "../src/peak_list/DynamicObservedList.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>



class CellStatsFixture
{
public:
  CellStatsFixture()
  {
    globalSettings_.main_twoThetaTolerance = 0.10;

    peakGenerator_ = new mcc::AllowedPeakGenerator{globalSettings_};
    calculatedList_ = new mcc::DynamicCalculatedList{globalSettings_};
    observedList_ = new mcc::DynamicObservedList{globalSettings_};
    cellStats_ = new mcc::CellStats{globalSettings_,
                                    *calculatedList_,
                                    *observedList_};
  }

  ~CellStatsFixture()
  {
    delete peakGenerator_;
    delete calculatedList_;
    delete observedList_;
    delete cellStats_;
  }

protected:
  mcc::GlobalSettings globalSettings_;
  mcc::PeakGenerator* peakGenerator_;
  mcc::CalculatedPeakList* calculatedList_;
  mcc::ObservedPeakList* observedList_;
  mcc::CellStats* cellStats_;
  mcc::UnitCell* unitCell_; // NOT DELETED IN DESTRUCTOR!
};



BOOST_FIXTURE_TEST_SUITE(CellStatsTests, CellStatsFixture)



BOOST_AUTO_TEST_CASE(ModuleName)
{
  BOOST_CHECK_EQUAL(cellStats_->getModuleName(), "CellStats");
}



/* Based on real Pt data. Calculated positions are completely artificial.
 * One peak is missing and one is too far from observed counterpart.
 */
BOOST_AUTO_TEST_CASE(CellStatsCase1)
{
  std::vector<double> observedTestPositions = {39.7, 46.2, 67.5, 81.3, 85.7};
  std::vector<double> calculatedTestPositions = {39.7, 69.0, 81.31, 85.68};
  calculatedList_->copyPeakPositions(calculatedTestPositions);
  observedList_->copyPeakPositions(observedTestPositions);

  const int trueNIndexed {3};
  const int trueNUnindexed {2};
  const int trueNPossible {4};

  cellStats_->calculateStats();
  BOOST_CHECK_EQUAL(cellStats_->getNIndexedPeaks(), trueNIndexed);
  BOOST_CHECK_EQUAL(cellStats_->getNUnindexedPeaks(), trueNUnindexed);
  BOOST_CHECK_EQUAL(cellStats_->getNPossiblePeaks(), trueNPossible);
  BOOST_CHECK_EQUAL(cellStats_->calculateNPossiblePeaks(), trueNPossible);

  // Sum of indexed and unindexed peaks should be number of total peaks
  int nPeaks {observedList_->getPeakCount()};
  int sum {cellStats_->getNIndexedPeaks() + cellStats_->getNUnindexedPeaks()};
  BOOST_CHECK_EQUAL(sum, nPeaks);
}


/* Real Na₅P₃O₁₀ data. Artificial peak added at the end and should be ignored.
 * Two calculated peaks are messed up.
 */
BOOST_AUTO_TEST_CASE(CellStatsCase2)
{
  std::vector<double> observedTestPositions =
     { 9.6055, 18.5469, 18.9878, 19.2794, 19.8457, 20.7854, 21.8190, 21.9280,
      23.2046, 24.5712, 27.9463, 28.4932, 29.0622, 29.9499, 31.7729, 32.4719,
      32.6546, 33.0515, 33.3812, 33.4969, 33.9282, 34.3574, 34.8117, 35.0505,
      35.8761, 36.3268, 36.5719, 37.2316, 38.2173, 38.4550, 38.8173, 39.1334,
      39.9473, 40.3014, 41.4033, 41.6231, 41.8250, 42.0905, 42.2556};
  std::vector<double> calculatedTestPositions =
     { 9.6055, 18.5469, 18.9878, 19.2794, 19.2800, 20.7854, 21.8190, 21.9280,
      23.2046, 24.5712, 27.9463, 28.4932, 29.0622, 29.9499, 31.7729, 32.4719,
      32.6546, 33.0515, 33.3812, 33.4969, 33.9282, 34.3574, 34.8117, 35.0505,
      35.8761, 36.3268, 36.5719, 37.2316, 38.2173, 38.4550, 38.8173, 39.1334,
      39.9473, 40.3014, 41.4033, 41.6231, 41.8250, 42.0905, 42.1111, 45.0000};
  calculatedList_->copyPeakPositions(calculatedTestPositions);
  observedList_->copyPeakPositions(observedTestPositions);

  const int trueNIndexed {37};
  const int trueNUnindexed {2};
  const int trueNPossible {39};

  cellStats_->calculateStats();
  BOOST_CHECK_EQUAL(cellStats_->getNIndexedPeaks(), trueNIndexed);
  BOOST_CHECK_EQUAL(cellStats_->getNUnindexedPeaks(), trueNUnindexed);
  BOOST_CHECK_EQUAL(cellStats_->getNPossiblePeaks(), trueNPossible);
  BOOST_CHECK_EQUAL(cellStats_->calculateNPossiblePeaks(), trueNPossible);

  // Sum of indexed and unindexed peaks should be number of total peaks
  int nPeaks {observedList_->getPeakCount()};
  int sum {cellStats_->getNIndexedPeaks() + cellStats_->getNUnindexedPeaks()};
  BOOST_CHECK_EQUAL(sum, nPeaks);
}



BOOST_AUTO_TEST_SUITE_END()
