#include "../src/peak_generation/AllowedPeakGenerator.hpp"

#include <cmath>
#include <iostream>
#include <iomanip>

#include "../src/peak_list/DynamicObservedList.hpp"
#include "../src/core/DTwoThetaConverter.hpp"
#include "../src/peak_list/DynamicCalculatedList.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>



class AllowedPeakGeneratorFixture
{
public:
  AllowedPeakGeneratorFixture() :
                    peakGenerator_{settings_}
  {
    calculatedList_ = new mcc::DynamicCalculatedList{settings_};
  }

  ~AllowedPeakGeneratorFixture()
  {
    delete calculatedList_;
  }

protected:
  mcc::GlobalSettings settings_;
  mcc::AllowedPeakGenerator peakGenerator_;
  mcc::CalculatedPeakList* calculatedList_;
  mcc::UnitCell* cell_;

  void executeIndexingTest(const std::vector<double>& observedPositions,
                           const mcc::UnitCell* unitCell,
                           double wavelength,
                           double tolerance,
                           bool consoleOutput = false);
};



void AllowedPeakGeneratorFixture::executeIndexingTest(
                           const std::vector<double>& observedPositions,
                           const mcc::UnitCell* unitCell,
                           double wavelength,
                           double tolerance,
                           bool consoleOutput)
{
  mcc::DTwoThetaConverter::setWavelength(wavelength);
  peakGenerator_.setUnitCell(unitCell);
  peakGenerator_.setMaxTwoTheta(observedPositions.back() + tolerance);

  peakGenerator_.populatePeakList(*calculatedList_);
  calculatedList_->sortPeaks();
  calculatedList_->removeDuplicates();
  int nIndexed {0};

  for (double observed : observedPositions) {
    int iCalculated {calculatedList_->findClosestPeak(observed)};
    const mcc::Reflection& r {calculatedList_->getReflection(iCalculated)};

    double difference {std::fabs(r.twoTheta - observed)};
    if (difference <= tolerance)
      nIndexed++;

    if (consoleOutput == true) {
      std::cout << std::fixed;
      std::cout << std::setw(4) << r.h << std::setw(4) << r.k << std::setw(4) <<
                   r.l << ": " <<  std::setw(6) << std::setprecision(2) <<
                   r.twoTheta << " --> " << std::setw(6) <<
                   std::setprecision(2) << observed << "     delta: " <<
                   std::setw(6) << std::setprecision(3) << difference <<
                   std::endl;
    }
  }

  BOOST_CHECK_EQUAL(nIndexed, observedPositions.size());
}



BOOST_FIXTURE_TEST_SUITE(AllowedPeakGeneratorTests,
                         AllowedPeakGeneratorFixture);



BOOST_AUTO_TEST_CASE(ModuleName)
{
  BOOST_CHECK_EQUAL(peakGenerator_.getModuleName(), "AllowedPeakGenerator");
}



BOOST_AUTO_TEST_CASE(CubicTest)
{
  const double TOLERANCE {0.1};
  const double WAVELENGTH {1.5406};

  // Real Pt data.
  std::vector<double> observedTestPositions =
     { 39.7634,  46.2427,  67.4541,  81.2861,  85.7121, 103.5078, 117.7112,
      122.8070, 148.2614 };

  mcc::CubicCell cell;
  cell.a = 3.9231;

  executeIndexingTest(observedTestPositions, &cell, WAVELENGTH, TOLERANCE);
}



BOOST_AUTO_TEST_CASE(HexagonalTest)
{
  const double TOLERANCE {0.11};
  const double WAVELENGTH {1.5406};

  // Real La₂O₃ data.
  std::vector<double> observedTestPositions =
     {  26.1103,  29.1301,  29.9602,  39.5270,  46.0836,  52.1321,  53.7155,
        55.4392,  55.9532,  60.3700,  62.2580,  66.8695,  72.0941,  73.3935,
        75.3018,  79.1550,  80.8479,  83.7646,  85.0515,  85.3199,  89.9202,
        92.5598,  97.8236, 101.4280, 103.0362, 103.8203, 109.0434, 110.5499,
       111.0290, 115.0419, 116.2628, 120.2558, 122.9750, 127.6494, 130.5564,
       131.6586, 136.8550, 146.2221, 148.3118 };


  mcc::HexagonalCell cell;
  cell.a = 3.9373;
  cell.c = 6.1299;

  executeIndexingTest(observedTestPositions, &cell, WAVELENGTH, TOLERANCE);
}



BOOST_AUTO_TEST_CASE(TetragonalTest)
{
  const double TOLERANCE {0.1};
  const double WAVELENGTH {1.5406};

  // Real La₂O₃ data.
  // Problematic peak 86.9627 removed
  std::vector<double> observedTestPositions =
     {  22.0382,  22.2620,  31.4971,  31.6459,  38.8871,  44.8552,  45.3767,
        50.6127,  50.9763,  51.0986,  55.9532,  56.2514,  65.7534,  66.1210,
        70.3570,  70.6605,  74.3341,  75.0921,  75.1618,  78.7655,  79.4698,
        83.4896,            87.2845,  88.0661,  91.5829,  92.0571,  92.3244,
        99.4911, 100.9802, 103.8651, 104.4981, 104.9872, 105.3579, 108.2523,
       108.9416, 109.7291, 113.5517, 114.3577, 117.5011 };

  mcc::TetragonalCell cell;
  cell.a = 3.994;
  cell.c = 4.038;

  executeIndexingTest(observedTestPositions, &cell, WAVELENGTH, TOLERANCE);
}



BOOST_AUTO_TEST_CASE(OrthorhombicTest)
{
  const double TOLERANCE {0.1};
  const double WAVELENGTH {1.5406};

  // Real MoO₃ data.
  std::vector<double> observedTestPositions =
     { 12.7634, 23.3281, 25.7038, 27.3344, 29.6950, 33.1270, 33.7308, 34.3710,
       35.4947, 38.5750, 38.9747, 39.6540, 42.3803, 45.4007, 45.7395, 46.2826,
       49.2393, 50.0481, 51.5630, 52.0363, 52.7799, 54.1272, 55.1859, 56.3641,
       57.6753, 58.0733, 58.8044, 61.6149, 62.8681, 64.5258, 64.9295, 66.7615,
       67.5258, 69.4633 };

  mcc::OrthorhombicCell cell;
  cell.a = 3.962;
  cell.b = 13.858;
  cell.c = 3.697;

  executeIndexingTest(observedTestPositions, &cell, WAVELENGTH, TOLERANCE);
}



BOOST_AUTO_TEST_CASE(MonoclinicTest)
{
  const double TOLERANCE {0.1};
  const double WAVELENGTH {1.5406};

  // Real Na₅P₃O₁₀ data.
  std::vector<double> observedTestPositions =
     { 9.6055, 18.5469, 18.9878, 19.2794, 19.8457, 20.7854, 21.8190, 21.9280,
      23.2046, 24.5712, 27.9463, 28.4932, 29.0622, 29.9499, 31.7729, 32.4719,
      32.6546, 33.0515, 33.3812, 33.4969, 33.9282, 34.3574, 34.8117, 35.0505,
      35.8761, 36.3268, 36.5719, 37.2316, 38.2173, 38.4550, 38.8173, 39.1334,
      39.9473, 40.3014, 41.4033, 41.6231, 41.8250, 42.0905, 42.2556};

  mcc::MonoclinicCell cell;
  cell.a = 18.55;
  cell.b = 5.363;
  cell.c = 9.643;
  cell.beta = 96.95;

  executeIndexingTest(observedTestPositions, &cell, WAVELENGTH, TOLERANCE);
}



BOOST_AUTO_TEST_CASE(TriclinicTest)
{
  const double TOLERANCE {0.1};
  const double WAVELENGTH {1.5406};

  // Real Pt data.
  std::vector<double> observedTestPositions =
     { 17.8320, 17.9411, 18.5861, 19.1117, 24.3661, 24.9919, 26.0326, 26.2671,
       27.8572, 28.2171, 28.4932, 29.7558, 32.0889, 33.9547, 34.1280, 34.5212,
       34.6312, 35.4367, 36.1000, 37.6851, 38.2173, 38.5922, 38.7478, 39.0099,
       39.4909, 39.9842, 40.5093, 41.0682, 41.3437, 41.6432, 42.8015, 43.1447,
       43.2313, 43.9828, 44.5066, 46.0836, 46.4330, 46.5339, 47.3572, 48.1841,
       48.4297, 49.1260, 49.3532, 49.5826, 49.9308, 50.4629, 51.3143, 51.4695,
       51.6570, 52.2282, 52.9114, 53.6136, 53.8863, 54.0581, 54.7219, 55.3666,
       55.7317, 58.3148, 58.4364, 59.2608, 60.0241, 60.2831, 60.9857, 61.1641,
       61.7059, 62.3045, 63.4911, 64.1769, 64.8280, 65.0823, 65.5972, 66.1738,
       66.5465, 67.1413, 68.3660, 68.5368, 68.7659 };


  mcc::TriclinicCell cell;
  cell.a = 6.860;
  cell.b = 8.787;
  cell.c = 6.527;
  cell.alpha = 91.02;
  cell.beta = 113.02;
  cell.gamma = 88.18;

  executeIndexingTest(observedTestPositions, &cell, WAVELENGTH, TOLERANCE);
}



BOOST_AUTO_TEST_SUITE_END();
