#include "../src/core/ArgumentParser.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>



BOOST_AUTO_TEST_SUITE(ArgumentParser);



/* Test with a minimal set of arguments, which should contain only command
 * name.
 */
BOOST_AUTO_TEST_CASE(NoArguments)
{
  const char* RUN_COMMAND {"mcc"};
  const int argc{1};
  const char* argv[] = {RUN_COMMAND};

  bool result;
  mcc::ArgumentParser args{argc, argv, result};
  // Parse should not fail
  BOOST_REQUIRE(result == true);

  // Command name
  BOOST_CHECK(args.getCommandName() == RUN_COMMAND);
  // Default values
  BOOST_CHECK(args.getWriteConfigurationFile() == false);
}



/* Test with all valid arguments. Parse results are checked.
 */
BOOST_AUTO_TEST_CASE(AllArguments)
{
  const char* RUN_COMMAND {"mcc"};
  const char* CONFIG_FILE {"alt_config.conf"};
  const int argc{3}; // DO NOT FORGET TO ADJUST AFTER CHANGING argv!
  const char* argv[] = {RUN_COMMAND, "-c", CONFIG_FILE};

  bool result;
  mcc::ArgumentParser args{argc, argv, result};
  // Parse should not fail
  BOOST_REQUIRE_EQUAL(result, true);

  // Command name
  BOOST_CHECK_EQUAL(args.getCommandName(), RUN_COMMAND);
  // Default values
  BOOST_CHECK_EQUAL(args.getWriteConfigurationFile(), true);
  BOOST_CHECK_EQUAL(args.getConfigurationFile(), CONFIG_FILE);
}



/* Test with one invalid argument. It should fail because of that.
 */
BOOST_AUTO_TEST_CASE(InvalidArguments)
{
  const char* RUN_COMMAND {"mcc"};
  const int argc{3}; // DO NOT FORGET TO ADJUST AFTER CHANGING argv!
  const char* argv[] = {RUN_COMMAND, "wtf", "more than I can take"};

  bool result;
  mcc::ArgumentParser args{argc, argv, result};
  // Parse has to fail
  BOOST_REQUIRE_EQUAL(result, false);
}



BOOST_AUTO_TEST_SUITE_END();