#include "../src/core/DTwoThetaConverter.hpp"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>



BOOST_AUTO_TEST_SUITE(DTwoThetaConverterTests)



BOOST_AUTO_TEST_CASE(DToTwoTheta)
{
  const double TOLERANCE {0.001};
  double result;

  mcc::DTwoThetaConverter::setWavelength(1.5406);
  result = mcc::DTwoThetaConverter::dToTwoTheta(7.88933);
  BOOST_CHECK_CLOSE(result, 11.2064, TOLERANCE);
  result = mcc::DTwoThetaConverter::dToTwoTheta(3.41436);
  BOOST_CHECK_CLOSE(result, 26.0770, TOLERANCE);

  mcc::DTwoThetaConverter::setWavelength(0.81870);
  result = mcc::DTwoThetaConverter::dToTwoTheta(9.4387);
  BOOST_CHECK_CLOSE(result, 4.9713, TOLERANCE);
  result = mcc::DTwoThetaConverter::dToTwoTheta(2.48463);
  BOOST_CHECK_CLOSE(result, 18.9657, TOLERANCE);
}



BOOST_AUTO_TEST_CASE(TwoThetaToD)
{
  const double TOLERANCE {0.90};
  double result;

  mcc::DTwoThetaConverter::setWavelength(1.5406);
  result = mcc::DTwoThetaConverter::twoThetaToD(11.2064);
  BOOST_CHECK_CLOSE(result, 7.88933, TOLERANCE);
  result = mcc::DTwoThetaConverter::dToTwoTheta(26.0770);
  BOOST_CHECK_CLOSE(result, 3.41436, TOLERANCE);

  mcc::DTwoThetaConverter::setWavelength(0.81870);
  result = mcc::DTwoThetaConverter::dToTwoTheta(4.9713);
  BOOST_CHECK_CLOSE(result, 9.4387, TOLERANCE);
  result = mcc::DTwoThetaConverter::dToTwoTheta(18.9657);
  BOOST_CHECK_CLOSE(result, 2.48463, TOLERANCE);
}



BOOST_AUTO_TEST_SUITE_END()
