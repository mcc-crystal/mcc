# MCC

MCC is a free powder pattern indexing program, relying on brute force to find
the correct unit cell. It uses Monte Carlo and grid search methods.

This page contains the source code. For documentation and pre-compiled binaries
visit the [MCC group](https://gitlab.com/mcc-crystal).

## Quick build instructions

### Dependencies

* [g++](https://gcc.gnu.org/) or [clang](https://clang.llvm.org/) C++ compiler
* [CMake](https://cmake.org/)
* [Boost](https://www.boost.org/) (for running unit tests only)

### Linux

Make a build directory and change into it:

    $ mkdir build
    $ cd build

Use CMake to generate build tree (this will generate forced 64-bit build
optimized for current CPU; options may be changed by using ``ccmake`` or CMake
graphical interface instead):

    $ cmake -DCMAKE_BUILD_TYPE=Release ..

Build the program:

    $ make

Executable `mcc` can be found in `build/src`.

It is also possible to use faster Ninja build system:

    $ cmake -G Ninja ..
    $ ninja