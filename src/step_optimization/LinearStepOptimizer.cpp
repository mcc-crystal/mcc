#include "LinearStepOptimizer.hpp"



namespace mcc
{
  // Static initialization
  const double LinearStepOptimizer::LENGTH_OPTIMIZATION_SCALER = 0.03;
  const double LinearStepOptimizer::ANGLE_OPTIMIZATION_SCALER = 0.06;



  LinearStepOptimizer::LinearStepOptimizer(const GlobalSettings& settings,
                                    const MonteCarloController& mcController) :
                       StepOptimizer{settings, mcController}
  { }



  void LinearStepOptimizer::performLengthStepOptimization(
                                       int& refAcceptedLengthSteps,
                                       int& refTotalLengthSteps,
                                       double& refLengthStep)
  {
    double acceptedRatio {static_cast<double>(refAcceptedLengthSteps) /
                          static_cast<double>(refTotalLengthSteps)};
    double difference {acceptedRatio - idealAcceptedRatio_};

    // Change computation
    double change {LENGTH_OPTIMIZATION_SCALER * difference};

    // Safety check: check for maximum change
    if (std::fabs(change) > MAXIMUM_LENGTH_STEP_CHANGE) {
      // reportWarning("Length step optimization change too big: " +
      //               std::to_string(change));
      if (change < 0)
        change = -MAXIMUM_LENGTH_STEP_CHANGE;
      else
        change = MAXIMUM_LENGTH_STEP_CHANGE;
    }

    refLengthStep += change;

    // Safety check: check for maximum value
    if (refLengthStep > MAXIMUM_LENGTH_STEP) {
      // reportWarning("Length step over maximum value: " +
      //               std::to_string(refLengthStep));
      refLengthStep = MAXIMUM_LENGTH_STEP;
    }
    // Safety check: check for minimum value
    else if (refLengthStep < MINIMUM_LENGTH_STEP) {
      // reportWarning("Length step under minimum value: " +
      //               std::to_string(refLengthStep));
      refLengthStep = MINIMUM_LENGTH_STEP;
    }

    // Reset step counts
    refAcceptedLengthSteps = 0;
    refTotalLengthSteps = 0;
  }



  void LinearStepOptimizer::performAngleStepOptimization(
                                      int& refAcceptedAngleSteps,
                                      int& refTotalAngleSteps,
                                      double& refAngleStep)
  {
    double acceptedRatio {static_cast<double>(refAcceptedAngleSteps) /
                          static_cast<double>(refTotalAngleSteps)};
    double difference {acceptedRatio - idealAcceptedRatio_};

    // Change computation
    double change {ANGLE_OPTIMIZATION_SCALER * difference};

    // Safety check: check for maximum change
    if (std::fabs(change) > MAXIMUM_ANGLE_STEP_CHANGE) {
      reportWarning("Angle step optimization change too big: " +
                    std::to_string(change));
      if (change < 0)
        change = -MAXIMUM_ANGLE_STEP_CHANGE;
      else
        change = MAXIMUM_ANGLE_STEP_CHANGE;
    }

    refAngleStep += change;

    // Safety check: check for maximum value
    if (refAngleStep > MAXIMUM_ANGLE_STEP) {
      reportWarning("Angle step over maximum value: " +
                    std::to_string(refAngleStep));
      refAngleStep = MAXIMUM_ANGLE_STEP;
    }
    // Safety check: check for minimum value
    else if (refAngleStep < MINIMUM_ANGLE_STEP) {
      // reportWarning("Angle step under minimum value: " +
      //               std::to_string(refAngleStep));
      refAngleStep = MINIMUM_ANGLE_STEP;
    }

    // Reset step counts
    refAcceptedAngleSteps = 0;
    refTotalAngleSteps = 0;
  }



  std::string LinearStepOptimizer::getModuleName() const
  {
    return "LinearStepOptimizer";
  }
}
