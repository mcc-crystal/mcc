/** \file LinearStepOptimizer.hpp
 *  \brief Declares step optimizer using linear function to perform step
 *         optimization.
 */

#ifndef LINEARSTEPOPTIMIZER_HPP_INCLUDED
#define LINEARSTEPOPTIMIZER_HPP_INCLUDED

#include "StepOptimizer.hpp"



namespace mcc
{
  /** \brief Optimizes maximum steps in Monte Carlo simulations using a
   *         linear function.
   *
   *  Formula:
   *  newStep = oldStep + constant * (acceptedRatio - idealAcceptedRatio)
   *
   *  Accepted ratio is calculated as number of accepted steps divided by
   *  number of total steps.
   */
  class LinearStepOptimizer : public StepOptimizer
  {
  public:
    // Static constants
    /** \brief Scaling constant for optimization of max length step */
    static const double LENGTH_OPTIMIZATION_SCALER;
    /** \brief Scaling constant for optimization of max angle step */
    static const double ANGLE_OPTIMIZATION_SCALER;

    /** \brief Initializes base class.
     *  
     *  \param setting Reference to shared global settings object
     *  \param mcController Reference to parent Monte Carlo controller module
     *                      which owns this object
     */
    LinearStepOptimizer(const GlobalSettings& settings,
                        const MonteCarloController& mcController);

    void performLengthStepOptimization(int& refAcceptedLengthSteps,
                                       int& refTotalLengthSteps,
                                       double& refLengthStep) override;

    void performAngleStepOptimization(int& refAcceptedAngleSteps,
                                      int& refTotalAngleSteps,
                                      double& refAngleStep) override;

    std::string getModuleName() const override;
  };
}



#endif // LINEARSTEPOPTIMIZER_HPP_INCLUDED
