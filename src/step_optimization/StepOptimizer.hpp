/** \file StepOptimizer.hpp
 *  \brief Declares base class for Monte Carlo step optimizers.
 */

#ifndef STEPOPTIMIZER_HPP_INCLUDED
#define STEPOPTIMIZER_HPP_INCLUDED

#include "../core/Module.hpp"

#include "../monte_carlo/MonteCarloController.hpp"



namespace mcc
{
  // Forward declaration
  class MonteCarloController;



  /** \brief Base class for Monte Carlo step optimizers. Their purpose is to
   *         provide different methods of maximum simulation step optimization.
   */
  class StepOptimizer : public Module
  {
  public:
    // Static constants
    /** \brief Maximum value of maximum step for length parameters [Å]. It
               will not get optimized above this value. */
    static const double MAXIMUM_LENGTH_STEP;
    /** \brief Maximum value of maximum step for angle parameters [°]. It
               will not get optimized above this value. */
    static const double MAXIMUM_ANGLE_STEP;
    /** \brief Minimum value of maximum step for length parameters [Å]. It
               will not get optimized below this value (to prevent negative
               steps). */
    static const double MINIMUM_LENGTH_STEP;
    /** \brief Minimum value of maximum step for angle parameters [°]. It
               will not get optimized below this value (to prevent negative
               steps). */
    static const double MINIMUM_ANGLE_STEP;
    /** \brief Maximum change of maximum length step during optimization [Å].
     */
    static const double MAXIMUM_LENGTH_STEP_CHANGE;
    /** \brief Maximum change of maximum angle step during optimization [°]. */
    static const double MAXIMUM_ANGLE_STEP_CHANGE;

    /** \brief Initializes base class, this object's members and copies
     *         relevant settings from shared object.
     * 
     *  \param settings Reference to shared global settings object
     *  \param mcController Reference to parent Monte Carlo controller module
     *                      which owns this object
     */
    StepOptimizer(const GlobalSettings& settings,
                  const MonteCarloController& mcController);

    /** \brief Optimizes maximum length step using data provided as arguments.
     *         Resets step counts after optimization is done.
     *
     *  \param refAcceptedLengthSteps Number of accepted length steps (reset
     *                                to 0 after optimization)
     *  \param refTotalLengthSteps Number of total length steps (including
     *                             rejected ones; reset to 0 after
     *                             optimization)
     *  \param refLengthStep Maximum length step to be modified
     */
    virtual void performLengthStepOptimization(int& refAcceptedLengthSteps,
                                               int& refTotalLengthSteps,
                                               double& refLengthStep) = 0;

     /** \brief Optimizes maximum angle step using data provided as arguments.
      *         Resets step counts after optimization is done.
      *
      *  \param refAcceptedAngleSteps Number of accepted angle steps (reset to
      *                               0 after optimization)
      *  \param refTotalAngleSteps Number of total angle steps (including
      *                            rejected ones; reset to 0 after
      *                            optimization)
      *  \param refAngleStep Maximum angle step to be modified
      */
    virtual void performAngleStepOptimization(int& refAcceptedAngleSteps,
                                              int& refTotalAngleSteps,
                                              double& refAngleStep) = 0;

  protected:
    /** \brief Reference to MonteCarloController parent object. */
    const MonteCarloController& mcController_;
    /** \brief Ideal ratio of accepted steps vs total steps from settings */
    double idealAcceptedRatio_;
  };
}



#endif // STEPOPTIMIZER_HPP_INCLUDED
