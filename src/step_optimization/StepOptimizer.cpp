#include "StepOptimizer.hpp"



namespace mcc
{
  // Static initialization
  const double StepOptimizer::MAXIMUM_LENGTH_STEP = 2.0;
  const double StepOptimizer::MAXIMUM_ANGLE_STEP = 3.0;
  const double StepOptimizer::MINIMUM_LENGTH_STEP = 1e-4;
  const double StepOptimizer::MINIMUM_ANGLE_STEP = 1e-3;
  const double StepOptimizer::MAXIMUM_LENGTH_STEP_CHANGE = 0.50;
  const double StepOptimizer::MAXIMUM_ANGLE_STEP_CHANGE = 1.00;



  StepOptimizer::StepOptimizer(const GlobalSettings& settings,
                               const MonteCarloController& mcController) :
                 Module{settings},
                 mcController_{mcController}
  {
    // Initialize from settings
    idealAcceptedRatio_ = globalSettings_.monteCarlo_acceptedStepRatio;
  }
}
