/** \file LogarithmicStepOptimizer.hpp
 *  \brief Declares step optimizer using logarithmic function to change step.
 */

#ifndef LOGARITHMICSTEPOPTIMIZER_HPP_INCLUDED
#define LOGARITHMICSTEPOPTIMIZER_HPP_INCLUDED

#include "StepOptimizer.hpp"



namespace mcc
{
  /** \brief Optimizes maximum steps in Monte Carlo simulations using a
   *         logarithmic function.
   *
   * Formula:
   * difference = acceptedRatio - idealAcceptedRatio
   *
   * if difference < 0:
   * newStep = oldStep + constant * -ln(difference)
   *
   * otherwise:
   * newStep = oldStep + constant * ln(-difference)
   *
   * Accepted ratio is calculated as number of accepted steps divided by number
   * of total steps.
   */
  class LogarithmicStepOptimizer : public StepOptimizer
  {
  public:
    // Static constants
    /** \brief Scaling constant for optimization of max length step */
    static const double LENGTH_OPTIMIZATION_SCALER;
    /** \brief Scaling constant for optimization of max angle step */
    static const double ANGLE_OPTIMIZATION_SCALER;

    /** \brief Initializes base class.
     * 
     *  \param settings Reference to shared global settings object
     *  \param mcController Reference to parent Monte Carlo controller module
     *                      which owns this object
     */
    LogarithmicStepOptimizer(const GlobalSettings& settings,
                             const MonteCarloController& mcController);

    void performLengthStepOptimization(int& refAcceptedLengthSteps,
                                       int& refTotalLengthSteps,
                                       double& refLengthStep) override;

    void performAngleStepOptimization(int& refAcceptedAngleSteps,
                                      int& refTotalAngleSteps,
                                      double& refAngleStep) override;

    std::string getModuleName() const override;
  };
}



#endif // LOGARITHMICSTEPOPTIMIZER_HPP_INCLUDED
