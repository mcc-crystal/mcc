/** \file AllowedPeakGenerator.hpp
 *  \brief Declares peak generator module, which calculates only reflections
 *         allowed by the symmetry.
 */
#ifndef ALLOWEDPEAKGENERATOR_HPP_INCLUDED
#define ALLOWEDPEAKGENERATOR_HPP_INCLUDED

#include "PeakGenerator.hpp"



namespace mcc
{
  /** \brief An implementation of peak generator, which generates peaks while
   *         filtering out hkl triplets not allowed by the symmetry. Bounds of
   *         Miller indices are determined automatically.
   */
  class AllowedPeakGenerator : public PeakGenerator
  {
  public:
    /** \brief Initializes base class and members. Copies relevant settings
     *         from shared settings object.
     * 
     *  \param globalSettings Reference to shared global settings object
     *  \param observedList Optional list of observed peaks to get maximum
     *                      2Theta from
     */
    AllowedPeakGenerator(const GlobalSettings& globalSettings,
                         const ObservedPeakList* observedList = nullptr);

    std::string getModuleName() const override;

    void reset() override;

    bool calculateNextReflection(Reflection& outReflection) override;


  protected:
    /** \brief Minimum index h (calculated by calculateIndexBounds()) */
    int hMin_;
    /** \brief Minimum index k (calculated by calculateIndexBounds()) */
    int kMin_;
    /** \brief Minimum index l (calculated by calculateIndexBounds()) */
    int lMin_;
    /** \brief Maximum index h (calculated by calculateIndexBounds()) */
    int hMax_;
    /** \brief Maximum index k (calculated by calculateIndexBounds()) */
    int kMax_;
    /** \brief Maximum index l (calculated by calculateIndexBounds()) */
    int lMax_;

    /** \brief Minimum d (based on max 2Theta, not on wavelength!).
     *         Calculated when the generator is reset.
     * 
     *  Maximum 2Theta used is either provided by settings, set manually by
     *  method, or retrieved from list of observed peaks when possible.
     */
    double dMinTT_;
    /** \brief Whether reflection being calculated is the first one in the
     *         series.
     */
    bool isFirst_;

    /** \brief Check if reflection with the current hkl triplet is allowed
     *         considering the crystal system symmetry.
     *
     *  \return True if reflection is allowed, false otherwise
     */
    bool isReflectionAllowed() const;

    /** \brief Calculates minimum and maximum Miller indices based on crystal
     *         system and maximum 2Theta.
     */
    void calculateIndexBounds();
  };
}



#endif // ALLOWEDPEAKGENERATOR_HPP_INCLUDED
