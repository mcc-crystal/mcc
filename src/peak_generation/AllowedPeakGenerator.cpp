#include "AllowedPeakGenerator.hpp"

#include "../core/CrystalSystem.hpp"
#include "../core/DTwoThetaConverter.hpp"

#include <cmath>

// For compatibility with other compilers
#ifndef M_PI
#define M_PI 3.14159265358979323846d
#endif // M_PI



namespace mcc
{
  AllowedPeakGenerator::AllowedPeakGenerator(
                                      const GlobalSettings& globalSettings,
                                      const ObservedPeakList* observedList) :
                        PeakGenerator(globalSettings, observedList),
                        hMin_{0},
                        kMin_{0},
                        lMin_{0},
                        hMax_{0},
                        kMax_{0},
                        lMax_{0},
                        dMinTT_{0.0},
                        isFirst_{true}
  { }



  std::string AllowedPeakGenerator::getModuleName() const
  {
    return "AllowedPeakGenerator";
  }



  void AllowedPeakGenerator::reset()
  {
    // Get maximum 2 Theta from list of observed peaks, if provided
    if (observedList_ != nullptr)
      twoThetaMax_ = observedList_->getMaxTwoTheta() + twoThetaTolerance_;
    
    // Calculate minimum d from max 2Theta
    dMinTT_ = DTwoThetaConverter::twoThetaToD(twoThetaMax_);

    // If minimum d from 2Theta is smaller than theoretical smallest
    // measurable d (maybe user set a weird max 2Theta), use the theoretical
    // smallest d instead
    if (dMinTT_ < dMin_)
      dMinTT_ = dMin_;

    calculateIndexBounds();
    h_ = hMin_;
    k_ = kMin_;
    l_ = lMin_;
    isFirst_ = true;
  }



  bool AllowedPeakGenerator::calculateNextReflection(Reflection& outReflection)
  {
    if (isFirst_) {
      isFirst_ = false;
      goto ReflectionAllowedCheck;
    }

    while (true) {
      l_ += 1;
      if (l_ <= lMax_) {
        // JUMP LABEL
        ReflectionAllowedCheck:
        if (isReflectionAllowed()) {
          d_ = cell_->calculateD(h_, k_, l_);

          // 2Theta calculation beyond minimum d would fail because arc sin
          // (ensured that does not happen by checking dMinTT_ against dMin_)
          //
          // Now checking against d_ against dMinTT_ instead of dMin_ which is
          // larger -> more likely to not have to calculate 2Theta
          if (d_ > dMinTT_) {
            twoTheta_ = DTwoThetaConverter::dToTwoTheta(d_);
            // Because of more strict evaluations of d_, 2Theta checking is
            // no longer necessary
            /*
            if (twoTheta_ <= twoThetaMax_) {
              assignReflection(outReflection);
              return true;
            }
            */
            assignReflection(outReflection);
            return true;
          }
        }
        // else: continue
      }
      else {
        k_ += 1;
        if (k_ <= kMax_) {
          l_ = lMin_;
          goto ReflectionAllowedCheck;
        }
        else {
          h_ += 1;
          if (h_ <= hMax_) {
            k_ = kMin_;
            l_ = lMin_;
            goto ReflectionAllowedCheck;
          }
          else {
            return false;
          }
        }
      }
    }
  }



  bool AllowedPeakGenerator::isReflectionAllowed() const
  {
    // Eliminate 000
    if (h_ == 0 && k_ == 0 && l_ == 0) return false;

    switch (cell_->getCrystalSystem())
    {
      case CrystalSystem::CUBIC:
        if (k_ > l_) return false;
        // Fall-through
      case CrystalSystem::HEXAGONAL:
        // Fall-through
      case CrystalSystem::TETRAGONAL:
        if (h_ > k_) return false;
        // Fall-through
      case CrystalSystem::ORTHORHOMBIC:
        // The following now excluded with boundary index calculation
        // if (h_ < 0 || k_ < 0 || l_ < 0) return false;
        // break;
        // Fall-through
      case CrystalSystem::MONOCLINIC:
        // The following now excluded with boundary index calculation
        // if (k_ < 0 || l_ < 0) return false;
        // break;
        // Fall-through
      case CrystalSystem::TRICLINIC:
        // The following now excluded with boundary index calculation
        // if (l_ < 0) return false;
        return true;
        break;
    }
    return true;
  }



  void AllowedPeakGenerator::calculateIndexBounds()
  {
    switch (cell_->getCrystalSystem())
    {
      // Cubic
      case CrystalSystem::CUBIC:
        hMin_ = 0;
        kMin_ = 0;
        lMin_ = 0;

        hMax_ = std::ceil(cell_->a / dMinTT_);
        kMax_ = hMax_;
        lMax_ = hMax_;
        break;
      // Hexagonal
      case CrystalSystem::HEXAGONAL:
        hMin_ = 0;
        kMin_ = 0;
        lMin_ = 0;

        hMax_ = std::ceil(std::sqrt(0.75) * cell_->a / dMinTT_);
        kMax_ = hMax_;
        lMax_ = std::ceil((cell_->c * cell_->c) * ((1.0 / (dMinTT_ * dMinTT_) -
                                        (4.0 / (3.0 * cell_->a * cell_->a)))));
        break;
      // Tetragonal
      case CrystalSystem::TETRAGONAL:
        hMin_ = 0;
        kMin_ = 0;
        lMin_ = 0;

        hMax_ = std::ceil(cell_->a / dMinTT_);
        kMax_ = hMax_;
        lMax_ = std::ceil(cell_->c / dMinTT_);
        break;
      // Orthorhombic
      case CrystalSystem::ORTHORHOMBIC:
        hMin_ = 0;
        kMin_ = 0;
        lMin_ = 0;

        hMax_ = std::ceil(cell_->a / dMinTT_);
        kMax_ = std::ceil(cell_->b / dMinTT_);
        lMax_ = std::ceil(cell_->c / dMinTT_);
        break;
      // Monoclinic
      case CrystalSystem::MONOCLINIC:
        hMax_ = std::ceil(cell_->a * std::sin(cell_->beta * M_PI / 180.0) /
                          dMinTT_);
        kMax_ = std::ceil(cell_->b / dMinTT_);
        lMax_ = std::ceil(cell_->c * std::sin(cell_->beta * M_PI / 180.0) /
                          dMinTT_);

        hMin_ = -hMax_;
        kMin_ = 0; // Symmetry independence
        lMin_ = 0; // Symmetry independence
        break;
      // Triclinic
      case CrystalSystem::TRICLINIC:
        double cosAlpha = std::cos(cell_->alpha * M_PI / 180.0);
        double cosBeta = std::cos(cell_->beta * M_PI / 180.0);
        double cosGamma = std::cos(cell_->gamma * M_PI / 180.0);

        double cosSqAlpha = cosAlpha * cosAlpha;
        double cosSqBeta = cosBeta * cosBeta;
        double cosSqGamma = cosGamma * cosGamma;

        double x = 1.0 + 2.0 * cosAlpha * cosBeta * cosGamma -
                   cosSqAlpha - cosSqBeta - cosSqGamma;

        hMax_ = std::ceil(cell_->a / dMinTT_ *
                          std::sqrt(x / (1.0 - cosSqAlpha)));
        kMax_ = std::ceil(cell_->b / dMinTT_ *
                          std::sqrt(x / (1.0 - cosSqBeta)));
        lMax_ = std::ceil(cell_->c / dMinTT_ *
                          std::sqrt(x / (1.0 - cosSqGamma)));

        hMin_ = -hMax_;
        kMin_ = -kMax_;
        lMin_ = 0; // Symmetry independence
        break;
    }
  }
}
