#include "PeakGenerator.hpp"

#include <cmath>
#include <stdexcept>

#include "../core/DTwoThetaConverter.hpp"

// For compatibility with other compilers
#ifndef M_PI
#define M_PI 3.14159265358979323846d
#endif // M_PI



namespace mcc
{
  PeakGenerator::PeakGenerator(const GlobalSettings& globalSettings,
                               const ObservedPeakList* observedList) :
                 Module{globalSettings},
                 observedList_{observedList},
                 cell_{nullptr},
                 h_{0},
                 k_{0},
                 l_{0},
                 d_{0.0},
                 twoTheta_{0.0},
                 twoThetaMax_{0.0},
                 twoThetaTolerance_{0.0},
                 dMin_{0.0}
  {
    // Initialize from settings
    wavelength_ = globalSettings_.main_wavelength;
    twoThetaMax_ = globalSettings.peakGeneration_twoThetaMax;
    twoThetaTolerance_ = globalSettings.main_twoThetaTolerance;

    // Calculate theoretical minimum d
    dMin_ = wavelength_ / 2.0;
  }



  void PeakGenerator::setUnitCell(const UnitCell* unitCell)
  {
    cell_ = unitCell;
  }



  void PeakGenerator::setObservedList(const ObservedPeakList* observedList)
  {
    observedList_ = observedList;
  }



  void PeakGenerator::setMaxTwoTheta(double maxTwoTheta)
  {
    twoThetaMax_ = maxTwoTheta;
  }



  void PeakGenerator::populatePeakList(CalculatedPeakList& peakList)
  {
    if (cell_ == nullptr)
      throw std::logic_error("Unit cell is not set! "
                             "Can not calculate reflections.");

    peakList.clear();
    reset();

    while (true) {
      Reflection r;
      bool result {calculateNextReflection(r)};
      if (result == false) break;
      peakList.addReflection(r);
    }
  }



  void PeakGenerator::assignReflection(Reflection& outReflection) const
  {
    outReflection.d = d_;
    outReflection.twoTheta = twoTheta_;
    outReflection.h = h_;
    outReflection.k = k_;
    outReflection.l = l_;
  }
}