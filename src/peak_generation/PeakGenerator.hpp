/** \file PeakGenerator.hpp
 *  \brief Declares base class for all peak generator modules.
 */
#ifndef PEAKGENERATOR_HPP_INCLUDED
#define PEAKGENERATOR_HPP_INCLUDED

#include "../core/Module.hpp"

#include "../core/UnitCell.hpp"
#include "../peak_list/ObservedPeakList.hpp"
#include "../peak_list/CalculatedPeakList.hpp"
#include "../core/Reflection.hpp"



namespace mcc
{
  /** \brief Abstract base class for peak generator modules.
   */
  class PeakGenerator : public Module
  {
  public:
    /** \brief Initializes Module base class, this object's members, copies
     *         relevant settings from shared settings object and calculates
     *         theoretical minimum d.
     * 
     *  \param globalSettings Reference to shared global settings object
     *  \param observedList Optional list of observed peaks to get maximum
     *                      2Theta from
     */
    PeakGenerator(const GlobalSettings& globalSettings,
                  const ObservedPeakList* observedList = nullptr);

    /** \brief Calculates next reflection. Returns false if all
     *         peaks within limits have been calculated (result is not
     *         assigned in this case).
     *
     *  \param outReflection Reference to the variable to contain the
     *                       calculated reflection
     *
     *  \return True if new reflection was calculated and assigned to given
     *          parameter, false if no more reflections will be calculated
     */
    virtual bool calculateNextReflection(Reflection& outReflection) = 0;

    /** \brief Resets peak generator to generate peaks from the beginning
     *         again.
     */
    virtual void reset() = 0;

    /** \brief Sets the unit cell to calculate reflections of.
     *
     *  \param unitCell Unit cell, which provides parameters and crystal
     *                  system for peak calculation
     */
    void setUnitCell(const UnitCell* unitCell);

    /** \brief Sets observed peak list to be used for determining
     *         maximum 2Theta.
     *
     *  \param observedList Reference to observed peak list
     */
    void setObservedList(const ObservedPeakList* observedList);

    /** \brief Sets maximum 2Theta for calculated peaks.
     *
     *  \param maxTwoTheta Maximum 2Theta [°]
     */
    void setMaxTwoTheta(double maxTwoTheta);

    /** \brief Populates provided calculated peak list using. Peak generator
     *         is reset and peaks list is cleared before calculation.
     */
    virtual void populatePeakList(CalculatedPeakList& peakList);


  protected:
    /** \brief Wavelength of X-rays [Å] from settings */
    double wavelength_;
    /** \brief Current Miller index h */
    int h_;
    /** \brief Current Miller index k */
    int k_;
    /** \brief Current Miller index l */
    int l_;
    /** \brief Last calculated d [Å] */
    double d_;
    /** \brief Last calculated twoTheta [°] */
    double twoTheta_;
    /** \brief Maximum 2Theta [°] of calculated peaks */
    double twoThetaMax_;
    /** \brief 2Theta tolerance [°] from settings. Should be added to maximum
     *         2Theta if retrieved from list of observed peaks for
     *         good measure.
     */
    double twoThetaTolerance_;
    /** \brief Theoretical minimum d based on wavelength [Å]
     *         (d > lambda / 2)
     */
    double dMin_;
    /** \brief Unit cell to generate reflections of */
    const UnitCell* cell_;
    /** \brief Optional observed peak list to get max 2Theta from */
    const ObservedPeakList* observedList_;

    /** \brief Assigns current calculated reflection data to provided
     *         Reflection object.
     * 
     *  \param outReflection Reference to the object to assign reflection
     *                       data to
     */
    void assignReflection(Reflection& outReflection) const;
  };
}



#endif // PEAKGENERATOR_HPP_INCLUDED
