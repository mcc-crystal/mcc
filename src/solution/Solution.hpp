/** \file Solution.hpp
 *  \brief Declares a class meant to contain solutions (unit cells with other
 *         relevant data).
 */

#ifndef SOLUTION_HPP_INCLUDED
#define SOLUTION_HPP_INCLUDED

#include "../core/UnitCell.hpp"
#include "../core/CrystalSystem.hpp"
#include "../core/CellStats.hpp"



namespace mcc
{
  /** \brief Contains relevant data of one solution. This includes unit cell
   *         parameters and cell's energy.
   */
  class Solution
  {
  public:
    /** \brief Copies unit cell parameters into the Solution object.
     *
     *  \param unitCell Source unit cell to copy data from
     *  \param energy Energy of the unit cell
     *  \param cellStats Optional CellStats object with numbers of indexed and
     *                   unindexed peaks (should already be calculated,
     *                   constructor only reads the data already present)
     * \param nObservedPeaks Number of observed peaks (useful for comparison
     *                       with number of calculated peaks
     */
    Solution(const UnitCell& unitCell,
             double energy,
             const CellStats* cellStats = nullptr,
             int nObservedPeaks = 0);

    /** \brief Unit cell parameter a [Å] */
    double a;
    /** \brief Unit cell parameter b [Å] */
    double b;
    /** \brief Unit cell parameter c [Å] */
    double c;
    /** \brief Unit cell parameter alpha [°] */
    double alpha;
    /** \brief Unit cell parameter beta [°] */
    double beta;
    /** \brief Unit cell parameter gamma [°] */
    double gamma;
    /** \brief Volume of unit cell [Å³] */
    double volume;
    /** \brief Crystal system of the cell */
    CrystalSystem crystalSystem;
    /** \brief Energy of the solution */
    double energy;
    /** \brief Number of indexed peaks */
    int nIndexed;
    /** \brief Number of unindexed peaks */
    int nUnindexed;
    /** \brief Number of calculated peaks */
    int nCalculated;
    /** \brief Number of observed peaks */
    int nObserved;
  };
}



#endif // SOLUTION_HPP_INCLUDED
