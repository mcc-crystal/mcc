#include "DynamicSolutionStorage.hpp"



namespace mcc
{
  DynamicSolutionStorage::DynamicSolutionStorage(
                                              const GlobalSettings& settings) :
                          SolutionStorage{settings}
  { }



  std::string DynamicSolutionStorage::getModuleName() const
  {
    return "DynamicSolutionStorage";
  }



  int DynamicSolutionStorage::getSolutionCount() const
  {
    return solutions_.size();
  }



  Solution DynamicSolutionStorage::getSolution(int i) const
  {
    return solutions_[i];
  }



  void DynamicSolutionStorage::submitSolution(const Solution& solution)
  {
    const int N = {static_cast<int>(solutions_.size())};
    // If there are less than maximum solutions contained, accept in any case
    if (N < maxN_) {
      solutions_.push_back(solution);
    }
    else {
      // Consider accepting only if energy is lower than that of the
      // worst solution
      if (getWorstEnergy() > solution.energy) {
        // Check for identical solutions
        int iIdentical {findIdenticalSolution(solution)};
        // If no identical solutions found ...
        if (iIdentical < 0) {
          solutions_[N - 1] = solution;
        }
        // If energy of identical solution is higher, replace it, else ignore
        else {
          Solution& identical = solutions_[iIdentical];
          if (identical.energy > solution.energy)
            identical = solution;
          else
            return;
        }
      }
    }

    bubbleSortSolutions();
  }



  double DynamicSolutionStorage::getWorstEnergy() const
  {
    // If there are no solutions yet, return some very high energy so any
    // solution gets accepted.
    if (solutions_.empty()) {
      return 1e20;
    }
    return solutions_.back().energy;
  }



  void DynamicSolutionStorage::bubbleSortSolutions()
  {
    bool sortingPerformed;
    const int N = {static_cast<int>(solutions_.size())};

    do {
      sortingPerformed = false;
      for (int iRight {1}; iRight < N; ++iRight) {
        int iLeft {iRight - 1};
        Solution& sLeft = solutions_[iLeft];
        Solution& sRight = solutions_[iRight];
        if (sLeft.energy > sRight.energy) {
          Solution sTemp = sLeft;
          sLeft = sRight;
          sRight = sTemp;
          sortingPerformed = true;
        }
      }
    } while (sortingPerformed == true);
  }
}
