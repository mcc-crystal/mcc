/** \file SolutionStorage.hpp
 *  \brief Declares base class for storing candidate unit cells (solutions).
 */

#ifndef SOLUTIONSTORAGE_HPP_INCLUDED
#define SOLUTIONSTORAGE_HPP_INCLUDED

#include <string>

#include "../core/Module.hpp"
#include "../solution/Solution.hpp"



namespace mcc
{
  /** \brief Base class for storing candidate unit cells (solutions).
   */
  class SolutionStorage : public Module
  {
  public:
    /** \brief Initializes base Module class and copies relevant settings from
     *         shared global settings object.
     */
    SolutionStorage(const GlobalSettings& settings);

    /** \brief Return the number of solutions stored in the object.
     *
     *  \return Number of solutions
     */
    virtual int getSolutionCount() const = 0;

    /** \brief Gets a Solution object pointed to by index i.
     *
     *  \param i Index of solution
     *  \return Solution object
     */
    virtual Solution getSolution(int i) const = 0;

    /** \brief Stores the solution in the object and sorts solutions.
     *
     *  If there are less than maximum number of solutions in the list,
     *  the solution is stored in any case. Otherwise it is only stored if its
     *  energy is lower than the worst, replacing the solution with the worst
     *  energy.
     *
     *  Before storing the solution it is checked if it is identical to one of
     *  already stored solutions. In this case it replaces it or is ignored.
     *
     * \param solution Solution to store
     */
    virtual void submitSolution(const Solution& solution) = 0;

    /** \brief Returns energy of the worst solution in the storage. If the
     *         storage is empty, some very high positive energy is returned.
     *
     *  \return Energy of the worst solution
     */
    virtual double getWorstEnergy() const = 0;

    /** \brief Writes solutions to file.
     *
     *  \param filePath Path to the file to write to
     */
    void writeToFile(std::string filePath) const;

    /** \brief Writes information about a single solution to a custom stream.
     *
     *  \param stream Stream to write solution data to
     *  \param s Solution to write data of
     *  \param solutionNumber Number that is written next to solution
     */
    void writeSolutionBlock(std::ostream& stream, const Solution& s,
                            int solutionNumber) const;


  protected:
    /** \brief Characters reserved for name of a parameter */
    static const int PARAM_NAME_WIDTH;
    /** \brief Characters reserved for value of a parameter */
    static const int PARAM_VALUE_WIDTH;
    /** \brief Characters reserved for value of cell volume */
    static const int VOLUME_VALUE_WIDTH;
    /** \brief Characters reserved for number of indexed or unindexed peaks */
    static const int N_INDEXED_VALUE_WIDTH;
    /** \brief Characters reserved for energy value */
    static const int ENERGY_VALUE_WIDTH;
    /** \brief Precision of length parameters */
    static const int LENGTH_PRECISION;
    /** \brief Precision of angle parameters */
    static const int ANGLE_PRECISION;
    /** \brief Precision of cell volume */
    static const int VOLUME_PRECISION;
    /** \brief Precision of energy */
    static const int ENERGY_PRECISION;

    /** \brief Maximum number of solutions stored from settings */
    int maxN_;
    /** \brief Identical solution length parameter tolerance from settings */
    double lengthTolerance_;
    /** \brief Identical solution angle parameter tolerance from settings */
    double angleTolerance_;
    /** \brief Identical solution energy tolerance from settings */
    double energyTolerance_;

    /** \brief Returns index of an identical solution if it exists (prioritises
     *         solutions with lowest energy (best). Returns a negative number
     *         if no identical solution is found.
     *
     * Solutions are identical if their energies and cell parameters are
     * similar enough. If identical solution with lower energy is found, it
     * should replace the one in the list, otherwise it should be ignored. If
     * solutions are not identical, solution should be added to the list.
     *
     * \param solution Solution to be tested against all solutions in the list
     * \return Index of identical solution or negative number if no identical
     *         solution is found
     */
    int findIdenticalSolution(const Solution& solution) const;

    /** \brief Returns true if solutions are identical (have similar energy and
     *         cell parameters).
     *
     * \param s1 Solution to be compared
     * \param s2 The other solution to be compared
     * \return True if solutions are identical, false otherwise
     */
    bool areSolutionsIdentical(const Solution& s1, const Solution& s2) const;
  };
}



#endif // SOLUTIONSTORAGE_HPP_INCLUDED
