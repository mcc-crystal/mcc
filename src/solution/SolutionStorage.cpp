#include "SolutionStorage.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <exception>
#include <cmath>



namespace mcc
{
  // Static initialization
  const int SolutionStorage::PARAM_NAME_WIDTH = 6;
  const int SolutionStorage::PARAM_VALUE_WIDTH = 7;
  const int SolutionStorage::VOLUME_VALUE_WIDTH = 6;
  const int SolutionStorage::N_INDEXED_VALUE_WIDTH = 3;
  const int SolutionStorage::ENERGY_VALUE_WIDTH = 8;
  const int SolutionStorage::LENGTH_PRECISION = 3;
  const int SolutionStorage::ANGLE_PRECISION = 2;
  const int SolutionStorage::VOLUME_PRECISION = 0;
  const int SolutionStorage::ENERGY_PRECISION = 3;



  SolutionStorage::SolutionStorage(const GlobalSettings& settings) :
                   Module{settings}
  {
    // Initialize from settings
    maxN_ = globalSettings_.main_nStoredCells;
    lengthTolerance_ = globalSettings_.solutions_lengthTolerance;
    angleTolerance_ = globalSettings_.solutions_angleTolerance;
    energyTolerance_ = globalSettings_.solutions_energyTolerance;
  }



  void SolutionStorage::writeToFile(std::string filePath) const
  {
    const int N {getSolutionCount()};
    std::ofstream file;

    try {
      file.open(filePath, std::ios::out | std::ios::trunc);

      if (file.is_open()) {
        file << N << " solutions:" << std::endl;

        for (int i = 0; i < N; ++i) {
          const Solution& s = getSolution(i);
          writeSolutionBlock(file, s, i + 1);
        }
      }
      else {
        std::stringstream message;
        message << "Failed to open file '" << filePath << "' while writing "
                   "solutions.";
        reportError(message.str());
      }
    }
    catch (const std::exception& e) {
      std::stringstream message;
      message << "Exception occured while writing to file '" << filePath <<
                 "': " << e.what();
      reportError(message.str());
    }

    if (file.is_open())
      file.close();
  }



  void SolutionStorage::writeSolutionBlock(std::ostream& stream,
                                           const Solution& s,
                                           int solutionNumber) const
  {
    stream << std::fixed;
    stream << "\nSolution " << solutionNumber << ": E = " <<
              std::setprecision(ENERGY_PRECISION) <<
              std::setw(ENERGY_VALUE_WIDTH) << s.energy <<
              "                   V = " << std::setw(VOLUME_VALUE_WIDTH) <<
              std::setprecision(VOLUME_PRECISION) << s.volume << " A^3"
              << std::endl;
    stream << std::setw(PARAM_NAME_WIDTH) << "a" << " = " <<
              std::setw(PARAM_VALUE_WIDTH) <<
              std::setprecision(LENGTH_PRECISION) << s.a << " A ";
    stream << std::setw(PARAM_NAME_WIDTH) << "b" << " = " <<
              std::setw(PARAM_VALUE_WIDTH) <<
              std::setprecision(LENGTH_PRECISION) << s.b << " A ";
    stream << std::setw(PARAM_NAME_WIDTH) << "c" << " = " <<
              std::setw(PARAM_VALUE_WIDTH) <<
              std::setprecision(LENGTH_PRECISION) << s.c << " A"
              << std::endl;
    stream << std::setw(PARAM_NAME_WIDTH) << "alpha" << " = " <<
              std::setw(PARAM_VALUE_WIDTH) <<
              std::setprecision(ANGLE_PRECISION) << s.alpha << "°  ";
    stream << std::setw(PARAM_NAME_WIDTH) << "beta" << " = " <<
              std::setw(PARAM_VALUE_WIDTH) <<
              std::setprecision(ANGLE_PRECISION) << s.beta << "°  ";
    stream << std::setw(PARAM_NAME_WIDTH) << "gamma" << " = " <<
              std::setw(PARAM_VALUE_WIDTH) <<
              std::setprecision(ANGLE_PRECISION) << s.gamma << "°"
              << std::endl;
    stream << "Indexed peaks: " << std::setw(N_INDEXED_VALUE_WIDTH) <<
              s.nIndexed << "   Unindexed peaks: " <<
              std::setw(N_INDEXED_VALUE_WIDTH) << s.nUnindexed << std::endl;
  }



  int SolutionStorage::findIdenticalSolution(const Solution& solution) const
  {
    const int N {getSolutionCount()};
    for (int i = 0; i < N; ++i) {
      const Solution& otherSolution {getSolution(i)};
      if (areSolutionsIdentical(solution, otherSolution))
        return i;
    }

    return -1;
  }



  bool SolutionStorage::areSolutionsIdentical(const Solution& s1,
                                              const Solution& s2) const
  {
    double deltaE {std::fabs(s1.energy - s2.energy)};
    // Check energy
    if (deltaE > energyTolerance_)
      return false;

    const int N_PARAM {3};
    double lengths1[] = {s1.a, s1.b, s1.c};
    double lengths2[] = {s2.a, s2.b, s2.c};
    double angles1[] = {s1.alpha, s1.beta, s1.gamma};
    double angles2[] = {s2.alpha, s2.beta, s2.gamma};

    // Check lengths and angles
    for (int i = 0; i < N_PARAM; ++i) {
      double deltaL {fabs(lengths1[i] - lengths2[i])};
      if (deltaL > lengthTolerance_)
        return false;

      double deltaA {fabs(angles1[i] - angles2[i])};
      if (deltaA > angleTolerance_)
        return false;
    }

    return true;
  }
}
