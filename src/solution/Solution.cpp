#include "Solution.hpp"



namespace mcc
{
  Solution::Solution(const UnitCell& cell,
                     double energy,
                     const CellStats* cellStats,
                     int nObservedPeaks) :
            energy{energy},
            crystalSystem{cell.getCrystalSystem()},
            a{cell.a},
            b{0.0},
            c{0.0},
            alpha{90.0},
            beta{90.0},
            gamma{90.0},
            volume{cell.calculateVolume()},
            nIndexed{0},
            nUnindexed{0},
            nCalculated{0},
            nObserved{nObservedPeaks}
  {
    switch (crystalSystem) {
    case CrystalSystem::CUBIC:
      b = a;
      c = a;
      break;
    case CrystalSystem::HEXAGONAL:
      gamma = 120.0;
      // Fall-through
    case CrystalSystem::TETRAGONAL:
      b = a;
      c = cell.c;
      break;
    case CrystalSystem::TRICLINIC:
      alpha = cell.alpha;
      gamma = cell.gamma;
      // Fall-through
    case CrystalSystem::MONOCLINIC:
      beta = cell.beta;
      // Fall-through
    case CrystalSystem::ORTHORHOMBIC:
      a = cell.a;
      b = cell.b;
      c = cell.c;
      break;
    }

    if (cellStats != nullptr) {
      nIndexed = cellStats->getNIndexedPeaks();
      nUnindexed = cellStats->getNUnindexedPeaks();
      nCalculated = cellStats->getNPossiblePeaks();
    }
  }
}
