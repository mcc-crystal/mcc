/** \file DynamicSolutionStorage.hpp
 *  \brief Declares solution storage using dynamic collection.
 */

#ifndef DYNAMICSOLUTIONSTORAGE_HPP_INCLUDED
#define DYNAMICSOLUTIONSTORAGE_HPP_INCLUDED

#include "../solution/SolutionStorage.hpp"

#include <vector>



namespace mcc
{
  /** \brief Solution storage using dynamic collection to store solutions.
   */
  class DynamicSolutionStorage : public SolutionStorage
  {
  public:
    /** \brief Initializes base class.
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit DynamicSolutionStorage(const GlobalSettings& settings);

    std::string getModuleName() const override;

    int getSolutionCount() const override;

    Solution getSolution(int i) const override;

    void submitSolution(const Solution& solution) override;

    double getWorstEnergy() const override;


  protected:
    /** \brief Dynamic collection of solutions */
    std::vector<Solution> solutions_;
    

    /** \brief Sorts solutions using bubble sort method from minimum energy to
     *         maximum energy.
     */
    virtual void bubbleSortSolutions();
  };
}



#endif // DYNAMICSOLUTIONSTORAGE_HPP_INCLUDED
