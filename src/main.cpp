/** \mainpage
 *
 *  MCC - a powder diffraction pattern indexing program
 *  Copyright (C) 2018-2019  Matej Reberc
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 */



/** \file main.cpp
 *  \brief Main program.
 */

#include "core/main_functions.hpp"
#include "core/ArgumentParser.hpp"
#include "core/GlobalSettings.hpp"



int main(int argc, const char* argv[])
{
  bool result;

  // Display greeting
  mcc::printGreeting();

  // Parse command line parameters
  mcc::ArgumentParser args{argc, argv, result};
  if (result == false) return -1;

  // Decide what to do based on the parameters
  if (args.getWriteConfigurationFile()) {
    mcc::writeDefaultConfigurationFile();
    return 0;
  }

  // Read settings
  mcc::GlobalSettings settings;
  result = mcc::readSettingsFile(settings, args.getConfigurationFile());
  if (result == false) return -1;

  // Initialize and run root module
  result = mcc::initializeAndRunRootModule(settings);
  if (result == false) return -1;

  return 0;
}
