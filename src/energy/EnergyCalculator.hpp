/** \file EnergyCalculator.hpp
 *  \brief Declares base class for calculation of energy (penalty function).
 */

#ifndef ENERGYCALCULATOR_HPP_INCLUDED
#define ENERGYCALCULATOR_HPP_INCLUDED

#include "../core/Module.hpp"

#include "../energy/CellEvaluator.hpp"
#include "../peak_list/CalculatedPeakList.hpp"
#include "../peak_list/ObservedPeakList.hpp"



namespace mcc
{
  /** \brief Base class for calculation of energy (penalty function).
   */
  class EnergyCalculator : public Module
  {
  public:
    /** Initializes EnergyCalculator module.
     * 
     *  Also initializes references to calculated and observed peak lists
     *  for derived classes to use.
     * 
     *  \param settings Reference to shared global settings object
     *  \param calculatedList Reference to list of calculated peaks
     *  \param observedList Reference to list of observed peaks
     */
    EnergyCalculator(const GlobalSettings& settings,
                     const CalculatedPeakList& calculatedList,
                     const ObservedPeakList& observedList);

    /** \brief Calculates "energy" of the current unit cell. Implementation
     *         depends on module.
     * 
     *  \warning Module implementations assume that peaks in both observed and
     *           calculated lists are sorted!
     *
     *  \return Energy of the unit cell
     */
    virtual double calculateEnergy() = 0;


  protected:
    /** \brief List of calculated peaks used for energy calculation. */
    const CalculatedPeakList& calculatedList_;
    /** \brief List of observed peaks used for energy calculation. */
    const ObservedPeakList& observedList_;
  };
}



#endif // ENERGYCALCULATOR_HPP_INCLUDED
