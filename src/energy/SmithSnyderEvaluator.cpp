#include "SmithSnyderEvaluator.hpp"

#include <sstream>
#include <cmath>



namespace mcc
{
  SmithSnyderEvaluator::SmithSnyderEvaluator(
                                      const GlobalSettings& globalSettings,
                                      const CalculatedPeakList& calculatedList,
                                      const ObservedPeakList& observedList) :
                        CellEvaluator{globalSettings,
                                      calculatedList,
                                      observedList},
                        cellStats_{globalSettings,
                                   calculatedList,
                                   observedList}
  { }



  std::string SmithSnyderEvaluator::getModuleName() const
  {
    return "SmithSnyderEvaluator";
  }



  double SmithSnyderEvaluator::calculateCellQuality(int n) const
  {
    double sumDeltaTwoTheta {0.0};
    int observedPeakCount {observedList_.getPeakCount()};

    if (n > observedPeakCount) {
      reportError("N is greater than the number of observed peaks!");
      return 0.0;
    }

    int nPossible {cellStats_.calculateNPossiblePeaks()};

    // Sum of Delta2Theta
    for (int i = 0; i < n; ++i) {
      const Peak& observed {observedList_.getPeak(i)};
      int iClosest {calculatedList_.findClosestPeak(observed.twoTheta)};
      const Reflection& calculated {calculatedList_.getReflection(iClosest)};
      double deltaTwoTheta {std::fabs(observed.twoTheta -
                                      calculated.twoTheta)};
      sumDeltaTwoTheta += deltaTwoTheta;
    }

    // F(N) calculation
    double fnLeft (n / sumDeltaTwoTheta);
    double fnRight = (static_cast<double>(n) / static_cast<double>(nPossible));
    double fn = fnLeft * fnRight;

    return fn;
  }

}
