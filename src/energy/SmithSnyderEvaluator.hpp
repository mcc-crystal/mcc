/** \file SmithSnyderEvaluator.hpp
 *  \brief Declares class for evaluating cells based on Smith-Snyder
 *         figure of merit F(N).
 */

#ifndef SMITHSNYDEREVALUATOR_HPP_INCLUDED
#define SMITHSNYDEREVALUATOR_HPP_INCLUDED

#include "../energy/CellEvaluator.hpp"

#include "../core/CellStats.hpp"



namespace mcc
{
  /** \brief Evaluates unit cells using Smith-Snyder figure of merit F(N).
   * 
   *  F(N) = (N / Nposs) (1 / |average delta 2Theta|) <br/>
   *  |average delta 2Theta| = 1/N sum(2Theta(obs, i) - 2Theta(calc, i))
   *
   *  \warning Peaks in calculated and observed lists must be sorted prior to
   *           calculation of F(N).
   */
  class SmithSnyderEvaluator : public CellEvaluator
  {
  public:
    /** Initializes base CellEvaluator class.
     * 
     *  \param globalSettings Reference to shared global settings object
     *  \param calculatedList Reference to list of calculated peaks
     *  \param observedList   Reference to list of observed peaks
     */
    SmithSnyderEvaluator(const GlobalSettings& globalSettings,
                         const CalculatedPeakList& calculatedList,
                         const ObservedPeakList& observedList);

    std::string getModuleName() const override;

    double calculateCellQuality(int n) const override;


  private:
    /** \brief Used to determine number of possible peaks */
    CellStats cellStats_;
  };
}



#endif // SMITHSNYDEREVALUATOR_HPP_INCLUDED
