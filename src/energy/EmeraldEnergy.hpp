/** \file EmeraldEnergy.hpp
 *  \brief Declares energy calculator using formula based on Topas Academic 6.
 */

#ifndef EMERALDENERGY_HPP_INCLUDED
#define EMERALDENERGY_HPP_INCLUDED

#include "EnergyCalculator.hpp"
#include "../core/CellStats.hpp"



namespace mcc
{
  /** \brief Energy calculator using formula based on the one published in
   *         what's new document for Topas Academic 6.
   *
   *  Topas figure of merit:
   *  M = 1 / ((1 + N(uind)) * d²(obs, min) * (N(calc) / N(obs)) *
   *      sum(|d²(obs, i) - d²(calc, i)| * Qi))
   *
   *  Emerald modified energy function:
   *  E = -10 / ((1 + N(uind)) * d²(obs, min) * (N(calc) / N(obs)) *
   *      sum(|d²(obs, i) - d²(calc, i)|))
   */
  class EmeraldEnergy : public EnergyCalculator
  {
  public:
    /** Initializes EnergyCalculator module and embedded CellStats module.
     * 
     *  \param settings Reference to shared global settings object
     *  \param calculatedList Reference to list of calculated peaks
     *  \param observedList Reference to list of observed peaks
     */
    EmeraldEnergy(const GlobalSettings& settings,
                  const CalculatedPeakList& calculatedList,
                  const ObservedPeakList& observedList);

    double calculateEnergy() override;

    std::string getModuleName() const override;

  private:
    /** \brief Cell statistics calculation object. */
    CellStats cellStats_;
  };
}



#endif // EMERALDENERGY_HPP_INCLUDED
