/** \file SmithSnyderEnergy.hpp
 *  \brief Declares energy calculator using only Smith-Snyder figure of merit
 *         to calculate penalty function.
 */

#ifndef SMITHSNYDERENERGY_HPP_INCLUDED
#define SMITHSNYDERENERGY_HPP_INCLUDED

#include "../energy/EnergyCalculator.hpp"
#include "../energy/SmithSnyderEvaluator.hpp"
#include "../peak_list/CalculatedPeakList.hpp"
#include "../peak_list/ObservedPeakList.hpp"



namespace mcc
{
  /** \brief Energy calculator using only Smith-Snyder figure of merit to
   *         calculate penalty function. Sign of the figure of merit is
   *         inverted so that higher figure results in lower energy.
   */
  class SmithSnyderEnergy : public EnergyCalculator
  {
  public:
    /** Initializes EnergyCalculator module and embedded SmithSnyderEvaluator
     *  module.
     * 
     *  \param globalSettings Reference to shared global settings object
     *  \param calculatedList Reference to list of calculated peaks
     *  \param observedList Reference to list of observed peaks
     */
    SmithSnyderEnergy(const GlobalSettings& globalSettings,
                      const CalculatedPeakList& calculatedList,
                      const ObservedPeakList& observedList);

    double calculateEnergy() override;

    std::string getModuleName() const override;


  protected:
    /** \brief Cell evaluator used to calculate figure of merit */
    SmithSnyderEvaluator evaluator_;
    /** \brief Number of observed peaks to use in calculations */
    int n_;

    /** \brief Determines number of peaks to use in unit cell evaluation.
     *         Uses all available peaks in observed peak list.
     */
    void calculateN();
  };
}



#endif // SMITHSNYDERENERGY_HPP_INCLUDED
