#include "CellEvaluator.hpp"



namespace mcc
{
  CellEvaluator::CellEvaluator(const GlobalSettings& globalSettings,
                               const CalculatedPeakList& calculatedList,
                               const ObservedPeakList& observedList) :
                 Module{globalSettings},
                 calculatedList_{calculatedList},
                 observedList_{observedList}
  { }
}
