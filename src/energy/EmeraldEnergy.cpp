#include "EmeraldEnergy.hpp"

#include <cmath>

#include "../core/DTwoThetaConverter.hpp"



namespace mcc
{
  EmeraldEnergy::EmeraldEnergy(const GlobalSettings& settings,
                               const CalculatedPeakList& calculatedList,
                               const ObservedPeakList& observedList) :
                 EnergyCalculator{settings, calculatedList, observedList},
                 cellStats_{globalSettings_, calculatedList_, observedList_}
  { }



  double EmeraldEnergy::calculateEnergy()
  {
    // Prerequisites
    cellStats_.calculateStats();
    const int nUnindexed {cellStats_.getNUnindexedPeaks()};
    const int nCalculated {cellStats_.getNPossiblePeaks()};
    const int nObserved {observedList_.getPeakCount()};

    // To prevent small cells from causing -infinity energy result.
    if (nCalculated == 0) return 0.0;

    double twoThetaObsMax {observedList_.getMaxTwoTheta()};
    double dObsMin {DTwoThetaConverter::twoThetaToD(twoThetaObsMax)};
    double sum {0.0};

    for (int i = 0; i < nObserved; ++i) {
      const Peak& observed {observedList_.getPeak(i)};
      double dObs {DTwoThetaConverter::twoThetaToD(observed.twoTheta)};
      int iCalc {calculatedList_.findClosestPeak(observed.twoTheta)};
      const Reflection& calculated {calculatedList_.getReflection(iCalc)};
      double dCalc {calculated.d};

      sum += std::fabs((dObs * dObs) - (dCalc * dCalc));
    }

    double energy = -10.0 /
                    ((1.0 + nUnindexed) *
                     dObsMin * dObsMin *
                     (static_cast<double>(nCalculated) /
                      static_cast<double>(nObserved)) *
                     sum);
    return energy;
  }



  std::string EmeraldEnergy::getModuleName() const
  {
    return "EmeraldEnergy";
  }
}
