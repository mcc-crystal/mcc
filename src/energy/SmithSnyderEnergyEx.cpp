#include "SmithSnyderEnergyEx.hpp"



namespace mcc
{
  SmithSnyderEnergyEx::SmithSnyderEnergyEx(
                                  const GlobalSettings& settings,
                                  const CalculatedPeakList& calculatedList,
                                  const ObservedPeakList& observedList) :
                  EnergyCalculator{settings, calculatedList, observedList},
                  evaluator_{settings, calculatedList, observedList},
                  cellStats_{settings, calculatedList, observedList},
                  n_ {0}
  {
    // Initialize from settings
    penaltyScaleFactor_ = globalSettings_.energy_SmithSnyderExFactor;
  }



  std::string SmithSnyderEnergyEx::getModuleName() const
  {
    return "SmithSnyderEnergyEx";
  }



  double SmithSnyderEnergyEx::calculateEnergy()
  {
    if (n_ <= 0) {
      determineN();
      if (n_ <= 0) {
        reportError("Number of peaks to use is invalid. "
                    "Check observed peak list!");
      }
    }

    double energy = -(evaluator_.calculateCellQuality(n_));

    // Penalize excess peaks
    int nPoss {cellStats_.calculateNPossiblePeaks()};
    int difference {nPoss - n_};
    if (difference > 0) {
      double penalty {difference * penaltyScaleFactor_};
      energy += penalty;
    }

    return energy;
  }



  void SmithSnyderEnergyEx::determineN()
  {
    // Set N to use all observed peaks in figure of merit calculations.
    n_ = observedList_.getPeakCount();
  }
}
