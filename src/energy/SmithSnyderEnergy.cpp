#include "SmithSnyderEnergy.hpp"



namespace mcc
{
  SmithSnyderEnergy::SmithSnyderEnergy(
                                    const GlobalSettings& globalSettings,
                                    const CalculatedPeakList& calculatedList,
                                    const ObservedPeakList& observedList) :
                    EnergyCalculator{globalSettings,
                                     calculatedList,
                                     observedList},
                    evaluator_{globalSettings, calculatedList, observedList},
                    n_{0}
  {
    calculateN();
  }



  std::string SmithSnyderEnergy::getModuleName() const
  {
    return "SmithSnyderEnergy";
  }



  double SmithSnyderEnergy::calculateEnergy()
  {
    if (n_ <= 0) {
      calculateN();
      if (n_ <= 0) {
        reportError("Number of peaks to use is invalid. "
                    "Check observed peak list!");
      }
    }

    double energy = -(evaluator_.calculateCellQuality(n_));
    return energy;
  }



  void SmithSnyderEnergy::calculateN()
  {
    // Set N to use all observed peaks in figure of merit calculations.
    n_ = observedList_.getPeakCount();
  }
}
