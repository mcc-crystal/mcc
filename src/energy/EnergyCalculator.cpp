#include "EnergyCalculator.hpp"



namespace mcc
{
  EnergyCalculator::EnergyCalculator(const GlobalSettings& settings,
                                     const CalculatedPeakList& calculatedList,
                                     const ObservedPeakList& observedList) :
                    Module{settings},
                    calculatedList_{calculatedList},
                    observedList_{observedList}
  { }
}
