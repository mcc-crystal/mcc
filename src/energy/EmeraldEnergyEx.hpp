/** \file EmeraldEnergyEx.hpp
 *  \brief Declares energy calculator using formula based on Topas Academic 6
 *         with additional customization options.
 */

#ifndef EMERALDENERGYEX_HPP_INCLUDED
#define EMERALDENERGYEX_HPP_INCLUDED

#include "EnergyCalculator.hpp"
#include "../core/CellStats.hpp"



namespace mcc
{
  /** \brief Energy calculator using formula based on the one published in
   *         what's new document for Topas Academic 6. This version is
   *         modified with a customizable exponential function to penalize
   *         excess calculated peaks.
   *
   *  Topas figure of merit:
   *  M = 1 / ((1 + N(uind)) * d²(obs, min) * (N(calc) / N(obs)) *
   *      sum(|d^2(obs, i) - d^2(calc, i)| * Qi))
   *
   *  Emerald Extended modified energy function:
   *  E = -10 / ((1 + unindexedFactor * N(uind)) * d²(obs, min) *
   *      expBase^((N(calc) / N(obs)) - 1.0) *
   *      sum(|d^2(obs, i) - d^2(calc, i)|))
   *
   *  Exponential factor used in exp() is customizable.
   *
   *  Exponential function harshly punishes big cells with lots of reflections.
   */
  class EmeraldEnergyEx : public EnergyCalculator
  {
  public:
    /** Initializes EnergyCalculator module and embedded CellStats module.
     * 
     *  \param settings Reference to shared global settings object
     *  \param calculatedList Reference to list of calculated peaks
     *  \param observedList Reference to list of observed peaks
     */
    EmeraldEnergyEx(const GlobalSettings& settings,
                    const CalculatedPeakList& calculatedList,
                    const ObservedPeakList& observedList);

    double calculateEnergy() override;

    std::string getModuleName() const override;

  private:
    /** \brief Base for exponential function. Greater base forces smaller
     *         unit cells. Copied from settings.
     */
    double expBase_;
    /** \brief Factor to multiply number of unindexed peaks with. Copied from
     *         settings.
     * 
     *  Default behaviour is achieved with factor 1.0. Greater factor makes
     *  the energy function more sensitive to unindexed peaks. Factor between
     *  0.0 and 1.0 makes it less sensitive to unindexed peaks.
     */
    double unindexedFactor_;

    /** \brief Cell statistics calculation object. */
    CellStats cellStats_;
  };
}



#endif // EMERALDENERGYEX_HPP_INCLUDED
