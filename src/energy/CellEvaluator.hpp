/** \file CellEvaluator.hpp
 *  \brief Declares base class for cell evaluators.
 */

#ifndef CELLEVALUATOR_HPP_INCLUDED
#define CELLEVALUATOR_HPP_INCLUDED

#include "../core/Module.hpp"

#include "../peak_list/CalculatedPeakList.hpp"
#include "../peak_list/ObservedPeakList.hpp"



namespace mcc
{
  /** \brief Base class for cell evaluators. Cell evaluators are meant to
   *         compare calculated reflections to observed peaks and numerically
   *         evaluate unit cell quality.
   */
  class CellEvaluator : public Module
  {
  public:
    /** \brief Initializes a new CellEvaluator object, storing references
     *         to global settings and peak lists.
     * 
     *  \param globalSettings Reference to shared global settings object
     *  \param calculatedList Reference to calcualated peak list
     *  \param observedList Reference to observed peak list
     */
    CellEvaluator(const GlobalSettings& globalSettings,
                  const CalculatedPeakList& calculatedList,
                  const ObservedPeakList& observedList);

    /** \brief Calculates numerical quality of unit cell according to observed
     *         data.
     *
     *  Implementation dependent on particular derived class.
     * 
     *  \warning References to calculated and observed peak lists must be
     *           set before use! Peaks in both lists must be sorted!
     *
     *  \param n Number of peaks to use in evaluation
     *  \return Numerical quality of cell
     */
    virtual double calculateCellQuality(int n) const = 0;

  protected:
    /** \brief List of calculated reflections */
    const CalculatedPeakList& calculatedList_;
    /** \brief List of observed peaks */
    const ObservedPeakList& observedList_;
  };
}



#endif // CELLEVALUATOR_HPP_INCLUDED
