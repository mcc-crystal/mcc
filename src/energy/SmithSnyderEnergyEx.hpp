/** \file SmithSnyderEnergyEx.hpp
 *  \brief Declares energy calculator using Smith-Snyder figure of merit and
 *         further penalizing cells with too many calculated peaks.
 */

#ifndef SMITHSNYDERNERGYEX_HPP_INCLUDED
#define SMITHSNYDERNERGYEX_HPP_INCLUDED

#include "../energy/EnergyCalculator.hpp"

#include "../energy/SmithSnyderEvaluator.hpp"
#include "../core/CellStats.hpp"



namespace mcc
{
  /** \brief Energy calculator using Smith-Snyder figure of merit to evaluate
   *         cell. In addition it also penalizes high number of calculated
   *         peaks when number of observed peaks is significantly lower.
   */
  class SmithSnyderEnergyEx : public EnergyCalculator
  {
  public:
    /** Initializes SmithSnyderEnergyEx module with provided references and
     *  copies relevant settings from shared object.
     * 
     *  \param settings Shared global settings object
     *  \param calculatedList Reference to list of calcuated peaks
     *  \param observedList Reference to list of observed peaks
     */
    SmithSnyderEnergyEx(const GlobalSettings& settings,
                        const CalculatedPeakList& calculatedList,
                        const ObservedPeakList& observedList);

    double calculateEnergy() override;

    std::string getModuleName() const override;


  protected:
    /** \brief Smith-Snyder cell evaluator */
    SmithSnyderEvaluator evaluator_;
    /** \brief Used to calculated number of possible peaks */
    CellStats cellStats_;
    /** \brief Scale factor for (N(possible) - N)-based energy penalty
     *         calculation from settings.
     */
    double penaltyScaleFactor_;
    /** \brief Number of observed peaks to use in calculations */
    int n_;

    /** \brief Determines number of peaks to use when evaluating unit cells.
     *         Uses all available peaks in observed peak list.
     */
    void determineN();

    /** \brief Determines number of possible peaks up to last observed peak
     *         used.
     */
    void determineNposs();
  };
}



#endif // SMITHSNYDERNERGYEX_HPP_INCLUDED
