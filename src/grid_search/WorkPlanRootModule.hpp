/** \file WorkPlanRootModule.hpp
 *  \brief Declares base class for root modules using work plans to divide
 *         work.
 */

#ifndef WORKPLANROOTMODULE_HPP_INCLUDED
#define WORKPLANROOTMODULE_HPP_INCLUDED

#include "../core/RootModule.hpp"

#include <vector>
#include <thread>

#include "WorkPlan.hpp"



namespace mcc
{
  /** \brief Base class for root modules where dividing of work is required.
   *         Useful for grid search as an example.
   */
  class WorkPlanRootModule : public RootModule
  {
  public:
    /** Initializes base RootModule class and instance variables.
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit WorkPlanRootModule(const GlobalSettings& settings);

    /** \brief Gives work from workQueue (to a thread). This method is
     *         protected by a mutex for thread safety. Will return false if no
     *         more work is available.
     *
     *  \param outWorkPlan WorkPlan object to assign new work plan to
     *  \return True if new work is available (and has been assigned) or
     *          false otherwise
     */
    bool giveWork(WorkPlan& outWorkPlan);

    /** \brief Returns work queue size and work index meant to be used by
     *         interfaces displaying calculation progress.
     *
     *  \param outWorkQueueSize Reference to variable to store the value
     *  \param outWorkIndex Reference to variable to store the value
     */
    virtual void getWorkQueueInformation(int& outWorkQueueSize,
                                         long unsigned int& outWorkIndex)
                                         const;

  protected:
    /** \brief Queue of jobs for threads */
    std::vector<WorkPlan> workQueue_;
    /** \brief Collection of created threads */
    std::vector<std::thread> threads_;
    /** \brief Controls access to workQueue */
    std::mutex workQueueMutex_;
    /** \brief Points at the next job in workQueue_ to be given to a thread. */
    long unsigned int workIndex_;
  };
}



#endif // WORKPLANROOTMODULE_HPP_INCLUDED
