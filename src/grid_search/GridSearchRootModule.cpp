#include "GridSearchRootModule.hpp"

#include <chrono>
#include <iostream>
#include <cmath>

#include "CellTracker.hpp"
#include "GridPlanProgressInterface.hpp"
#include "../core/constants.hpp"
#include "../io/DataExtractor.hpp"
#include "../peak_list/DynamicObservedList.hpp"



namespace mcc
{
  GridSearchRootModule::GridSearchRootModule(const GlobalSettings& settings) :
                        WorkPlanRootModule{settings}
  {
    // Initialize from settings
    minA_ = globalSettings_.cell_minA;
    minB_ = globalSettings_.cell_minB;
    minC_ = globalSettings_.cell_minC;
    minAlpha_ = globalSettings_.cell_minAlpha;
    minBeta_ = globalSettings_.cell_minBeta;
    minGamma_ = globalSettings_.cell_minGamma;
    maxA_ = globalSettings_.cell_maxA;
    maxB_ = globalSettings_.cell_maxB;
    maxC_ = globalSettings_.cell_maxC;
    maxAlpha_ = globalSettings_.cell_maxAlpha;
    maxBeta_ = globalSettings_.cell_maxBeta;
    maxGamma_ = globalSettings_.cell_maxGamma;
    crystalSystem_ = globalSettings_.cell_crystalSystem;
    lengthStep_ = globalSettings_.gridSearch_lengthStep;
    angleStep_ = globalSettings_.gridSearch_angleStep;
    workAreaDivider_ = globalSettings_.parallel_workAreaDivider;
    cellTrackerEnabled_ = globalSettings_.logging_cellTracker;

    // Generate work plans
    generateWorkPlans();
  }



  GridSearchRootModule::~GridSearchRootModule()
  {
    const int n = gridSearchControllers_.size();
    for (int i = 0; i < n; ++i) {
      delete gridSearchControllers_[i];
    }
  }



  void GridSearchRootModule::spawnThreads()
  {
    // Extract observed peaks
    DynamicObservedList observedList{globalSettings_};
    {
      bool result;
      DataExtractor extractor{globalSettings_};
      result = extractor.extractData(observedList);

      if (result == false) {
        std::cout << "Extracting observed peaks failed. Calculations can not "
                     "begin." << std::endl;
        return;        
      }
    }

    // Start threads
    for (int i = 0; i < nThreads_; ++i) {
      std::string threadName {"thread" + std::to_string(i)};
      GridSearchController* controller =
        new GridSearchController{*this,
                                 globalSettings_,
                                 threadName,
                                 observedList};
      gridSearchControllers_.push_back(controller);
      threads_.push_back(controller->spawnThread());
    }

    // Display progress
    GridPlanProgressInterface progress{*this};
    progress.initializeInterface();
    while (true) {
      bool threadsInactive = true;
      // Continue updating if at least one thread is still working
      for (const GridSearchController* gsc : gridSearchControllers_) {
        if (gsc->isThreadActive()) {
          threadsInactive = false;
          break;
        }
      }

      if (threadsInactive)
        break;
      else {
        progress.updateInterface();
        std::this_thread::sleep_for(
                         std::chrono::milliseconds(INTERFACE_UPDATE_INTERVAL));
      }
    }

    // Join threads
    for (std::thread& thread : threads_) {
      thread.join();
    }

    // Write solutions to file
    solutions_->writeToFile(Constants::solutionFilePath);

    // Cell tracker merge, sort and write to file
    if (cellTrackerEnabled_ > 0) {
      CellTracker ct;

      std::cout << "Merging thread cell tracker data ..." << std::endl;
      for (GridSearchController* gsc : gridSearchControllers_) {
        CellTracker& ctThread {gsc->getCellTrackerReference()};
        ct.mergeData(ctThread);
      }

      // Sorting not operational yet
      // ct.sortData();
      std::cout << "Writing cell tracker data to file ..." << std::endl;
      ct.writeToFile(Constants::trackedDataFilePath);
    }
  }



  void GridSearchRootModule::signalInterrupt()
  {
    for (GridSearchController* gsController : gridSearchControllers_) {
      gsController->signalInterrupt();
    }
  }



  void GridSearchRootModule::generateWorkPlans()
  {
    dividerGenerateWorkPlans();
  }



  void GridSearchRootModule::dividerGenerateWorkPlans()
  {
    // Determine number of variables for current crystal system
    int nVariables = 1;
    switch (crystalSystem_) {
      case CrystalSystem::CUBIC:
        nVariables = 1;
        break;
      case CrystalSystem::HEXAGONAL:
      case CrystalSystem::TETRAGONAL:
        nVariables = 2;
        break;
      case CrystalSystem::ORTHORHOMBIC:
        nVariables = 3;
        break;
      case CrystalSystem::MONOCLINIC:
        nVariables = 4;
        break;
      case CrystalSystem::TRICLINIC:
        nVariables = 6;
        break;
    }
    // Calculate nth root of workAreaDivider_ where n is number of variables.
    // This should result in approximately workAreaDivider_ work plans.
    int divider {static_cast<int>(
                       std::floor(
                       std::pow(workAreaDivider_, 1.0 /
                                static_cast<double>(nVariables))
                       ))};

    double aChunk {(maxA_ - minA_) / divider};
    double bChunk {(maxB_ - minB_) / divider};
    double cChunk {(maxC_ - minC_) / divider};
    double alphaChunk {(maxAlpha_ - minAlpha_) / divider};
    double betaChunk {(maxBeta_ - minBeta_) / divider};
    double gammaChunk {(maxGamma_ - minGamma_) / divider};

    if (aChunk < lengthStep_) aChunk = lengthStep_;
    if (bChunk < lengthStep_) bChunk = lengthStep_;
    if (cChunk < lengthStep_) cChunk = lengthStep_;
    if (alphaChunk < angleStep_) alphaChunk = angleStep_;
    if (betaChunk < angleStep_) betaChunk = angleStep_;
    if (gammaChunk < angleStep_) gammaChunk = angleStep_;

    WorkPlan wp;
    wp.crystalSystem = crystalSystem_;

    switch (crystalSystem_) {
        case CrystalSystem::TRICLINIC:
          for (int iA {0}; iA < divider; ++iA) {
            wp.aMin = minA_ + iA * aChunk;
            wp.aMax = minA_ + (iA + 1) * aChunk - lengthStep_;
            if (wp.aMax <= wp.aMin)
              wp.aMax = wp.aMin;

            for (int iB {0}; iB < divider; ++iB) {
              wp.bMin = minB_ + iB * bChunk;
              wp.bMax = minB_ + (iB + 1) * bChunk - lengthStep_;
              if (wp.bMax <= wp.bMin)
                wp.bMax = wp.bMin;
              for (int iC {0}; iC < divider; ++iC) {
                wp.cMin = minC_ + iC * cChunk;
                wp.cMax = minC_ + (iC + 1) * cChunk - lengthStep_;
                if (wp.cMax <= wp.cMin)
                  wp.cMax = wp.cMin;
                for (int iAlpha {0}; iAlpha < divider; ++iAlpha) {
                  wp.alphaMin = minAlpha_ + iAlpha * alphaChunk;
                  wp.alphaMax = minAlpha_ + (iAlpha + 1) * alphaChunk -
                                angleStep_;
                  if (wp.alphaMax <= wp.alphaMin)
                    wp.alphaMax = wp.alphaMin;
                  for (int iBeta {0}; iBeta < divider; ++iBeta) {
                    wp.betaMin = minBeta_ + iBeta * betaChunk;
                    wp.betaMax = minBeta_ + (iBeta + 1) * betaChunk -
                                 angleStep_;
                    if (wp.betaMax <= wp.betaMin)
                      wp.betaMax = wp.betaMin;
                    for (int iGamma {0}; iGamma < divider; ++iGamma) {
                      wp.gammaMin = minGamma_ + iGamma * gammaChunk;
                      wp.gammaMax = minGamma_ + (iGamma + 1) * gammaChunk -
                                    angleStep_;
                      if (wp.gammaMax <= wp.gammaMin) {
                        wp.gammaMax = wp.gammaMin;
                        workQueue_.push_back(wp);
                        break;
                      }
                      workQueue_.push_back(wp);
                    }
                    if (wp.betaMax <= wp.betaMin)
                      break;
                  }
                  if (wp.alphaMax == wp.alphaMin)
                    break;
                }
                if (wp.cMax == wp.cMin)
                  break;
              }
              if (wp.bMax == wp.bMin)
                break;
            }
            if (wp.aMax == wp.aMin)
              break;
          }
          break;

        case CrystalSystem::MONOCLINIC:
          for (int iA {0}; iA < divider; ++iA) {
            wp.aMin = minA_ + iA * aChunk;
            wp.aMax = minA_ + (iA + 1) * aChunk - lengthStep_;
            if (wp.aMax <= wp.aMin)
              wp.aMax = wp.aMin;

            for (int iB {0}; iB < divider; ++iB) {
              wp.bMin = minB_ + iB * bChunk;
              wp.bMax = minB_ + (iB + 1) * bChunk - lengthStep_;
              if (wp.bMax <= wp.bMin)
                wp.bMax = wp.bMin;
              for (int iC {0}; iC < divider; ++iC) {
                wp.cMin = minC_ + iC * cChunk;
                wp.cMax = minC_ + (iC + 1) * cChunk - lengthStep_;
                if (wp.cMax <= wp.cMin)
                  wp.cMax = wp.cMin;
                for (int iBeta {0}; iBeta < divider; ++iBeta) {
                  wp.betaMin = minBeta_ + iBeta * betaChunk;
                  wp.betaMax = minBeta_ + (iBeta + 1) * betaChunk - angleStep_;
                  if (wp.betaMax <= wp.betaMin) {
                    wp.betaMax = wp.betaMin;
                    workQueue_.push_back(wp);
                    break;
                  }
                  workQueue_.push_back(wp);
                }
                if (wp.cMax == wp.cMin)
                  break;
              }
              if (wp.bMax == wp.bMin)
                break;
            }
            if (wp.aMax == wp.aMin)
              break;
          }
          break;

        case CrystalSystem::ORTHORHOMBIC:
          for (int iA {0}; iA < divider; ++iA) {
            wp.aMin = minA_ + iA * aChunk;
            wp.aMax = minA_ + (iA + 1) * aChunk - lengthStep_;
            if (wp.aMax <= wp.aMin)
              wp.aMax = wp.aMin;

            for (int iB {0}; iB < divider; ++iB) {
              wp.bMin = minB_ + iB * bChunk;
              wp.bMax = minB_ + (iB + 1) * bChunk - lengthStep_;
              if (wp.bMax <= wp.bMin)
                wp.bMax = wp.bMin;

              for (int iC {0}; iC < divider; ++iC) {
                wp.cMin = minC_ + iC * cChunk;
                wp.cMax = minC_ + (iC + 1) * cChunk - lengthStep_;
                if (wp.cMax <= wp.cMin) {
                  wp.cMax = wp.cMin;
                  workQueue_.push_back(wp);
                  break;
                }

                workQueue_.push_back(wp);
              }
              if (wp.bMax == wp.bMin)
                break;
            }
            if (wp.aMax == wp.aMin)
              break;
          }
          break;

        case CrystalSystem::TETRAGONAL:
        case CrystalSystem::HEXAGONAL:
          for (int iA {0}; iA < divider; ++iA) {
            wp.aMin = minA_ + iA * aChunk;
            wp.aMax = minA_ + (iA + 1) * aChunk - lengthStep_;
            if (wp.aMax <= wp.aMin)
              wp.aMax = wp.aMin;

            for (int iC {0}; iC < divider; ++iC) {
              wp.cMin = minC_ + iC * cChunk;
              wp.cMax = minC_ + (iC + 1) * cChunk - lengthStep_;
              if (wp.cMax <= wp.cMin) {
                wp.cMax = wp.cMin;
                workQueue_.push_back(wp);
                break;
              }

              workQueue_.push_back(wp);
            }
            if (wp.aMax == wp.aMin)
              break;
          }
          break;

        case CrystalSystem::CUBIC:
          for (int i {0}; i < divider; ++i) {
            wp.aMin = minA_ + i * aChunk;
            wp.aMax = minA_ + (i + 1) * aChunk - lengthStep_;
            if (wp.aMax <= wp.aMin) {
              wp.aMax = wp.aMin;
              workQueue_.push_back(wp);
              break;
            }

            workQueue_.push_back(wp);
          }
          break;
      }
  }

}
