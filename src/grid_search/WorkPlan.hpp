/** \file WorkPlan.hpp
 *  \brief Declares class which contains boundaries for unit cell search.
 */

#ifndef WORKPLAN_HPP_INCLUDED
#define WORKPLAN_HPP_INCLUDED

#include "../core/CrystalSystem.hpp"



namespace mcc
{
  /** \brief Contains information about limits inside which unit cell search
   *         should be performed.
   */
  class WorkPlan
  {
  public:
    /** \brief Initialized all member variables to value of 0.0 and
     *         crystalSystem to orthorhombic.
     */
    WorkPlan();

    double aMin; /**< \brief Minimum value of cell parameter a [Å] */
    double bMin; /**< \brief Minimum value of cell parameter b [Å] */
    double cMin; /**< \brief Minimum value of cell parameter c [Å] */
    double alphaMin; /**< \brief Minimum value of cell parameter alpha [°] */
    double betaMin; /**< \brief Minimum value of cell parameter beta [°] */
    double gammaMin; /**< \brief Minimum value of cell parameter gamma [°] */

    double aMax; /**< \brief Maximum value of cell parameter a [Å] */
    double bMax; /**< \brief Maximum value of cell parameter b [Å] */
    double cMax; /**< \brief Maximum value of cell parameter c [Å] */
    double alphaMax; /**< \brief Maximum value of cell parameter alpha [°] */
    double betaMax; /**< \brief Maximum value of cell parameter beta [°] */
    double gammaMax; /**< \brief Maximum value of cell parameter gamma [°] */

    /** \brief Crystal system to perform search in */
    CrystalSystem crystalSystem;
  };
}



#endif // WORKPLAN_HPP_INCLUDED
