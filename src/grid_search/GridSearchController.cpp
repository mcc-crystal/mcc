#include "GridSearchController.hpp"

#include <sstream>

#include "../io/DataExtractor.hpp"



namespace mcc
{
  GridSearchController::GridSearchController(
                                          GridSearchRootModule& parent,
                                          const GlobalSettings& settings,
                                          const std::string& threadName,
                                          const ObservedPeakList& sourceList) :
                        ThreadedController{settings, threadName, sourceList},
                        root_{parent},
                        isThreadActive_{false},
                        unitCell_{nullptr},
                        worstEnergy_{0.0}
  {
    // Initialize from settings
    lengthStep_ = globalSettings_.gridSearch_lengthStep;
    angleStep_ = globalSettings_.gridSearch_angleStep;
    cellTrackerEnabled_ = globalSettings_.logging_cellTracker;
  }



  void GridSearchController::beginWork()
  {
    isThreadActive_ = true;
    beginCalculations();
    isThreadActive_ = false;
  }



  std::string GridSearchController::getModuleName() const
  {
    return "GridSearchController";
  }



  std::thread GridSearchController::spawnThread()
  {
    // return std::thread([=] {beginWork();});
    return std::thread(&GridSearchController::beginWork, this);
  }



  const CellTracker& GridSearchController::getCellTrackerReference() const
  {
    return cellTracker_;
  }



  CellTracker& GridSearchController::getCellTrackerReference()
  {
    return cellTracker_;
  }



  bool GridSearchController::isThreadActive() const
  {
    return isThreadActive_;
  }



  void GridSearchController::performGridSearchStep()
  {
    peakGenerator_->populatePeakList(*calculatedList_);
    calculatedList_->sortPeaks();
    calculatedList_->removeDuplicates();
    double e {energyCalculator_->calculateEnergy()};

    if (cellTrackerEnabled_ > 0)
      cellTracker_.submitCell(*unitCell_, e);

    if (e < worstEnergy_) {
      Solution s{*unitCell_, e};
      root_.submitSolution(s);
      worstEnergy_ = root_.getWorstEnergy();
    }
  }



  void GridSearchController::beginCalculations()
  {
    if (modulesInitialized_ == false) {
      std::stringstream message;
      message << "Modules not initialized, thread " << threadName_ <<
                 " terminating.";
      reportError(message.str());
      return;
    }

    // Check if observed peak list is populated
    if (observedList_->getPeakCount() == 0) {
      std::stringstream message;
      message << "Observed peak list is empty, thread " << threadName_ <<
                 " terminating.";
      reportError(message.str());
      return;
    }

    while (true) {
      // Check for interrupt signal
      if (noInterruptReceived_ == false) {
        reportMessage("Thread '" + threadName_ + "' received interrupt signal."
                      " Exiting.");
        break;
      }

      bool result {root_.giveWork(workPlan_)};
      // Check if there is no more work
      if (result == false) {
          std::stringstream message;
          message << "Thread " << threadName_ << " out of work, finishing up.";
          reportMessage(message.str());
          break;
      }

      // Construct correct UnitCell object according to the work plan
      switch (workPlan_.crystalSystem) {
        case CrystalSystem::CUBIC:
          unitCell_ = new CubicCell;
          break;
        case CrystalSystem::HEXAGONAL:
          unitCell_ = new HexagonalCell;
          break;
        case CrystalSystem::TETRAGONAL:
          unitCell_ = new TetragonalCell;
          break;
        case CrystalSystem::ORTHORHOMBIC:
          unitCell_ = new OrthorhombicCell;
          break;
        case CrystalSystem::MONOCLINIC:
          unitCell_ = new MonoclinicCell;
          break;
        case CrystalSystem::TRICLINIC:
          unitCell_ = new TriclinicCell;
          break;
      }
      peakGenerator_->setUnitCell(unitCell_);

      worstEnergy_ = root_.getWorstEnergy();

      switch (workPlan_.crystalSystem) {
        case CrystalSystem::CUBIC:
          for (double a = workPlan_.aMin;
               a <= workPlan_.aMax;
               a += lengthStep_){
            unitCell_->a = a;
            performGridSearchStep();
          }
          break;
        case CrystalSystem::HEXAGONAL:
        case CrystalSystem::TETRAGONAL:
          for (double a = workPlan_.aMin;
               a <= workPlan_.aMax;
               a += lengthStep_){
            unitCell_->a = a;
            for (double c = workPlan_.cMin;
                 c <= workPlan_.cMax;
                 c += lengthStep_) {
              unitCell_->c = c;
              performGridSearchStep();
            }
          }
          break;
        case CrystalSystem::ORTHORHOMBIC:
          for (double a = workPlan_.aMin;
               a <= workPlan_.aMax;
               a += lengthStep_){
            unitCell_->a = a;
            for (double b = workPlan_.bMin;
                 b <= workPlan_.bMax;
                 b += lengthStep_) {
              unitCell_->b = b;
              for (double c = workPlan_.cMin;
                   c <= workPlan_.cMax;
                   c += lengthStep_) {
                unitCell_->c = c;
                performGridSearchStep();
              }
            }
          }
          break;
        case CrystalSystem::MONOCLINIC:
          for (double a = workPlan_.aMin;
               a <= workPlan_.aMax;
               a += lengthStep_){
            unitCell_->a = a;
            for (double b = workPlan_.bMin;
                 b <= workPlan_.bMax;
                 b += lengthStep_) {
              unitCell_->b = b;
              for (double c = workPlan_.cMin;
                   c <= workPlan_.cMax;
                   c += lengthStep_) {
                unitCell_->c = c;
                for (double beta = workPlan_.betaMin;
                     beta <= workPlan_.betaMax;
                     beta += angleStep_) {
                  unitCell_->beta = beta;
                  performGridSearchStep();
                }
              }
            }
          }
          break;
        case CrystalSystem::TRICLINIC:
          for (double a = workPlan_.aMin;
               a <= workPlan_.aMax;
               a += lengthStep_){
            unitCell_->a = a;
            for (double b = workPlan_.bMin;
                 b <= workPlan_.bMax;
                 b += lengthStep_) {
              unitCell_->b = b;
              for (double c = workPlan_.cMin;
                   c <= workPlan_.cMax;
                   c += lengthStep_) {
                unitCell_->c = c;
                for (double alpha = workPlan_.alphaMin;
                     alpha <= workPlan_.alphaMax;
                     alpha += angleStep_) {
                  unitCell_->alpha = alpha;
                  for (double beta = workPlan_.betaMin;
                       beta <= workPlan_.betaMax;
                       beta += angleStep_) {
                    unitCell_->beta = beta;
                    for (double gamma = workPlan_.gammaMin;
                         gamma <= workPlan_.gammaMax;
                         gamma += angleStep_) {
                      unitCell_->gamma = gamma;
                      performGridSearchStep();
                    }
                  }
                }
              }
            }
          }
          break;
      }

      // Free allocated memory
      peakGenerator_->setUnitCell(nullptr);
      delete unitCell_;
    }
  }

}
