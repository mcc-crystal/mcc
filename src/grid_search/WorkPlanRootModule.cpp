#include "WorkPlanRootModule.hpp"



namespace mcc
{
  WorkPlanRootModule::WorkPlanRootModule(const GlobalSettings& settings) :
                      RootModule{settings},
                      workIndex_{0}
  { }



  bool WorkPlanRootModule::giveWork(WorkPlan& outWorkPlan)
  {
    if (workIndex_ < workQueue_.size()) {
      workQueueMutex_.lock();

      outWorkPlan = workQueue_[workIndex_];
      workIndex_++;

      workQueueMutex_.unlock();
      return true;
    }
    else {
      return false;
    }
  }



  void WorkPlanRootModule::getWorkQueueInformation(
                                         int& outWorkQueueSize,
                                         long unsigned int& outWorkIndex) const
  {
    outWorkQueueSize = workQueue_.size();
    outWorkIndex = workIndex_;
  }
}
