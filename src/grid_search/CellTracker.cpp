#include "CellTracker.hpp"

#include <iostream>
#include <exception>
#include <iomanip>



namespace mcc
{
  // Static initialization
  const int CellTracker::PARAM_WIDTH = 8;
  const int CellTracker::PARAM_PRECISION = 4;



  void CellTracker::submitCell(const UnitCell& cell, double energy)
  {
    Solution s{cell, energy};
    solutions_.push_back(s);
  }



  void CellTracker::writeToFile(std::string filePath) const
  {
    std::ofstream file;

    try {
      file.open(filePath, std::ios::out | std::ios::trunc);
      if (file.is_open()) {
        writeHeaderLine(file);
        const int nSolutions {static_cast<int>(solutions_.size())};
        for (int i = 0; i < nSolutions; ++i) {
          writeSolutionLine(i, file);
        }
      }
      else {
        std::cout << "Error while opening file '" << filePath << "'." <<
                     std::endl;
      }
    }
    catch (std::exception& e) {
      std::cout << "Exception occurred while writing tracked cell data to "
                   "file '" << filePath << "': " << e.what() << std::endl;
    }

    if (file.is_open())
      file.close();
  }



  void CellTracker::mergeData(CellTracker& source)
  {
    copySolutions(source);
    source.clearData();
  }



  void CellTracker::clearData()
  {
    solutions_.clear();
  }



  const std::vector<Solution>& CellTracker::getSolutionStorageReference() const
  {
    return solutions_;
  }



  void CellTracker::writeSolutionLine(int i,
                                      std::ostream& stream) const
  {
    const Solution& s = solutions_[i];

    const double* parameters[] = {&s.a, &s.b, &s.c, &s.alpha, &s.beta,
                                  &s.gamma, &s.energy, nullptr};

    for (int i = 0; parameters[i] != nullptr; ++i) {
      stream << std::fixed;
      stream << std::setw(PARAM_WIDTH) << std::setprecision(PARAM_PRECISION) <<
                *parameters[i] << " ";
    }
    stream << std::endl;
  }



  void CellTracker::writeHeaderLine(std::ostream& stream) const
  {
    const std::string parameters[] = {"a [A]", "b [A]", "c [A]", "alpha",
                                      "beta", "gamma", "E", ""};

    for (int i = 0; parameters[i].empty() == false; ++i) {
      stream << std::fixed;
      stream << std::setw(PARAM_WIDTH) << parameters[i] << " ";
    }
    stream << std::endl;
  }



  void CellTracker::copySolutions(const CellTracker& source)
  {
    const std::vector<Solution>& sourceSolutions {
                                 source.getSolutionStorageReference()};
    // Copy solutions
    solutions_.reserve(solutions_.size() + sourceSolutions.size());
    solutions_.insert(solutions_.end(),
                      sourceSolutions.begin(),
                      sourceSolutions.end());
  }
}
