/** \file GridSearchRootModule.hpp
 *  \brief Declares root module for control of multiple-thread grid search.
 */

#ifndef GRIDSEARCHROOTMODULE_HPP_INCLUDED
#define GRIDSEARCHROOTMODULE_HPP_INCLUDED

#include "WorkPlanRootModule.hpp"

#include "GridSearchController.hpp"



namespace mcc
{
  // Forward declaration
  class GridSearchController;

  /** \brief Root module for control of multiple-thread grid search.
   */
  class GridSearchRootModule : public WorkPlanRootModule
  {
  public:
    /** Initializes the module and copies relevant setting from the shared
     *  object.
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit GridSearchRootModule(const GlobalSettings& settings);

    virtual ~GridSearchRootModule();

    void spawnThreads() override;

    void signalInterrupt() override;


  protected:
    /** \brief Collection of grid search controllers the threads use */
    std::vector<GridSearchController*> gridSearchControllers_;
    /** \brief Minimum cell parameter a from settings [Å] */
    double minA_;
    /** \brief Minimum cell parameter b from settings [Å] */
    double minB_;
    /** \brief Minimum cell parameter c from settings [Å] */
    double minC_;
    /** \brief Minimum cell parameter alpha from settings [°] */
    double minAlpha_;
    /** \brief Minimum cell parameter beta from settings [°] */
    double minBeta_;
    /** \brief Minimum cell parameter gamma from settings [°] */
    double minGamma_;
    /** \brief Maximum cell parameter a from settings [Å] */
    double maxA_;
    /** \brief Maximum cell parameter b from settings [Å] */
    double maxB_;
    /** \brief Maximum cell parameter c from settings [Å] */
    double maxC_;
    /** \brief Maximum cell parameter alpha from settings [°] */
    double maxAlpha_;
    /** \brief Maximum cell parameter beta from settings [°] */
    double maxBeta_;
    /** \brief Maximum cell parameter gamma from settings [°] */
    double maxGamma_;
    /** \brief Crystal system of the unit cell from settings */
    CrystalSystem crystalSystem_;
    /** \brief Grid search length step from settings */
    double lengthStep_;
    /** \brief Grid search angle step from settings */
    double angleStep_;
    /** \brief In roughly how many pieces work area will be divided */
    int workAreaDivider_;
    /** \brief CellTracker will be enabled if > 0 */
    int cellTrackerEnabled_;

    /** \brief Generates work plans based on settings and fills the work queue.
     * 
     *  This is a wrapper method. It relies on another method to provide an
     *  actual implementation of work plan generation.
     */
    void generateWorkPlans();

    /** \brief Implementation of work plan generation: generates by dividing
     *         whole work area in predetermined number of equally sized pieces.
     */
    void dividerGenerateWorkPlans();
  };
}



#endif // GRIDSEARCHROOTMODULE_HPP_INCLUDED
