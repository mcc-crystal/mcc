/** \file CellTracker.hpp
 *  \brief Declares class for tracking unit cell states.
 */

#ifndef CELLTRACKER_HPP_INCLUDED
#define CELLTRACKER_HPP_INCLUDED

#include <vector>
#include <fstream>
#include <string>

#include "../solution/Solution.hpp"
#include "../core/UnitCell.hpp"



namespace mcc
{
  /** \brief Tracks unit cell states and provides interface to write tracked
   *         data to a file.
   */
  class CellTracker
  {
  public:
    /** \brief Stores unit cell and its energy.
     *
     *  \param cell Unit cell to store
     *  \param energy Energy of the unit cell
     */
    virtual void submitCell(const UnitCell& cell, double energy);

    /** \brief Writes contained data to a file. Uses fixed column width format
     *         useful for plotting charts.
     *
     *  Output file is truncated prior to writing.
     *
     *  \param filePath Path to the file to write into
     */
    void writeToFile(std::string filePath) const;

    /** \brief Copies all stored solutions from the other CellTracker object
     *         into this object's storage and deletes data from the source
     *         object to release memory.
     *
     *  \param source Object to copy and delete data from
     */
    virtual void mergeData(CellTracker& source);

    /** \brief Deletes tracked cell data, releasing memory.
     */
    virtual void clearData();

    /** \brief Return const reference to this object's solution storage.
     *
     *  \return Const reference to solution storage
     */
    const std::vector<Solution>& getSolutionStorageReference() const;


  protected:
    /** \brief Parameter width used for formatted output */
    static const int PARAM_WIDTH;
    /** \brief Parameter precision used for formatted output */
    static const int PARAM_PRECISION;

    /** \brief Stores unit cell states */
    std::vector<Solution> solutions_;

    /** \brief Writes solution data in a formatted line.
     *
     *  \param i Index pointing to solution to write
     *  \param stream Stream to write to
     */
    virtual void writeSolutionLine(int i, std::ostream& stream) const;

    /** \brief Writes header, indicating what information is written in each
     *         column, in a formatted line. This method is called when writing
     *         solution data to a file.
     *
     *  \param stream Stream to write to
     */
    virtual void writeHeaderLine(std::ostream& stream) const;

    /** \brief Copies solutions from another CellTracker object into this one.
     *         Data is appended to any existing data in this object.
     *
     *  \param source Object to copy data from
     */
    void copySolutions(const CellTracker& source);
  };
}



#endif // CELLTRACKER_HPP_INCLUDED
