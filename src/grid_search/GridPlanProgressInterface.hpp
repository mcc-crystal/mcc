/** \file GridPlanProgressInterface.hpp
 *  \brief Declares simple progress interface for grid search
 */

#ifndef GRIDPLANPROGRESSINTERFACE_HPP_INCLUDED
#define GRIDPLANPROGRESSINTERFACE_HPP_INCLUDED

#include "../core/TextInterface.hpp"
#include "GridSearchRootModule.hpp"



namespace mcc
{
  /** \brief Simple progress interface for grid search using work plans to
   *         divide work between threads. Progress is calculated based on
   *         how many work plans have been picked up by threads.
   */
  class GridPlanProgressInterface : public TextInterface
  {
  public:
    /** Initializes the object with reference to the grid search root module.
     * 
     *  \param gridSearchRoot Reference to grid search root module
     */
    explicit GridPlanProgressInterface(GridSearchRootModule& gridSearchRoot);

    void initializeInterface() override;

    void updateInterface() override;


  protected:
    /** \brief Reference to grid search root module to display data of */
    GridSearchRootModule& gridSearchRoot_;
  };
}



#endif // GRIDPLANPROGRESSINTERFACE_HPP_INCLUDED
