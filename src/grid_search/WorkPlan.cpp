#include "WorkPlan.hpp"



namespace mcc
{
  WorkPlan::WorkPlan() :
            aMin {0.0},
            bMin {0.0},
            cMin {0.0},
            alphaMin {0.0},
            betaMin {0.0},
            gammaMin {0.0},
            aMax {0.0},
            bMax {0.0},
            cMax {0.0},
            alphaMax {0.0},
            betaMax {0.0},
            gammaMax {0.0},
            crystalSystem {CrystalSystem::ORTHORHOMBIC}
  { }
}
