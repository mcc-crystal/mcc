#include "GridPlanProgressInterface.hpp"

#include <iostream>
#include <sstream>
#include <iomanip>



namespace mcc
{
  GridPlanProgressInterface::GridPlanProgressInterface(
                                       GridSearchRootModule& gridSearchRoot) :
                             gridSearchRoot_{gridSearchRoot}
  { }



  void GridPlanProgressInterface::initializeInterface()
  {
    std::cout << "Grid search progress (work plans picked up):" << std::endl;
  }



  void GridPlanProgressInterface::updateInterface()
  {
    int workQueueSize;
    long unsigned int workIndex;
    gridSearchRoot_.getWorkQueueInformation(workQueueSize, workIndex);

    double workPercent {static_cast<double>(workIndex) /
                        static_cast<double>(workQueueSize) * 100.0};

    double bestEnergy {0.0};

    if (gridSearchRoot_.getSolutionCountSafe() > 0) {
      Solution s {gridSearchRoot_.getBestSolution()};
      bestEnergy = s.energy;
    }

    std::stringstream report;
    report << std::fixed << std::setw(8) << std::setprecision(2) <<
              workPercent << " % (" << workIndex << "/" << workQueueSize <<
              ")    Best E: " << std::setw(8) << std::setprecision(2) <<
              bestEnergy << std::endl;

    std::cout << report.str();
  }
}
