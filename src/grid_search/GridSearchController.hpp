/** \file GridSearchController.hpp
 *  \brief Declares threaded grid search controller class.
 */

#ifndef GRIDSEARCHCONTROLLER_HPP_INCLUDED
#define GRIDSEARCHCONTROLLER_HPP_INCLUDED

#include "../core/ThreadedController.hpp"

#include "WorkPlan.hpp"
#include "GridSearchRootModule.hpp"
#include "CellTracker.hpp"



namespace mcc
{
  // Forward declaration
  class GridSearchRootModule;

  /** \brief Controller for brute force grid search method. All unit cell
   *         possibilities defined by limits and steps are tried without any
   *         minimum seeking.
   */
  class GridSearchController : public ThreadedController
  {
  public:
    /** Initializes the object and ThreadedController base class. Copies
     *  settings from the shared object.
     * 
     *  \param parent Reference to the root module
     *  \param settings Reference to shared global settings object
     *  \param threadName Name to identify the thread by
     *  \param sourceList Observed peak list to copy peaks from
     */
    GridSearchController(GridSearchRootModule& parent,
                         const GlobalSettings& settings,
                         const std::string& threadName,
                         const ObservedPeakList& sourceList);

    void beginWork() override;

    std::string getModuleName() const override;

    std::thread spawnThread() override;

    /** \brief Returns const reference to this thread's CellTracker object.
     *
     *  \return Const reference to CellTracker object
     */
    const CellTracker& getCellTrackerReference() const;

    /** \brief Returns read-write reference to this thread's CellTracker
     *         object.
     *
     *  \return Reference to CellTracker object
     */
    CellTracker& getCellTrackerReference();

    bool isThreadActive() const override;


  protected:
    /** \brief Provides boundaries for the current search */
    WorkPlan workPlan_;
    /** \brief Reference to the root module to get work from */
    GridSearchRootModule& root_;
    /** \brief Energy of the worst solution in the storage of root object.
     *         Saved so there is no need to check on a shared object every
     *         time.
     */
    double worstEnergy_;
    /** \brief Grid search step for lengths [Å] */
    double lengthStep_;
    /** \brief Grid search step for angles [°] */
    double angleStep_;
    /** \brief Cell tracking enabled if > 0 */
    int cellTrackerEnabled_;
    /** \brief Whether thread is doing calculations */
    bool isThreadActive_;
    /** \brief Reference to unit cell object providing cell parameters used by
     *         this thread.
     */
    UnitCell* unitCell_;

    /** \brief Performs cell tracking if enabled in settings */
    CellTracker cellTracker_;

    /** \brief Calculates peaks and performs cell evaluation for a single
     *         unit cell. If its energy is lower than the worst energy in the
     *         solution list, it is submitted to the solution list.
     */
    void performGridSearchStep();

    /** \brief A protected wrapper for functionality that should be offered by
     *         beginWork(). It allows beginWork() to easily control status of
     *         isThreadActive_.
     */
    void beginCalculations();
  };
}



#endif // GRIDSEARCHCONTROLLER_HPP_INCLUDED
