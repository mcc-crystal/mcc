#include "ObservedPeakList.hpp"



namespace mcc
{
  ObservedPeakList::ObservedPeakList(const GlobalSettings& settings) :
                    PeakList{settings}
  {
    // Initialize from settings
    dataFilePath_ = globalSettings_.main_dataFile;
    dataFileFormat_ = globalSettings_.main_dataFormat;
  }
}
