#include "DynamicObservedList.hpp"

#include <cmath>



namespace mcc
{
  DynamicObservedList::DynamicObservedList(const GlobalSettings& settings) :
                       ObservedPeakList{settings},
                       heapsort_{peaks_}
  { }



  std::string DynamicObservedList::getModuleName() const
  {
    return "DynamicObservedList";
  }



  int DynamicObservedList::getPeakCount() const
  {
    return peaks_.size();
  }



  void DynamicObservedList::sortPeaks()
  {
    // Sort in order of ascending 2Theta
    heapsort_.sortAscending();
  }



  void DynamicObservedList::addPeak(const Peak& peak)
  {
    peaks_.push_back(peak);
  }



  void DynamicObservedList::removeDuplicates()
  {
    std::vector<Peak> temp{peaks_};
    peaks_.clear();
    peaks_.reserve(temp.size());

    const int peakCount {static_cast<int>(temp.size())};
    // Initially set to 2Theat which is impossible to measure
    double currentTwoTheta {0.0};

    for (int i = 0; i < peakCount; ++i) {
      const Peak& p {temp[i]};
      if (p.twoTheta == currentTwoTheta) continue;
      else {
        currentTwoTheta = p.twoTheta;
        peaks_.push_back(p);
      }
    }
  }



  int DynamicObservedList::findClosestPeak(double twoTheta) const
  {
    return linearPeakSearch(twoTheta);
  }



  void DynamicObservedList::copyPeakPositions(
                                           const std::vector<double>& source)
  {
    peaks_.clear();

    Peak p;
    p.intensity = 0;

    for (double twoTheta : source) {
      p.twoTheta = twoTheta;
      peaks_.push_back(p);
    }
  }



  double DynamicObservedList::getMaxTwoTheta() const
  {
    if (peaks_.empty())
      return 0.0;

    return peaks_.back().twoTheta;
  }



  const Peak& DynamicObservedList::getPeak(int iPeak) const
  {
    return peaks_[iPeak];
  }



  void DynamicObservedList::clear()
  {
    peaks_.clear();
  }



  void DynamicObservedList::copyData(const ObservedPeakList& source)
  {
    peaks_.clear();
    const int N {source.getPeakCount()};

    for (int i {0}; i < N; ++i) {
      const Peak& p {source.getPeak(i)};
      peaks_.push_back(p);
    }
  }



  int DynamicObservedList::linearPeakSearch(double twoTheta) const
  {
    const unsigned long int peakCount {peaks_.size()};
    if (peakCount == 0) return -1;
    if (peakCount == 1) return 0;

    // Negative difference: current peak has lower 2Theta.
    // Positive difference: current peak has higher 2Theta.
    double lastDifference;
    double difference;

    // Iteration for i = 0
    lastDifference = peaks_[0].twoTheta - twoTheta;

    // Check for first 2Theta already being too high
    if (lastDifference > 0.0) return 0;

    for (int i = 1; i < peakCount; ++i) {
      const Peak& p {peaks_[i]};
      difference = p.twoTheta - twoTheta;

      if (lastDifference * difference < 0.0) {
        if (std::fabs(lastDifference) < std::fabs(difference)) return i - 1;
        else return i;
      }
    }

    // Last peak still has too low 2Theta
    return peakCount - 1;
  }
}
