/** \file DynamicCalculatedList.hpp
 *  \brief Declares calculated peak list module using a dynamic collection to
 *         store peaks.
 */
#ifndef DYNAMICPEAKLIST_HPP_INCLUDED
#define DYNAMICPEAKLIST_HPP_INCLUDED

#include <vector>

#include "../peak_list/CalculatedPeakList.hpp"
#include "../core/Reflection.hpp"
#include "../core/Heapsort.hpp"



namespace mcc
{
  /** \brief Peak list module using dynamic array to store peaks. Contains
   *         comprehensive information about peaks since it stores Reflection
   *         objects.
   */
  class DynamicCalculatedList : public CalculatedPeakList
  {
  public:
    /** \brief Initializes base class and this object's members.
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit DynamicCalculatedList(const GlobalSettings& settings);

    std::string getModuleName() const override;

    int getPeakCount() const override;

    const Reflection& getReflection(int iReflection) const override;

    void addReflection(const Reflection& reflection) override;

    void sortPeaks() override;

    void removeDuplicates() override;

    int findClosestPeak(double twoTheta) const override;

    void copyPeakPositions(const std::vector<double>& source) override;

    /** \brief Deletes all stored reflection and reallocates number of
     *         elements defined by a constant.
     * 
     *  This is an optimization to require less memory reallocations when
     *  the list is being populated.
     */
    void clear() override;


  protected:
    /** \brief Memory for how many Reflection object should be preallocated
     *         by the reflections_ dynamic collection.
     */
    static const int N_PREALLOCATED_REFLECTIONS;

    /** \brief Dynamic array to store the reflections */
    std::vector<Reflection> reflections_;
    /** \brief Object to heapsort reflections */
    Heapsort<Reflection> heapsort_;

    /** \brief Searches for the peak with closest matching 2Theta using simple
     *         linear search. Not optimized for speed.
     *
     *  \param twoTheta Target 2Theta
     *  \return Index of the closest matching peak
     */
    int linearPeakSearch(double twoTheta) const;
  };
}



#endif // DYNAMICPEAKLIST_HPP_INCLUDED
