/** \file PeakList.hpp
 *  \brief Declares base class for peak list modules.
 */
#ifndef PEAKLIST_HPP_INCLUDED
#define PEAKLIST_HPP_INCLUDED

#include <vector>

#include "../core/Module.hpp"



namespace mcc
{
  /** \brief Base class for lists of observed and calculated peaks.
   */
  class PeakList : public Module
  {
  public:
    /** Initializes the Module base class.
     * 
     *  \param settings Reference to shared global settings object
     */
    PeakList(const GlobalSettings& settings);

    /** \brief Clears the peak list (deletes all peaks).
     */
    virtual void clear() = 0;

    /** \brief Returns number of peaks in the list.
     *
     *  \return Number of peaks in the list
     */
    virtual int getPeakCount() const = 0;

    /** \brief Sorts peaks in the list. Method depends on specific object
     *         implementation.
     */
    virtual void sortPeaks() = 0;

    /** \brief Removes duplicate peaks.
     * 
     *  \warning Implementation methods assume that peaks are sorted! Sort
     *           peaks before calling this method.
     */
    virtual void removeDuplicates() = 0;

    /** \brief Returns the index of the peak with the closest 2Theta position
     *         to the argument. If there are no peaks in the list, -1 is
     *         returned.
     *
     *  Implementation of search left to the module.
     * 
     *  \warning Peak list must be sorted prior to use!
     *
     *  \param twoTheta 2Theta position to be compared
     *  \return Index of the closest peak or -1
     */
    virtual int findClosestPeak(double twoTheta) const = 0;

    /** \brief Initializes the peak list with data from the source. Source
     *         should be a list of 2Theta positions [°].
     *
     *  This method should only be used for testing purposes. It does not
     *  provide additional data that lists can contain like reflection indices,
     *  interplanar distances and intensities.
     *
     *  \param source Source list of 2Theta positions
     */
    virtual void copyPeakPositions(const std::vector<double>& source) = 0;
  };
}



#endif // PEAKLIST_HPP_INCLUDED
