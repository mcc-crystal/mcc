/** \file CalculatedPeakList.hpp
 *  \brief Declares base class for calculated peak lists
 */

#ifndef CALCULATEDPEAKLIST_HPP_INCLUDED
#define CALCULATEDPEAKLIST_HPP_INCLUDED

#include "PeakList.hpp"

#include "../core/Reflection.hpp"



namespace mcc
{
  /** \brief Base class for lists of calculated peaks, which use peak
   *         generators to populate themselves.
   */
  class CalculatedPeakList : public PeakList
  {
  public:
    /** Initializes base PeakList class.
     * 
     *  \param globalSettings Reference to shared global settings object
     */
    CalculatedPeakList(const GlobalSettings& globalSettings);

    /** \brief Returns const reference to reflection object containing more
     *         information about the calculated peak.
     * 
     *  \warning Method does not check for index out of bounds.
     *
     *  \param iReflection Index of the reflection
     *  \return Const reference to the Reflection object
     */
    virtual const Reflection& getReflection(int iReflection) const = 0;

    /** \brief Adds a Reflection object to the peak list.
     *
     *  \param reflection Reflection object to add
     */
    virtual void addReflection(const Reflection& reflection) = 0;
  };
}



#endif // CALCULATEDPEAKLIST_HPP_INCLUDED
