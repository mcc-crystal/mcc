/** \file ObservedPeakList.hpp
 *  \brief Declares base class for lists of observed peaks.
 */
#ifndef OBSERVEDLIST_HPP_INCLUDED
#define OBSERVEDLIST_HPP_INCLUDED

#include <string>

#include "../peak_list/PeakList.hpp"

#include "../core/Peak.hpp"



namespace mcc
{
  /** \brief Base class for lists of observed peaks.
   */
  class ObservedPeakList : public PeakList
  {
  public:
    /** \brief Initializes the base class and copies relevant settings from
     *         shared settings object.
     * 
     *  \param settings Reference to shared global settings object
     */
    ObservedPeakList(const GlobalSettings& settings);

    /** \brief Returns a Peak object from the list.
     * 
     *  \warning Method does not check for index out of bounds.
     *
     *  \param iPeak Index of the observed peak
     *  \return Const reference to a Peak object
     */
    virtual const Peak& getPeak(int iPeak) const = 0;

    /** \brief Adds a peak to the list.
     *
     *  \param peak Peak to add
     */
    virtual void addPeak(const Peak& peak) = 0;

    /** \brief Returns maximum 2Theta [°] (from the last peak in the list).
     * 
     *  \warning Assumes that the peak list is sorted!
     *
     *  \return Maximum 2Theta [°]
     */
    virtual double getMaxTwoTheta() const = 0;

    /** \brief Copies peak data from another ObservedPeakList. This list is
     *         cleared before copying the data.
     * 
     *  \param source Observed peak list to copy data from
     */
    virtual void copyData(const ObservedPeakList& source) = 0;


  protected:
    /** \brief Path to the data file from global settings */
    std::string dataFilePath_;
    /** \brief Format of the data file from global settings */
    std::string dataFileFormat_;
    
  };
}



#endif // OBSERVEDLIST_HPP_INCLUDED
