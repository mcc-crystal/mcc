/** \file DynamicObservedList.hpp
 *  \brief Declares DynamicObservedList class which uses a dynamic collection
 *         to store observed peaks.
 */
#ifndef DYNAMICOBSERVEDLIST_HPP_INCLUDED
#define DYNAMICOBSERVEDLIST_HPP_INCLUDED

#include <vector>

#include "../peak_list/ObservedPeakList.hpp"
#include "../core/Peak.hpp"
#include "../core/Heapsort.hpp"



namespace mcc
{
  /** \brief List of observed peaks which uses a dynamic collection to store
   *         observed peaks.
   */
  class DynamicObservedList : public ObservedPeakList
  {
  public:
    /** \brief Initializes base class and this object's members.
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit DynamicObservedList(const GlobalSettings& settings);

    std::string getModuleName() const override;

    int getPeakCount() const override;

    void sortPeaks() override;

    void addPeak(const Peak& peak) override;

    void removeDuplicates() override;

    int findClosestPeak(double twoTheta) const override;

    void copyPeakPositions(const std::vector<double>& source) override;

    double getMaxTwoTheta() const override;

    const Peak& getPeak(int iPeak) const override;

    void clear() override;

    void copyData(const ObservedPeakList& source) override;


  protected:
    /** \brief Dynamic collection of peaks */
    std::vector<Peak> peaks_;
    /** \brief Generic object used to sort peaks using heapsort method */
    Heapsort<Peak> heapsort_;

    /** \brief Searches for the peak with closest matching 2Theta using simple
     *         linear search. Not optimized for speed.
     *
     *  \param twoTheta Target 2Theta
     *  \return Index of the closest matching peak
     */
    int linearPeakSearch(double twoTheta) const;
  };
}



#endif // DYNAMICOBSERVEDLIST_HPP_INCLUDED
