#include "DynamicCalculatedList.hpp"

#include "../core/Reflection.hpp"
#include "../core/DTwoThetaConverter.hpp"

#include <cmath>



namespace mcc
{
  // Static constant initialization
  const int DynamicCalculatedList::N_PREALLOCATED_REFLECTIONS {200};



  DynamicCalculatedList::DynamicCalculatedList(const GlobalSettings& settings)
                         :
                         CalculatedPeakList{settings},
                         heapsort_{reflections_}
  { }



  std::string DynamicCalculatedList::getModuleName() const
  {
    return "DynamicCalculatedList";
  }



  int DynamicCalculatedList::getPeakCount() const
  {
    return reflections_.size();
  }



  const Reflection& DynamicCalculatedList::getReflection(int iReflection) const
  {
    return reflections_[iReflection];
  }



  void DynamicCalculatedList::addReflection(const Reflection& reflection)
  {
    reflections_.push_back(reflection);
  }



  void DynamicCalculatedList::sortPeaks()
  {
    // Sort in order of descending d
    heapsort_.sortDescending();
  }



  void DynamicCalculatedList::removeDuplicates()
  {
    std::vector<Reflection> temp{reflections_};
    reflections_.clear();
    reflections_.reserve(temp.size());

    const int reflectionCount {static_cast<int>(temp.size())};
    // Initially set to d that is impossible to calculate
    double currentD {0.0};

    for (int i = 0; i < reflectionCount; ++i) {
      const Reflection& r {temp[i]};
      if (r.d == currentD) continue;
      else {
        currentD = r.d;
        reflections_.push_back(r);
      }
    }
  }



  int DynamicCalculatedList::findClosestPeak(double twoTheta) const
  {
    return linearPeakSearch(twoTheta);
  }



  void DynamicCalculatedList::copyPeakPositions(
                                          const std::vector<double>& source)
  {
    reflections_.clear();

    Reflection r;
    r.h = 0;
    r.k = 0;
    r.l = 0;
    r.d = 0;

    for (double twoTheta : source) {
      r.twoTheta = twoTheta;
      reflections_.push_back(r);
    }
  }



  void DynamicCalculatedList::clear()
  {
    reflections_.clear();
    reflections_.reserve(N_PREALLOCATED_REFLECTIONS);
  }



  int DynamicCalculatedList::linearPeakSearch(double twoTheta) const
  {
    const unsigned long int peakCount {reflections_.size()};
    if (peakCount == 0) return -1;
    if (peakCount == 1) return 0;

    // Negative difference: current peak has lower 2Theta.
    // Positive difference: current peak has higher 2Theta.
    double lastDifference;

    // Iteration for i = 0
    lastDifference = reflections_[0].twoTheta - twoTheta;

    // Check for first 2Theta already being too high
    if (lastDifference > 0.0) return 0;

    for (int i = 1; i < peakCount; ++i) {
      const Reflection& r = reflections_[i];
      double difference {r.twoTheta - twoTheta};

      // Found calculated peaks that are immediate neighbours of the observed
      // peak. Exception is the first iteration (lower peak might not be
      // available).
      if (((i == 1) && (lastDifference >= 0.0)) ||
          (lastDifference * difference <= 0.0)) {
        if (std::fabs(lastDifference) < std::fabs(difference))
          return i - 1;
        else
          return i;
      }
      lastDifference = difference;
    }

    // Last peak still has too low 2Theta
    return peakCount - 1;
  }
}
