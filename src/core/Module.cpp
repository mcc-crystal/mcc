#include "Module.hpp"

#include <iostream>



namespace mcc
{
  Module::Module(const GlobalSettings& globalSettings) :
          globalSettings_{globalSettings}
  { }



  void Module::reportError(std::string message) const
  {
    std::string module {getModuleName()};
    std::cout << "ERROR in module " << module <<  ": " <<
                 message << std::endl;
  }



  void Module::reportWarning(std::string message) const
  {
    std::cout << "WARNING from module " << getModuleName() <<  ": " <<
                 message << std::endl;
  }



  void Module::reportMessage(std::string message) const
  {
    std::cout << getModuleName() <<  ": " << message << std::endl;
  }
}
