/** \file main_functions.hpp
 *  \brief Declares functions used in the main function (program entry point).
 */

#ifndef MAIN_FUNCTIONS_HPP_INCLUDED
#define MAIN_FUNCTIONS_HPP_INCLUDED

#include "GlobalSettings.hpp"
#include "RootModule.hpp"



namespace mcc
{
  /** \brief Reads the settings file specified in constants.hpp or an
   *         alternate one if the path is supplied.
   *
   *  \param outSettings GlobalSettings object to store settings that
   *                     are read
   *  \param path Optional path to the alternate configuration file. Default
   *              path is used if this argument is an empty string.
   *  \return True if no errors were encountered, false otherwise
   */
  bool readSettingsFile(GlobalSettings& outSettings, std::string path = "");

  /** \brief Performs grid search calculations based on settings.
   *
   *  Grid search module claims interrupt handling while it is used.
   *
   *  \param settings Global settings
   */
  void performGridSearch(const GlobalSettings& settings);

  /** \brief Performs Monte Carlo simulations based on settings.
   *
   *  Monte Carlo module claims interrupt handling while it is used.
   *
   *  \param settings Global settings
   */
  void performMonteCarlo(const GlobalSettings& settings);

  /** \brief Prints solutions from root module's storage to the console.
   *
   *  \param root Root module
   */
  void printSolutions(const RootModule& root);

  /** \brief Initializes root module based on settings and runs the
   *         calculations.
   *
   *  \param settings Global settings
   *  \return True if module was successfully initialized and ran,
   *          false if module was not initialized and ran
   */
  bool initializeAndRunRootModule(const GlobalSettings& settings);

  /** \brief Prints initial message when the program is started.
   */
  void printGreeting();

  /** \brief Writes default configuration file to the path expected for the
   *         configuration file to be located. Does not overwrite the file if
   *         it already exists.
   */
  void writeDefaultConfigurationFile();
}



#endif // MAIN_FUNCTIONS_HPP_INCLUDED
