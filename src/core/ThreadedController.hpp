/** \file ThreadedController.hpp
 *  \brief Declares base class for controllers meant to be run as threads.
 */

#ifndef THREADEDCONTROLLER_HPP_INCLUDED
#define THREADEDCONTROLLER_HPP_INCLUDED

#include "Module.hpp"

#include <thread>
#include <string>

#include "../peak_list/CalculatedPeakList.hpp"
#include "../peak_generation/PeakGenerator.hpp"
#include "../peak_list/ObservedPeakList.hpp"
#include "../energy/EnergyCalculator.hpp"
#include "UnitCell.hpp"



namespace mcc
{
  /** \brief Base class for top-level controllers that are meant to be run
   *         as a thread.
   */
  class ThreadedController : public Module
  {
  public:
    /** Initializes a new ThreadedController object and its modules. Copies
     *  observed peak data from source list.
     *  
     *  \param settings Reference to shared global settings object
     *  \param threadName Name to identify the thread by
     *  \param sourceList Observed peak list to copy peaks from
     */
    ThreadedController(const GlobalSettings& settings,
                       const std::string& threadName,
                       const ObservedPeakList& sourceList);

    ~ThreadedController();

    // Delete default methods to prevent troubles with pointers
    ThreadedController(const ThreadedController&) = delete;
    ThreadedController& operator= (const ThreadedController&) = delete;

    /** \brief Thread entry point method.
     */
    virtual void beginWork() = 0;

    /** \brief Starts work in a new thread and returns it.
     *
     *  \return New thread
     */
    virtual std::thread spawnThread() = 0;

    /** \brief Checks whether thread is performing calculations.
     *
     *  \return True if thread is performing calculations, false otherwise
     */
    virtual bool isThreadActive() const = 0;

    std::string getModuleName() const override;

    /** \brief Sets noInterruptReceived_ member to false and signals that
     *         calculations should stop. Derived classes may use additional
     *         measures to stop them.
     */
    virtual void signalInterrupt();


  protected:
    /** \brief Calculated peak list module used by this thread */
    CalculatedPeakList* calculatedList_;
    /** \brief Observed peak list module used by this thread */
    ObservedPeakList* observedList_;
    /** \brief Peak generator module used by this thread */
    PeakGenerator* peakGenerator_;
    /** \brief Energy calculator used by this thread */
    EnergyCalculator* energyCalculator_;
    /** \brief Is true when modules were initialized successfully */
    bool modulesInitialized_;
    /** \brief Name of this thread used as an ID */
    std::string threadName_;
    /** \brief Whether interrupt signal was not received. If false calculations
     *         should stop prematurely. True by default.
     */
    bool noInterruptReceived_;


  private:
    /** \brief Initializes modules with appropriate module implementation
     *         (specified by global settings).
     * 
     *  Update this method when adding new modules.
     *
     *  \return True if initialization of all modules was successful,
     *          false otherwise
     */
    bool initializeModules();
  };
}



#endif // THREADEDCONTROLLER_HPP_INCLUDED
