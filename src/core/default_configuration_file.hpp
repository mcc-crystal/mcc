/** \file default_configuration_file.hpp
 *  \brief Defines a string containing whole default configuration file.
 */

#ifndef DEFAULTCONFIGURATIONFILE_HPP_INCLUDED
#define DEFAULTCONFIGURATIONFILE_HPP_INCLUDED

#include <string>


namespace mcc
{
  /** \brief String containing the whole default configuration file. Can be
   *         written to a file directly.
   */
  const std::string DEFAULT_CONFIGURATION_FILE {
R"(# MCC configuration file

[Main]
dataFile = "peaks.dat"
# Default wavelength: 1.5406 A
# wavelength = 0.81870
# xy: 2Theta  Intesity(optional)
dataFormat = "xy"
zeroShift = 0.0
# Number of best results stored
nStoredCells = 4
# Tolerance to treat peak as indexed
twoThetaTolerance = 0.05


[Cell]
minLength =  1.0
maxLength = 15.0
# minA =  1.0
# maxA = 15.0
# minB =  1.0
# maxB = 15.0
# minC =  1.0
# maxC = 15.0
minAngle = 90.0
maxAngle = 120.0
# Supported crystal systems:
# cubic, hexagonal, tetragonal,
# orthorhombic, monoclinic, triclinic
crystalSystem = "monoclinic"


[GridSearch]
lengthStep = 0.010 # [A]
angleStep  = 0.010 # [degrees]


[MonteCarlo]
# Number of simulations performed
# (each simulation begins with a random cell)
nMaxSimulations = 100
# Steps per simulation
nMaxSteps = 100000
# Ideal ratio of accepted steps vs all steps
# (length and angle steps are optimized to try to achieve set ratio)
acceptedStepRatio = 0.5


[Temperature]
# Manual curve parameters for Linear temperature controller
# minimumTemperature = 1e-5
# linearIntercept = 0.5
# linearSlope = -5e-5

# Auto parameters for automatic curve parameter calculation
# (use for AutoLinear and AutoCosine temperature controllers)
autoMaxTemperature       = 0.05
autoMinTemperature       = 1e-6
autoStartConstantSteps   = 0
autoEndConstantSteps     = 3000
autoPeriodsPerSimulation = 2


[Energy]
# Scaling factor for additional excess peaks penalty
SmithSnyderExFactor = 0.005
# Base of exponential function for EmeraldEnergyEx
# Larger base punishes big unit cells more
EmeraldExpBase = 1.80
# Larger than 1.0 increases sensitivity to unindexed peaks,
# between 0.0 and 1.0 decreases sensitivity, default is 1.0
EmeraldUnindexedFactor = 1.0


[Modules]
# rootController = "GridSearch"
rootController = "MonteCarlo"

energyCalculator = "EmeraldEx"
temperatureController = "AutoCosine"
stepOptimizer = "Logarithmic"


[Parallel]
# Set to number of CPU cores you want to use
nThreads = 1
# Rough number of packets work is divided in when doing grid methods
workAreaDivider = 1000


[Logging]
# Use only for debugging and with low number of simulations
cellTracker = 0
)"
  };
}




#endif // DEFAULTCONFIGURATIONFILE_HPP_INCLUDED