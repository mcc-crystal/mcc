/** \file CellStats.hpp
 *  \brief Provides class for calculating cell stats like number of indexed
 *         and unindexed peaks, and number of possible peaks.
 */

#ifndef CELLSTATS_HPP_INCLUDED
#define CELLSTATS_HPP_INCLUDED

#include "Module.hpp"
#include "../peak_list/CalculatedPeakList.hpp"
#include "../peak_list/ObservedPeakList.hpp"



namespace mcc
{
  /** \brief Calculates cell stats by comparing lists of observed and
   *         calculated peaks. Provides number of indexed and unindexed peaks,
   *         number of possible peaks and more.
   */
  class CellStats : public Module
  {
  public:
    CellStats(const GlobalSettings& settings,
              const CalculatedPeakList& calculatedList,
              const ObservedPeakList& observedList);

    std::string getModuleName() const override;

    /** \brief Calculates cell stats based on provided peak lists (must be
     *         populated!).
     */
    void calculateStats();

    /** \brief Returns number of indexed peaks from last stats calculation.
     *
     *  \return Number of indexed peaks
     */
    int getNIndexedPeaks() const;

    /** \brief Returns number of unindexed peaks from last stats calculation.
     *
     *  \return Number of unindexed peaks
     */
    int getNUnindexedPeaks() const;

    /** \brief Returns number of possible peaks up to the last observed peak
     *         from last stats calculation.
     *
     *  \return Number of possible peaks
     */
    int getNPossiblePeaks() const;

    /** \brief Calculates only number of possible peaks and returns the result.
     *         Does not store the result in object's member variables.
     * 
     *  \return Number of possible peaks
     */
    int calculateNPossiblePeaks() const;


  private:
    /** \brief List of calculated peaks used in comparisons.
     */
    const CalculatedPeakList& calculatedList_;

    /** \brief List of observed peaks used in comparisons.
     */
    const ObservedPeakList& observedList_;

    /** \brief Number of indexed peaks. Calculated by calculateStats()
     *         method (must be called manually).
     */
    int nIndexedPeaks_;

    /** \brief Number of unindexed peaks. Calculated by calculateStats()
     *         method (must be called manually).
     */
    int nUnindexedPeaks_;

    /** \brief Number of possible peaks up to the last observed peak.
     *         Calculated by calculateStats() method (must be called manually).
     */
    int nPossiblePeaks_;

    /** \brief 2Theta tolerance used as a criterion in cell stats calculation.
     *         Initialized from settings.
     */
    double twoThetaTolerance_;
  };
}



#endif // CELLSTATS_HPP_INCLUDED
