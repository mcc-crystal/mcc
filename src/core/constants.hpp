/** \file constants.hpp
 *  \brief Declares constants common to the whole application.
 */

#ifndef CONSTANTS_HPP_INCLUDED
#define CONSTANTS_HPP_INCLUDED

#include <string>



namespace mcc
{
  /** \brief Contains static constants common to the whole application.
   */
  class Constants
  {
  public:
    // Application info

    /** \brief Name of the application */
    static const std::string applicationName;
    /** \brief Copyright year or years */
    static const std::string copyrightYear;
    /** \brief Name of the author/authors */
    static const std::string applicationAuthor;
    /** \brief Version of the application (used as build information) */
    static const std::string buildVersion;
    /** \brief Date of release of the current version of the application
     *         (used as build information)
     */
    static const std::string buildDate;

    // File paths

    /** \brief Path to the configuration file */
    static const std::string settingsFilePath;
    /** \brief Path to the file to which solution are written at the end
     *         of calculations.
     */
    static const std::string solutionFilePath;
    /** \brief Path to the file to which merged tracked cell data are writen
     *         at the end of calculations.
     */
    static const std::string trackedDataFilePath;
  };
}



#endif // CONSTANTS_HPP_INCLUDED
