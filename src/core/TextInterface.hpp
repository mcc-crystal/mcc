/** \file TextInterface.hpp
 *  \brief Declares base class for text interfaces which show progress during
 *         calculations.
 */

#ifndef TEXTINTERFACE_HPP_INCLUDED
#define TEXTINTERFACE_HPP_INCLUDED



namespace mcc
{
  /** \brief Base class for text interfaces to show progress information during
   *         the calculation process.
   */
  class TextInterface
  {
  public:
    /** \brief Initializes the text interface. Should be called after the
     *         calculations begin and before the first update call.
     */
    virtual void initializeInterface() = 0;

    /** \brief Updates the text interface with currently available information.
     */
    virtual void updateInterface() = 0;
  };
}



#endif // TEXTINTERFACE_HPP_INCLUDED
