/** \file Peak.hpp
 *  \brief Declares representation of an observed peak with all necessary
 *         information.
 */
#ifndef PEAK_HPP_INCLUDED
#define PEAK_HPP_INCLUDED



namespace mcc
{
  /** \brief Represents an observed peak with all the necessary information.
   */
  class Peak
  {
  public:
    /** \brief 2Theta position [°] */
    double twoTheta;
    /** \brief Intensity (unitless) */
    double intensity;
  };
}



#endif // PEAK_HPP_INCLUDED
