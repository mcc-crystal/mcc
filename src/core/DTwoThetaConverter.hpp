/** \file DTwoThetaConverter.hpp
 *  \brief Declares centralized static class for 2Theta position and d-value
 *         interconversion.
 */

#ifndef DTWOTHETACONVERTER_HPP_INCLUDED
#define DTWOTHETACONVERTER_HPP_INCLUDED



namespace mcc
{
  /** \brief Centralized static class for interplanar spacing and 2Theta
   *         position interconversion.
   */
  class DTwoThetaConverter
  {
  public:
    /** \brief Converts interplanar spacing [Å] to 2Theta position [°].
     *
     *  \param d Interplanar spacing [Å] to be converted
     *  \return 2Theta position [°]
     */
    static double dToTwoTheta(double d);

    /** \brief Converts 2Theta position [°] to interplanar spacing [Å].
     *
     *  \param twoTheta 2Theta value [°] to be converted
     *  \return Interplanar spacing [Å]
     */
    static double twoThetaToD(double twoTheta);

    /** \brief Sets wavelength used for conversions. This must be done before
     *         performing conversions.
     *
     *  \param wavelength Wavelength to be set [Å]
     */
    static void setWavelength(double wavelength);

  private:
    /** \brief Wavelength [Å] used for conversions */
    static double wavelength_;
  };
}



#endif // DTWOTHETACONVERTER_HPP_INCLUDED
