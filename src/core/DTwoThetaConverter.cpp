#include "DTwoThetaConverter.hpp"

#include <cmath>

// For compatibility with other compilers
#ifndef M_PI
#define M_PI 3.14159265358979323846d
#endif // M_PI



namespace mcc
{
  // Static initialization
  double DTwoThetaConverter::wavelength_ = 0;



  double DTwoThetaConverter::dToTwoTheta(double d)
  {
    double sinTheta = wavelength_ / (2.0 * d);
    double twoTheta = 360.0 / M_PI * std::asin(sinTheta);
    return twoTheta;
  }



  double DTwoThetaConverter::twoThetaToD(double twoTheta)
  {
    double d = wavelength_ / (2.0 * std::sin(twoTheta * M_PI / 360.0));
    return d;
  }



  void DTwoThetaConverter::setWavelength(double wavelength)
  {
    wavelength_ = wavelength;
  }
}
