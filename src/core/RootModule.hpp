/** \file RootModule.hpp
 *  \brief Declares base class for root modules.
 */

#ifndef ROOTMODULE_HPP_INCLUDED
#define ROOTMODULE_HPP_INCLUDED

#include <mutex>
#include <csignal>

#include "GlobalSettings.hpp"
#include "../solution/SolutionStorage.hpp"



namespace mcc
{
  /** \brief Base class for root modules, which are meant to control threads
   *         and provide centralized solution storage.
   */
  class RootModule
  {
  public:
    /** \brief Intializes new RootModule object and saves reference to global
     *         settings.
     * 
     *  \param globalSettings Shared global settings object
     */
    RootModule(const GlobalSettings& settings);

    ~RootModule();

    /** \brief Creates threads and begins calculations.
     *
     *  After receiving general information, threads may ask for work
     *  themselves in derivatives of this class.
     */
    virtual void spawnThreads() = 0;

    /** \brief Returns the worst energy in solution storage. Individual threads
     *         can then judge whether to try to submit their results or not to
     *         save time.
     * 
     *  \warning Not thread safe!
     *
     *  \return Energy of the worst solution
     */
    double getWorstEnergy() const;

    /** \brief Adds a solution to the solution storage if it is better than
     *         the worst solution contained. Protected with mutex for thread
     *         safety.
     *
     *  \param solution Solution to add
     */
    void submitSolution(const Solution& solution);

    /** \brief Returns number of stored solutions. Not thread safe!
     * 
     *  \warning Not thread safe!
     *
     *  \return Number of stored solutions
     */
    int getSolutionCount() const;

    /** \brief Returns number of stored solutions. Protected with mutex.
     *
     *  \return Number of stored solutions
     */
    int getSolutionCountSafe();

    /** \brief Gets solution from the storage at given index.
     * 
     *  \warning Not thread safe!
     *
     *  \param i Index of solution
     *  \return Solution object
     */
    Solution getSolution(int i) const;

    /** \brief Returns best solution so far. Protected with mutex.
     * 
     *  \warning Does not check for solution's existence! There must be one or
     *           more solutions in the object prior to calling this method!
     *
     *  \return Best solution so far
     */
    Solution getBestSolution();

    /** \brief Informs running threaded controllers that it is time to end
     *         calculations prematurely.
     */
    virtual void signalInterrupt() = 0;

    /** \brief Returns const reference to solution storage.
     * 
     *  \warning Access to storage via returned reference is not protected by
     *           mutex! Use only after the calculations have finished.
     *
     *  \return Reference to solution storage
     */
    const SolutionStorage& getSolutionStorage() const;


  protected:
    /** \brief How often text interface will be updated during the
     *         calculations [ms].
     */
    static const int INTERFACE_UPDATE_INTERVAL;
    /** \brief Reference to global settings */
    const GlobalSettings& globalSettings_;
    /** \brief Storage for solutions submitted by threads */
    SolutionStorage* solutions_;
    /** \brief Mutex for solution access control */
    std::mutex solutionsMutex_;
    /** \brief Number of threads from settings */
    int nThreads_;


  private:
    /** \brief Initializes contained modules with appropriate module
     *         implementations.
     *
     *  \return True if all modules were successfully initialized.
     */
    bool initializeModules();
  };
}



#endif // ROOTMODULE_HPP_INCLUDED
