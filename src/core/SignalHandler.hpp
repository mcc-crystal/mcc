/** \file SignalHandler.hpp
 *  \brief Declares a static class used to handle signals.
 */

#ifndef SIGNALHANDLER_HPP_INCLUDED
#define SIGNALHANDLER_HPP_INCLUDED

#include <csignal>

#include "RootModule.hpp"



namespace mcc
{
  /** \brief Static class that handles signals (mainly user interrupt signal -
   *         Control+C) and reports them down the chain.
   */
  class SignalHandler
  {
  private:
    /** \brief Return value of registerInterruptHandler method */
    static void (*interruptHandler_)(int);
    /** \brief Pointer to root module which will get interrupted when the
     *         interrupt signal is received.
     */
    static RootModule* rootModule_;


  public:
    /** \brief Registers interruptSignalHandler method as handler function for
     *         the interrupt signal.
     *
     *  \param rootModule Root module which will receive interrupt message
     *                    when the interrupt signal is received
     */
    static void registerInterruptHandler(RootModule* rootModule);

    /** \brief Returns interrupt signal handling back to the default handler.
     */
    static void unregisterInterruptHandler();

    /** \brief Calls root module's interrupt method, which should signal it
     *         to prematurely end calculations.
     *
     *  \param param Signal parameter
     */
    static void interruptSignalHandler(int param);
  };
}



#endif // SIGNALHANDLER_HPP_INCLUDED
