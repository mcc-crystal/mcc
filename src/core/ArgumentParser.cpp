#include "ArgumentParser.hpp"

#include <iostream>
#include <sstream>



namespace mcc
{
  ArgumentParser::ArgumentParser(int argc,
                                 const char* argv[],
                                 bool& outResult) :
                  argc_{argc},
                  argv_{argv},
                  // Initialize parse result variables
                  writeConfigurationFile_{false}
  {
    outResult = parseArguments();
  }



  std::string ArgumentParser::getCommandName() const
  {
    return commandName_;
  }



  bool ArgumentParser::getWriteConfigurationFile() const
  {
    return writeConfigurationFile_;
  }



  std::string ArgumentParser::getConfigurationFile() const
  {
    return configurationFile_;
  }



  bool ArgumentParser::parseArguments()
  {
    bool parseResult {true};

    if (argc_ < 1) {
      reportError("No command line arguments passed to ArgumentParser.");
      parseResult = false;
    }
    else
    {
      // Get always present command with which the program was executed
      commandName_ = argv_[0];

      // Parse the rest of the arguments
      for (int i {1}; i < argc_; ++i) {
        std::string arg {argv_[i]};
        // Switch -c
        if (arg == "-c") {
          writeConfigurationFile_ = true;
        }
        // Not a switch
        else if (!isSwitch(arg)) {
          // Only accept one parameter without a switch
          if (configurationFile_.empty())
            configurationFile_ = arg;
          else {
            std::stringstream message;
            message << "Redundant argument '" << arg << "'. " <<
                       "MCC only accepts one argument without a switch " <<
                       "('" << configurationFile_ << "' in this case).";
            reportError(message.str());
            parseResult = false;
          }
        }
        // Parameter unrecognized
        else {
          std::stringstream message;
          message << "Argument '" << arg << "' not recognized.";
          reportError(message.str());
          parseResult = false;
        }
      }
    }

    return parseResult;
  }



  void ArgumentParser::reportWarning(const std::string& message) const
  {
    std::cout << "Argument parse warning: " << message << std::endl;
  }



  void ArgumentParser::reportError(const std::string& message) const
  {
    std::cout << "Argument parse error: " << message << std::endl;
  }



  bool ArgumentParser::isSwitch(const std::string& parameter) const
  {
    if (!parameter.empty()) {
      if (parameter[0] == '-') {
        return true;
      }
    }

    return false;
  }
}