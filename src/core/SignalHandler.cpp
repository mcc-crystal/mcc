#include "SignalHandler.hpp"

#include <iostream>



namespace mcc
{
  // Static initialization
  RootModule* SignalHandler::rootModule_ = nullptr;
  void (*SignalHandler::interruptHandler_)(int) = nullptr;



  void SignalHandler::registerInterruptHandler(RootModule* rootModule)
  {
    if (rootModule == nullptr) {
      throw std::logic_error("rootModule must not be a null pointer!");
    }
    
    SignalHandler::rootModule_ = rootModule;
    SignalHandler::interruptHandler_ =
                  std::signal(SIGINT, SignalHandler::interruptSignalHandler);
  }



  void SignalHandler::unregisterInterruptHandler()
  {
    std::signal(SIGINT, SIG_DFL);
    SignalHandler::rootModule_ = nullptr;
  }



  void SignalHandler::interruptSignalHandler(int param)
  {
    std::cout << "\nSending interrupt signal to threads ..." << std::endl;
    rootModule_->signalInterrupt();
  }
}
