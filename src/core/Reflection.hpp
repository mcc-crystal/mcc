/** \file Reflection.hpp
 *  \brief Declares container for comprehensive reflection data.
 */
#ifndef REFLECTION_HPP_INCLUDED
#define REFLECTION_HPP_INCLUDED



namespace mcc
{
  /** \brief Represents a reflection with all relevant data: Miller indices,
   *         d-value and 2Theta position.
   * 
   *  \warning Members are left uninitialized by default constructor.
   */
  class Reflection
  {
  public:
    /** \brief Miller index h */
    int h;
    /** \brief Miller index k */
    int k;
    /** \brief Miller index l */
    int l;
    /** \brief Interplanar distance [Å] */
    double d;
    /** \brief 2Theta peak position [°] */
    double twoTheta;
  };
}



#endif // REFLECTION_HPP_INCLUDED
