/** \file Tests.hpp
 *  \brief Provides DEPRECATED test cases for various objects.
 */
#ifndef TESTS_HPP_INCLUDED
#define TESTS_HPP_INCLUDED

#include "GlobalSettings.hpp"



// DEPRECATED STUFF; MOVE TO UNIT TESTS



namespace mcc
{
   /** \brief Reads the settings file and outputs the final result when reading
   *         is finished (succeeded or failed). Also return the settings object
   *         for later use.
   *
   *  \return GlobalSettings   Settings object with read settings
   *
   */
  GlobalSettings settingsReaderTest();

  /** \brief Calculates some interplanar distances and volume for unit cell of
   *         each crystal system with given parameters.
   *
   * \param a double   Parameter a [Å]
   * \param b double   Parameter b [Å]
   * \param c double   Parameter c [Å]
   * \param alpha double   Parameter alpha [°]
   * \param beta double   Parameter beta [°]
   * \param gamma double   Parameter gamma [°]
   */
  void unitCellTest(double a, double b, double c,
                    double alpha, double beta, double gamma);

  /** \brief Test peak generation module.
   */
  void peakGeneratorTest(const GlobalSettings& settings);

  /** \brief DynamicPeakList testing function.
   */
  void dynamicPeakListTest(const GlobalSettings& settings);

  /** \brief Data reading and storage testing function.
   */
  void dataReadTest(const GlobalSettings& settings);

  /** \brief Data reading and closest peak search test.
   */
  void closestPeakTest(const GlobalSettings& settings);

  /** \brief Cell evaluation using Smith-Snyder test.
   */
  void evaluationFnTest(const GlobalSettings& settings);

  /** \brief Test for DynamicSolutionStorage object.
   */
  void dynamicSolutionStorageTest(const GlobalSettings& settings);

  /** \brief Test for threaded grid search.
   */
  void gridSearchTest(const GlobalSettings& settings);
}



#endif // TESTS_HPP_INCLUDED
