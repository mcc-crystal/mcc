#include "GlobalSettings.hpp"

#include <iostream>
#include <string>
#include <sstream>
#include <exception>

#include "DTwoThetaConverter.hpp"



namespace mcc
{

  // Static constants initialization
  const std::string GlobalSettings::VALID_GROUPS[] =
        {"Main", "Modules", "Cell", "MonteCarlo", "Temperature", "Energy",
         "PeakGeneration", "GridSearch", "Parallel", "Logging", "Solutions",
         ""};
  const std::string GlobalSettings::VALID_FORMATS[] =
        {"xy", ""};
  const std::string GlobalSettings::VALID_CALCULATED_LIST_MODULES[] =
        {"Dynamic", ""};
  const std::string GlobalSettings::VALID_OBSERVED_LIST_MODULES[] =
        {"Dynamic", ""};
  const std::string GlobalSettings::VALID_ENERGY_MODULES[] =
        {"SmithSnyder", "SmithSnyderEx", "Emerald", "EmeraldEx", ""};
  const std::string GlobalSettings::VALID_PEAK_GENERATOR_MODULES[] =
        {"Allowed", ""};
  const std::string GlobalSettings::VALID_ROOT_MODULES[] =
        {"GridSearch", "MonteCarlo", ""};
  const std::string GlobalSettings::VALID_TEMPERATURE_MODULES[] =
        {"Linear", "AutoLinear", "AutoCosine", ""};
  const std::string GlobalSettings::VALID_STEP_OPTIMIZER_MODULES[] =
        {"Linear", "Logarithmic", ""};

  const double GlobalSettings::MINIMUM_ANGLE = 50.0;
  const double GlobalSettings::MAXIMUM_ANGLE = 180.0;



  GlobalSettings::GlobalSettings() :
                  result_ {false},
                  dValue_ {0.0},
                  iValue_ {0}
  {
    // Set all settings to default values

    // [Main]
    main_dataFile = "";
    main_dataFormat = "xy";
    main_wavelength = 1.5406; // Defaults to Cu K alpha 1
    main_zeroShift = 0.0;
    main_nStoredCells = 5;
    main_twoThetaTolerance = 0.10;

    // [Cell]
    // Disable parameter overrides
    cell_minLength = 0.0;
    cell_maxLength = 0.0;
    cell_minAngle = 0.0;
    cell_maxAngle = 0.0;
    // ---
    cell_minA = 2.0;
    cell_minB = 2.0;
    cell_minC = 2.0;
    cell_minAlpha = 90.0;
    cell_minBeta = 90.0;
    cell_minGamma = 90.0;
    cell_maxA = 10.0;
    cell_maxB = 10.0;
    cell_maxC = 10.0;
    cell_maxAlpha = 120.0;
    cell_maxBeta = 120.0;
    cell_maxGamma = 120.0;
    cell_crystalSystem = CrystalSystem::ORTHORHOMBIC;

    // [PeakGeneration]
    peakGeneration_hMin = -7;
    peakGeneration_kMin = -7;
    peakGeneration_lMin = -7;
    peakGeneration_hMax = 7;
    peakGeneration_kMax = 7;
    peakGeneration_lMax = 7;
    peakGeneration_twoThetaMax = 90.0;
    // Disable index overrides
    peakGeneration_minIndex = 0;
    peakGeneration_maxIndex = 0;

    // [GridSearch]
    gridSearch_lengthStep = 0.01;
    gridSearch_angleStep = 0.1;

    // [MonteCarlo]
    monteCarlo_nMaxSteps = 1e5;
    monteCarlo_nMaxSimulations = 20;
    monteCarlo_acceptedStepRatio = 0.50;
    monteCarlo_nLengthStepCycles = 3;
    monteCarlo_nAngleStepCycles = 3;

    // [Temperature]
    temperature_minimumTemperature = 1e-5;
    temperature_linearIntercept = 0.5;
    temperature_linearSlope = -1e-5;
    temperature_autoMaxTemperature = 0.10;
    temperature_autoMinTemperature = 1e-6;
    temperature_autoStartConstantSteps = 0;
    temperature_autoEndConstantSteps = 0;
    temperature_autoPeriodsPerSimulation = 3.0;
    // [Energy]
    energy_SmithSnyderExFactor = 0.005;
    energy_EmeraldExpBase = 1.7;
    energy_EmeraldUnindexedFactor = 1.0;
    // [Modules]
    modules_calculatedList = "Dynamic";
    modules_observedList = "Dynamic";
    modules_energyCalculator = "EmeraldEx";
    modules_peakGenerator = "Allowed";
    modules_rootController = "MonteCarlo";
    modules_temperatureController = "AutoCosine";
    modules_stepOptimizer = "Logarithmic";
    // [Parallel]
    parallel_nThreads = 1;
    parallel_workAreaDivider = 1000;
    // [Solutions]
    solutions_lengthTolerance = 0.50;
    solutions_angleTolerance = 0.50;
    solutions_energyTolerance = 2.0;
    // [Logging]
    logging_cellTracker = 0;
  }



  void GlobalSettings::reportError(const std::string& message) const
  {
    std::cout << "Setting evaluation error: " << message << std::endl;
  }



  void GlobalSettings::reportWarning(const std::string& message) const
  {
    std::cout << "Setting evaluation warning: " << message << std::endl;
  }



  bool GlobalSettings::parseDouble(const std::string& value,
                                   double& result) const
  {
    try {
      result = std::stod(value);
    }
    catch (const std::exception& e) {
      std::stringstream message;
      message << "Failed to parse float '" << value << "'.";
      reportError(message.str());
      return false;
    }
    return true;
  }



  bool GlobalSettings::parseInteger(const std::string& value,
                                   int& result) const
  {
    try {
      result = std::stoi(value);
    }
    catch (const std::exception& e) {
      std::stringstream message;
      message << "Failed to parse integer '" << value << "'.";
      reportError(message.str());
      return false;
    }
    return true;
  }



  bool GlobalSettings::evaluateSetting(const std::string& group,
                                       const std::string& setting,
                                       const std::string& value)
  {
    std::stringstream message;

    bool returnValue {false};
    group_ = group;
    setting_ = setting;
    value_ = value;

    // [Main]
    if (group == "Main") {
      // [Main] wavelength
      if (setting == "wavelength")
        returnValue = evaluateGreaterThanZeroDouble(main_wavelength);
      // [Main] zeroShift
      else if (setting == "zeroShift")
        returnValue = evaluateAnyDouble(main_zeroShift);
      // [Main] dataFile
      else if (setting == "dataFile") {
        main_dataFile = value;
        returnValue = true;
      }
      // [Main] dataFormat
      else if (setting == "dataFormat") {
        for (int i = 0; VALID_FORMATS[i] != ""; ++i) {
          if (value == VALID_FORMATS[i]) {
            main_dataFormat = value;
            returnValue = true;
            break;
          }
        }
        if (returnValue == false) {
          // value is not in the list of valid formats
          message.str("");
          message << "Data format '" << value << "' is invalid.";
          reportError(message.str());
        }
      }
      // [Main] nStoredCells
      else if (setting == "nStoredCells")
        returnValue = evaluateGreaterThanZeroInteger(main_nStoredCells);
      // [Main] twoThetaTolerance
      else if (setting == "twoThetaTolerance")
        returnValue = evaluateZeroOrGreaterDouble(main_twoThetaTolerance);
    }
    // [Modules]
    else if (group == "Modules") {
      // [Modules] peakGenerator
      if (setting == "peakGenerator")
        returnValue = evaluateModuleName(VALID_PEAK_GENERATOR_MODULES,
                                         modules_peakGenerator);
      // [Modules] rootController
      else if (setting == "rootController")
        returnValue = evaluateModuleName(VALID_ROOT_MODULES,
                                         modules_rootController);
      // [Modules] calculatedList
      else if (setting == "calculatedList")
        returnValue = evaluateModuleName(VALID_CALCULATED_LIST_MODULES,
                                         modules_calculatedList);
      // [Modules] observedList
      else if (setting == "observedController")
        returnValue = evaluateModuleName(VALID_OBSERVED_LIST_MODULES,
                                         modules_observedList);
      // [Modules] energyCalculator
      else if (setting == "energyCalculator")
        returnValue = evaluateModuleName(VALID_ENERGY_MODULES,
                                         modules_energyCalculator);
      // [Modules] temperatureController
      else if (setting == "temperatureController")
        returnValue = evaluateModuleName(VALID_TEMPERATURE_MODULES,
                                         modules_temperatureController);
      // [Modules] stepOptimizer
      else if (setting == "stepOptimizer")
        returnValue = evaluateModuleName(VALID_STEP_OPTIMIZER_MODULES,
                                         modules_stepOptimizer);
    }
    // [Cell]
    else if (group == "Cell") {
      // [Cell] minLength
      if (setting == "minLength")
        returnValue = evaluateAnyDouble(cell_minLength);
      // [Cell] maxLength
      else if (setting == "maxLength")
        returnValue = evaluateAnyDouble(cell_maxLength);
      // [Cell] minAngle
      else if (setting == "minAngle")
        returnValue = evaluateZeroableAngle(cell_minAngle);
      // [Cell] maxAngle
      else if (setting == "maxAngle")
        returnValue = evaluateZeroableAngle(cell_maxAngle);
      // [Cell] minA
      else if (setting == "minA")
        returnValue = evaluateGreaterThanZeroDouble(cell_minA);
      // [Cell] minB
      else if (setting == "minB")
        returnValue = evaluateGreaterThanZeroDouble(cell_minB);
      // [Cell] minC
      else if (setting == "minC")
        returnValue = evaluateGreaterThanZeroDouble(cell_minC);
      // [Cell] minAlpha
      else if (setting == "minAlpha")
        returnValue = evaluateAngle(cell_minAlpha);
      // [Cell] minBeta
      else if (setting == "minBeta")
        returnValue = evaluateAngle(cell_minBeta);
      // [Cell] minBeta
      else if (setting == "minGamma")
        returnValue = evaluateAngle(cell_minGamma);
      // [Cell] maxA
      else if (setting == "maxA")
        returnValue = evaluateGreaterThanZeroDouble(cell_maxA);
      // [Cell] maxB
      else if (setting == "maxB")
        returnValue = evaluateGreaterThanZeroDouble(cell_maxB);
      // [Cell] maxC
      else if (setting == "maxC")
        returnValue = evaluateGreaterThanZeroDouble(cell_maxC);
      // [Cell] maxAlpha
      else if (setting == "maxAlpha")
        returnValue = evaluateAngle(cell_maxAlpha);
      // [Cell] maxBeta
      else if (setting == "maxBeta")
        returnValue = evaluateAngle(cell_maxBeta);
      // [Cell] maxBeta
      else if (setting == "maxGamma")
        returnValue = evaluateAngle(cell_maxGamma);
      else if (setting == "crystalSystem") {
        returnValue = true; // Can be negated in the else statement
        if (value == "cubic")
          cell_crystalSystem = CrystalSystem::CUBIC;
        else if (value == "hexagonal")
          cell_crystalSystem = CrystalSystem::HEXAGONAL;
        else if (value == "tetragonal")
          cell_crystalSystem = CrystalSystem::TETRAGONAL;
        else if (value == "orthorhombic")
          cell_crystalSystem = CrystalSystem::ORTHORHOMBIC;
        else if (value == "monoclinic")
          cell_crystalSystem = CrystalSystem::MONOCLINIC;
        else if (value == "triclinic")
          cell_crystalSystem = CrystalSystem::TRICLINIC;
        else {
          reportError("Crystal system '" + value + "' not recognized.");
          returnValue = false;
        }
      }
    }
    // [MonteCarlo]
    else if (group == "MonteCarlo") {
      // [MonteCarlo] nMaxSteps
      if (setting == "nMaxSteps")
        returnValue = evaluateGreaterThanZeroInteger(monteCarlo_nMaxSteps);
      // [MonteCarlo] nMaxSimulations
      else if (setting == "nMaxSimulations")
        returnValue =
                    evaluateGreaterThanZeroInteger(monteCarlo_nMaxSimulations);
      // [MonteCarlo] acceptedStepRatio
      else if (setting == "acceptedStepRatio")
        returnValue =
                   evaluateGreaterThanZeroDouble(monteCarlo_acceptedStepRatio);
      // [MonteCarlo] nLengthStepCycles
      else if (setting == "nLengthStepCycles")
        returnValue =
                  evaluateGreaterThanZeroInteger(monteCarlo_nLengthStepCycles);
      // [MonteCarlo] nAngleStepCycles
      else if (setting == "nAngleStepCycles")
        returnValue =
                   evaluateGreaterThanZeroInteger(monteCarlo_nAngleStepCycles);
    }
    // [Temperature]
    else if (group == "Temperature") {
      // [Temperature] minimumTemperature
      if (setting == "minimumTemperature")
        returnValue =
          evaluateGreaterThanZeroDouble(temperature_minimumTemperature);
      // [Temperature] linearIntercept
      else if (setting == "linearIntercept")
        returnValue = evaluateAnyDouble(temperature_linearIntercept);
      // [Temperature] linearSlope
      else if (setting == "linearSlope")
        returnValue = evaluateAnyDouble(temperature_linearSlope);
      // [Temperature] autoMaxTemperature
      else if (setting == "autoMaxTemperature")
        returnValue = evaluateGreaterThanZeroDouble(
                                               temperature_autoMaxTemperature);
      // [Temperature] autoMinTemperature
      else if (setting == "autoMinTemperature")
        returnValue = evaluateGreaterThanZeroDouble(
                                               temperature_autoMinTemperature);
      // [Temperature] autoStartConstantSteps
      else if (setting == "autoStartConstantSteps")
        returnValue = evaluateZeroOrGreaterInteger(
                                           temperature_autoStartConstantSteps);
      // [Temperature] autoEndConstantSteps
      else if (setting == "autoEndConstantSteps")
        returnValue = evaluateZeroOrGreaterInteger(
                                             temperature_autoEndConstantSteps);
      // [Temperature] autoPeriodsPerSimulation
      else if (setting == "autoPeriodsPerSimulation")
        returnValue = evaluateGreaterThanZeroDouble(
                                         temperature_autoPeriodsPerSimulation);
    }
    // [Energy]
    else if (group == "Energy") {
      // [Energy] NNpossScaleFactor
      if (setting == "SmithSnyderExFactor")
        returnValue = evaluateGreaterThanZeroDouble(
                                               energy_SmithSnyderExFactor);
      // [Energy] EmeraldExpBase
      if (setting == "EmeraldExpBase")
        returnValue = evaluateGreaterThanZeroDouble(energy_EmeraldExpBase);
      // [Energy] EmeraldUnindexedFactor
      if (setting == "EmeraldUnindexedFactor")
        returnValue = evaluateGreaterThanZeroDouble(
                                               energy_EmeraldUnindexedFactor);
    }
    // [GridSearch]
    else if (group == "GridSearch") {
      // [GridSearch] lengthStep
      if (setting == "lengthStep")
        returnValue = evaluateGreaterThanZeroDouble(gridSearch_lengthStep);
      // [GridSearch] angleStep
      else if (setting == "angleStep")
        returnValue = evaluateGreaterThanZeroDouble(gridSearch_angleStep);
    }
    // [PeakGeneration]
    else if (group == "PeakGeneration") {
      // [PeakGeneration] hMin
      if (setting == "hMin")
        returnValue = evaluateAnyInteger(peakGeneration_hMin);
      // [PeakGeneration] kMin
      else if (setting == "kMin")
        returnValue = evaluateAnyInteger(peakGeneration_kMin);
      // [PeakGeneration] lMin
      else if (setting == "lMin")
        returnValue = evaluateAnyInteger(peakGeneration_lMin);
      // [PeakGeneration] hMax
      else if (setting == "hMax")
        returnValue = evaluateAnyInteger(peakGeneration_hMax);
      // [PeakGeneration] kMax
      else if (setting == "kMax")
        returnValue = evaluateAnyInteger(peakGeneration_kMax);
      // [PeakGeneration] hMax
      else if (setting == "lMax")
        returnValue = evaluateAnyInteger(peakGeneration_hMax);
      // [PeakGeneration] twoThetaMax
      else if (setting == "twoThetaMax")
        returnValue = evaluateGreaterThanZeroDouble(
                                                   peakGeneration_twoThetaMax);
      // [PeakGeneration] minIndex
      else if (setting == "minIndex")
        returnValue = evaluateAnyInteger(peakGeneration_minIndex);
      // [PeakGeneration] maxIndex
      else if (setting == "maxIndex")
        returnValue = evaluateAnyInteger(peakGeneration_maxIndex);
      }
    // [Parallel]
    else if (group == "Parallel") {
      // [Parallel] workAreaDivider
      if (setting == "nThreads")
        returnValue = evaluateGreaterThanZeroInteger(parallel_nThreads);
      else if (setting == "workAreaDivider")
        returnValue = evaluateGreaterThanZeroInteger(parallel_workAreaDivider);
    }
    // [Solutions]
    else if (group == "Solutions") {
      // [Solutions] lengthTolerance
      if (setting == "lengthTolerance")
        returnValue = evaluateGreaterThanZeroDouble(solutions_lengthTolerance);
      // [Solutions] angleTolerance
      else if (setting == "angleTolerance")
        returnValue = evaluateGreaterThanZeroDouble(solutions_angleTolerance);
      // [Solutions] energyTolerance
      else if (setting == "energyTolerance")
        returnValue = evaluateGreaterThanZeroDouble(solutions_energyTolerance);
    }
    // [Logging]
    else if (group == "Logging") {
      // [Logging] cellTracker
      if (setting == "cellTracker")
        returnValue = evaluateAnyInteger(logging_cellTracker);
    }
    // Unknown group
    else {
      message << "Invalid group name [" << group << "].";
      reportError(message.str());
    }

    return returnValue;
  }



  bool GlobalSettings::evaluateGroup(const std::string& group) const
  {
    int i = 0;
    while (VALID_GROUPS[i] != "") {
      if (group == VALID_GROUPS[i])
        return true;
      ++i;
    }
    return false;
  }



  bool GlobalSettings::performPostEvaluation()
  {
    // Set to false if errors are encountered
    bool noErrors = true;

    // Set central interconversion object's wavelength
    DTwoThetaConverter::setWavelength(main_wavelength);

    // If general parameter or angle settings are greater than zero
    // they override individual settings
    if (cell_minLength > 0.0) {
      reportWarning("[Cell] minLength overriding individual parameter "
                    "settings.");
      cell_minA = cell_minLength;
      cell_minB = cell_minLength;
      cell_minC = cell_minLength;
    }
    if (cell_maxLength > 0.0) {
      reportWarning("[Cell] maxLength overriding individual parameter "
                    "settings.");
      cell_maxA = cell_maxLength;
      cell_maxB = cell_maxLength;
      cell_maxC = cell_maxLength;
    }
    if (cell_minAngle > 0.0) {
      reportWarning("[Cell] minAngle overriding individual parameter "
                    "settings.");
      cell_minAlpha = cell_minAngle;
      cell_minBeta = cell_minAngle;
      cell_minGamma = cell_minAngle;
    }
    if (cell_maxAngle > 0.0) {
      reportWarning("[Cell] maxAngle overriding individual parameter "
                    "settings.");
      cell_maxAlpha = cell_maxAngle;
      cell_maxBeta = cell_maxAngle;
      cell_maxGamma = cell_maxAngle;
    }

    // If general index settings are not zero they override individual settings
    if (peakGeneration_minIndex != 0) {
      reportWarning("[PeakGeneration] minIndex overriding individual index "
                    "settings.");
      peakGeneration_hMin = peakGeneration_minIndex;
      peakGeneration_kMin = peakGeneration_minIndex;
      peakGeneration_lMin = peakGeneration_minIndex;
    }
    if (peakGeneration_maxIndex != 0) {
      reportWarning("[PeakGeneration] maxIndex overriding individual index "
                    "settings.");
      peakGeneration_hMax = peakGeneration_maxIndex;
      peakGeneration_kMax = peakGeneration_maxIndex;
      peakGeneration_lMax = peakGeneration_maxIndex;
    }

    // Check min/max parameters and angles
    if (cell_minA > cell_maxA) {
      reportError("Minimum cell parameter a is greater than maximum one.");
      noErrors = false;
    }
    if (cell_minB > cell_maxB) {
      reportError("Minimum cell parameter b is greater than maximum one.");
      noErrors = false;
    }
    if (cell_minC > cell_maxC) {
      reportError("Minimum cell parameter c is greater than maximum one.");
      noErrors = false;
    }
    if (cell_minAlpha > cell_maxAlpha) {
      reportError("Minimum cell parameter alpha is greater than maximum one.");
      noErrors = false;
    }
    if (cell_minBeta > cell_maxBeta) {
      reportError("Minimum cell parameter beta is greater than maximum one.");
      noErrors = false;
    }
    if (cell_minGamma > cell_maxGamma) {
      reportError("Minimum cell parameter gamma is greater than maximum one.");
      noErrors = false;
    }

    // Check min/max indices
    if (peakGeneration_hMin > peakGeneration_hMax) {
      reportError("Minimum index h is greater than maximum one.");
      noErrors = false;
    }
    if (peakGeneration_kMin > peakGeneration_kMax) {
      reportError("Minimum index k is greater than maximum one.");
      noErrors = false;
    }
    if (peakGeneration_lMin > peakGeneration_lMax) {
      reportError("Minimum index l is greater than maximum one.");
      noErrors = false;
    }

    // Check min/max for auto temperature parameters
    if (temperature_autoMinTemperature > temperature_autoMaxTemperature) {
      reportError("Auto minimum temperature is greater than auto maximum "
                  "temperature.");
      noErrors = false;
    }

    // Check if there are more constant steps that total simulation steps
    if (temperature_autoStartConstantSteps + temperature_autoEndConstantSteps >
        monteCarlo_nMaxSteps) {
      reportError("Sum of auto constant temperature steps is greater than "
                  "number of simulation steps.");
      noErrors = false;
    }

    return noErrors;
  }



  bool GlobalSettings::evaluateAnyInteger(int& outVariable)
  {
    result_ = parseInteger(value_, iValue_);
    if (result_) {
      outVariable = iValue_;
      return true;
    }
    return false;
  }



  bool GlobalSettings::evaluateAnyDouble(double& outVariable)
  {
    result_ = parseDouble(value_, dValue_);
    if (result_) {
      outVariable = dValue_;
      return true;
    }
    return false;
  }



  bool GlobalSettings::evaluateGreaterThanZeroInteger(int& outVariable)
  {
    result_ = parseInteger(value_, iValue_);
    if (result_) {
      if (iValue_ <= 0.0) {
        message_.str("");
        message_ << "Value of [" << group_ << "] " << setting_ << " must be "
                   "greater than zero and an integer.";
        reportError(message_.str());
        return false;
      }
      outVariable = iValue_;
      return true;
    }
    return false;
  }



  bool GlobalSettings::evaluateZeroOrGreaterInteger(int& outVariable)
  {
    result_ = parseInteger(value_, iValue_);
    if (result_) {
      if (iValue_ < 0.0) {
        message_.str("");
        message_ << "Value of [" << group_ << "] " << setting_ << " must be "
                   "zero or greater than zero and an integer.";
        reportError(message_.str());
        return false;
      }
      outVariable = iValue_;
      return true;
    }
    return false;
  }



  bool GlobalSettings::evaluateGreaterThanZeroDouble(double& outVariable)
  {
    result_ = parseDouble(value_, dValue_);
    if (result_) {
      if (dValue_ <= 0.0) {
        message_.str("");
        message_ << "Value of [" << group_ << "] " << setting_ << " must be "
                   "greater than zero.";
        reportError(message_.str());
        return false;
      }
      outVariable = dValue_;
      return true;
    }
    return false;
  }



  bool GlobalSettings::evaluateZeroOrGreaterDouble(double& outVariable)
  {
    result_ = parseDouble(value_, dValue_);
    if (result_) {
      if (dValue_ < 0.0) {
        message_.str("");
        message_ << "Value of [" << group_ << "] " << setting_ << " must be "
                   "zero or greater than zero.";
        reportError(message_.str());
        return false;
      }
      outVariable = dValue_;
      return true;
    }
    return false;
  }



  bool GlobalSettings::evaluateAngle(double& outVariable)
  {
    result_ = parseDouble(value_, dValue_);
    if (result_) {
      if (dValue_ >= MINIMUM_ANGLE && dValue_ <= MAXIMUM_ANGLE) {
        outVariable = dValue_;
        return true;
      }
      else {
        message_.str("");
        message_ << "Value of [" << group_ << "] " << setting_ <<
                    " must be between " << MINIMUM_ANGLE << " and " <<
                    MAXIMUM_ANGLE << ".";
        reportError(message_.str());
      }
    }
    return false;
  }



  bool GlobalSettings::evaluateZeroableAngle(double& outVariable)
  {
    result_ = parseDouble(value_, dValue_);
    if (result_) {
      if (dValue_ <= 0.0 ||
          (dValue_ >= MINIMUM_ANGLE && dValue_ <= MAXIMUM_ANGLE)) {
        outVariable = dValue_;
        return true;
      }
      else {
        message_.str("");
        message_ << "Value of [" << group_ << "] " << setting_ <<
                    " must be between " << MINIMUM_ANGLE << " and " <<
                    MAXIMUM_ANGLE << ". To disable individual parameter "
                    "override set it to zero or less.";
        reportError(message_.str());
      }
    }
    return false;
  }



  bool GlobalSettings::evaluateModuleName(const std::string validNames[],
                                          std::string& outVariable)
  {
    for (int i = 0; validNames[i] != ""; ++i) {
      if (value_ == validNames[i]) {
        outVariable = value_;
        return true;
      }
    }

    message_.str("");
    message_ << "Module name '" << value_ << "' not recognized.";
    reportError(message_.str());
    return false;
  }

}
