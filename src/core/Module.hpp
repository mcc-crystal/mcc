/** \file Module.hpp
 *  \brief Declares base class for modules.
 */
#ifndef MODULE_HPP_INCLUDED
#define MODULE_HPP_INCLUDED

#include "GlobalSettings.hpp"

#include <string>



namespace mcc
{
  /** \brief Base class for modules.
   */
  class Module
  {
  public:
    /** \brief Intializes new Module object and saves reference to global
     *         settings.
     * 
     *  \param globalSettings Shared global settings object
     */
    Module(const GlobalSettings& globalSettings);

    virtual ~Module() = default;

    /** \brief Returns name of the module.
     *
     *  \return Name of the module
     */
    virtual std::string getModuleName() const = 0;


  protected:
    /** \brief Shared global settings object */
    const GlobalSettings& globalSettings_;

    /** \brief Prints an error message with module name information. Line is
     *         terminated automatically.
     *
     *  \param message Message to be printed
     */
    virtual void reportError(std::string message) const;

    /** \brief Prints a warning message with module name information. Line is
     *         terminated automatically.
     *
     *  \param message Message to be printed
     */
    virtual void reportWarning(std::string message) const;

    /** \brief Prints a message with module name information. Line is
     *         terminated automatically.
     *
     * \param message Message to be printed
     */
    virtual void reportMessage(std::string message) const;
  };
}



#endif // MODULE_HPP_INCLUDED
