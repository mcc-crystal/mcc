#include "CellStats.hpp"

#include <cmath>



namespace mcc
{
  CellStats::CellStats(const GlobalSettings& settings,
                       const CalculatedPeakList& calculatedList,
                       const ObservedPeakList& observedList) :
             Module{settings},
             calculatedList_{calculatedList},
             observedList_{observedList},
             nIndexedPeaks_{0},
             nUnindexedPeaks_{0},
             nPossiblePeaks_{0},
             twoThetaTolerance_{0}
  {
    // Initialize from settings
    twoThetaTolerance_ = settings.main_twoThetaTolerance;
  }



  std::string CellStats::getModuleName() const
  {
    return "CellStats";
  }



  void CellStats::calculateStats()
  {
    const int N_OBSERVED {observedList_.getPeakCount()};
    const int N_CALCULATED {calculatedList_.getPeakCount()};

    // Initialize counters
    nIndexedPeaks_ = 0;
    nUnindexedPeaks_ = 0;
    nPossiblePeaks_ = 0;

    // Safeguard: empty peak list
    if (N_CALCULATED < 1) {
      nUnindexedPeaks_ = N_OBSERVED;
      return;
    }

    // Determine number of indexed and unindexed peaks
    for (int i = 0; i < N_OBSERVED; ++i) {
      const Peak& observedPeak {observedList_.getPeak(i)};
      int iClosest {calculatedList_.findClosestPeak(observedPeak.twoTheta)};
      const Reflection& closestPeak {calculatedList_.getReflection(iClosest)};
      double absDifference {std::fabs(observedPeak.twoTheta -
                                      closestPeak.twoTheta)};

      if (absDifference <= twoThetaTolerance_)
        nIndexedPeaks_++;
      else
        nUnindexedPeaks_++;
    }

    // Determine number of possible peaks
    double maxTwoTheta = observedList_.getMaxTwoTheta() + twoThetaTolerance_;
    for (int i = N_CALCULATED - 1; i >= 0; --i) {
      const Reflection& r {calculatedList_.getReflection(i)};
      if (r.twoTheta <= maxTwoTheta) {
        nPossiblePeaks_ = i + 1;
        break;
      }
    }
  }



  int CellStats::getNIndexedPeaks() const
  {
    return nIndexedPeaks_;
  }



  int CellStats::getNUnindexedPeaks() const
  {
    return nUnindexedPeaks_;
  }



  int CellStats::getNPossiblePeaks() const
  {
    return nPossiblePeaks_;
  }



  int CellStats::calculateNPossiblePeaks() const
  {
    const int N_CALCULATED {calculatedList_.getPeakCount()};

    // Determine number of possible peaks
    double maxTwoTheta = observedList_.getMaxTwoTheta() + twoThetaTolerance_;
    for (int i = N_CALCULATED - 1; i >= 0; --i) {
      const Reflection& r {calculatedList_.getReflection(i)};
      if (r.twoTheta <= maxTwoTheta) {
        return i + 1;
      }
    }

    return 0;
  }
}
