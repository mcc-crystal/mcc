#include "constants.hpp"



namespace mcc
{
  // Application info
  const std::string Constants::applicationName = "MCC";
  const std::string Constants::copyrightYear = "2018-2019";
  const std::string Constants::applicationAuthor = "Matej Reberc";
  const std::string Constants::buildVersion = "1.0-RC1";
  const std::string Constants::buildDate = "June 2019";
  // File paths
  const std::string Constants::settingsFilePath = "mcc.conf";
  const std::string Constants::solutionFilePath = "solutions.dat";
  const std::string Constants::trackedDataFilePath = "tracked_data.dat";
}
