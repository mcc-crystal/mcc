#include "main_functions.hpp"

#include <string>
#include <iostream>
#include <iomanip>
#include <fstream>

#include "constants.hpp"
#include "default_configuration_file.hpp"
#include "SignalHandler.hpp"
#include "../io/SettingsReader.hpp"
#include "../grid_search/GridSearchRootModule.hpp"
#include "../monte_carlo/MonteCarloRootModule.hpp"



namespace mcc
{
  bool readSettingsFile(GlobalSettings& outSettings, std::string path)
  {
    if (path.empty())
      path = Constants::settingsFilePath;
    
    std::cout << "Reading settings file '" << path << "'." << std::endl;

    bool result;
    SettingsReader settingsReader{path, outSettings};
    result = settingsReader.readFile();

    if (result == true) {
      std::cout << "Performing setting evaluation." << std::endl;
      result = outSettings.performPostEvaluation();

      if (result == false) {
        std::cout << "Setting evaluation failed." << std::endl;
      }
    }
    else {
      std::cout << "Errors encountered while reading settings. Program will "
                   "terminate." << std::endl;
    }

    return result;
  }



  void performGridSearch(const GlobalSettings& settings)
  {
    GridSearchRootModule gsRoot{settings};
    SignalHandler::registerInterruptHandler(&gsRoot);

    std::cout << "\nBeginning grid search calculations ..." << std::endl;
    gsRoot.spawnThreads();

    printSolutions(gsRoot);
    SignalHandler::unregisterInterruptHandler();
  }



  void performMonteCarlo(const GlobalSettings& settings)
  {
    MonteCarloRootModule mcRoot{settings};
    SignalHandler::registerInterruptHandler(&mcRoot);

    std::cout << "\nBeginning Monte Carlo simulations ..." << std::endl;
    mcRoot.spawnThreads();

    printSolutions(mcRoot);
    SignalHandler::unregisterInterruptHandler();
  }



  void printSolutions(const RootModule& root)
  {
    const SolutionStorage& storage {root.getSolutionStorage()};
    const int N {storage.getSolutionCount()};

    std::cout << "\n" << N << " solutions:" << std::endl;

    for (int i = 0; i < N; ++i) {
      const Solution& solution {storage.getSolution(i)};
      storage.writeSolutionBlock(std::cout, solution, i + 1);
    }
  }



  bool initializeAndRunRootModule(const GlobalSettings& settings)
  {
    std::string rootModule {settings.modules_rootController};

    if (rootModule == "GridSearch")
      performGridSearch(settings);
    else if (rootModule == "MonteCarlo")
      performMonteCarlo(settings);
    else
      return false;

    return true;
  }



  void printGreeting()
  {
    // First line
    std::cout << " " << Constants::applicationName << " version " <<
                 Constants::buildVersion << " (" << Constants::buildDate <<
                 ")\n" << std::endl;

    // Separator
    std::cout << "------------------------------------------------------------"
                 "------------" << std::endl;

    // License-related information
    std::cout << " " << Constants::applicationName <<
                 " Copyright (C) " << Constants::copyrightYear << " " <<
                 Constants::applicationAuthor << "\n" <<
                 " This program comes with ABSOLUTELY NO WARRANTY. "
                 "This is free software,\n"
                 " and you are welcome to redistribute it under certain "
                 "conditions. See\n"
                 " the GNU General Public License for details." <<
                 std::endl;

    // Separator
    std::cout << "------------------------------------------------------------"
                 "------------\n" << std::endl;
  }



  void writeDefaultConfigurationFile()
  {
    try
    {
      // Check if file exists
      {
        std::ifstream file{mcc::Constants::settingsFilePath};
        if (file.is_open()) {
          std::cout << "File '" << mcc::Constants::settingsFilePath <<
                      "' already exists. Rename or delete the file before "
                      "writing default configuration file." << std::endl;
          file.close();
          return;
        }
      }

      // Open the new settings file and write default configuration to it
      std::ofstream file{mcc::Constants::settingsFilePath};
      if (!file.is_open()) {
        std::cout << "Could not open file '" <<
                     mcc::Constants::settingsFilePath << "' for writing." <<
                     std::endl;
        return;
      }

      std::cout << "Writing default configuration file '" <<
                   mcc::Constants::settingsFilePath << "'." << std::endl;
      file << DEFAULT_CONFIGURATION_FILE;
      file.close();
    }
    catch(const std::exception& e)
    {
      std::cerr << e.what() << '\n';
    }
  }
}
