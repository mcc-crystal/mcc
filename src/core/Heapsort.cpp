#include "Heapsort.hpp"

#include <cmath>
#include <stdexcept>



namespace mcc
{
  template <class T>
  Heapsort<T>::Heapsort(std::vector<T>& sortContainer) :
               sortContainer_{sortContainer}
  { }



  template <class T>
  void Heapsort<T>::sortAscending()
  {
    // Initialization
    nElements_ = sortContainer_.size();
    iStart_ = 0;
    iEnd_ = nElements_ - 1;

    heapifyAscending();
    iStart_ = 0;
    while (iEnd_ > 0) {
      // a[0] is the root and largest value. The swap moves it in front of the
      // sorted elements.
      swapElements(iEnd_, 0);
      // The heap size is reduced by one
      iEnd_--;
      // The swap ruined the heap property, so restore it
      siftDownAscending();
    }
  }



  template <class T>
  void Heapsort<T>::sortDescending()
  {
    // Initialization
    nElements_ = sortContainer_.size();
    iStart_ = 0;
    iEnd_ = nElements_ - 1;

    heapifyDescending();
    iStart_ = 0;
    while (iEnd_ > 0) {
      // a[0] is the root and largest value. The swap moves it in front of the
      // sorted elements.
      swapElements(iEnd_, 0);
      // The heap size is reduced by one
      iEnd_--;
      // The swap ruined the heap property, so restore it
      siftDownDescending();
    }
  }



  template <class T>
  short Heapsort<T>::compareElements(const T& element1,
                                     const T& element2) const
  {
    throw std::logic_error("Unsupported type, cannot compare elements.");
  }



  template <class T>
  short Heapsort<T>::compareElements(int iElement1,
                                     int iElement2) const
  {
    return compareElements(sortContainer_[iElement1],
                           sortContainer_[iElement2]);
  }



  template <class T>
  void Heapsort<T>::heapifyAscending()
  {
    // Start is assigned the index in the container of the last parent node.
    // Find the parent of last element
    iStart_ = calculateIndexOfParent(nElements_ - 1);

    while (iStart_ >= 0) {
      // Sift down the node at index iStart_ to the proper place such that all
      // nodes below the start index are in heap order
      siftDownAscending();
      // Go to the next parent node
      iStart_--;
    }
    // After sifting down the root all nodes/elements are in heap order.
  }



  template <class T>
  void Heapsort<T>::heapifyDescending()
  {
    // Start is assigned the index in the container of the last parent node.
    // Find the parent of last element
    iStart_ = calculateIndexOfParent(nElements_ - 1);

    while (iStart_ >= 0) {
      // Sift down the node at index iStart_ to the proper place such that all
      // nodes below the start index are in heap order
      siftDownDescending();
      // Go to the next parent node
      iStart_--;
    }
    // After sifting down the root all nodes/elements are in heap order.
  }



  template <class T>
  void Heapsort<T>::siftDownAscending()
  {
    iRoot_ = iStart_;
    int iChild = calculateIndexOfLeftChild(iRoot_); // Left child of root

    while (iChild <= iEnd_) {
      iSwap_ = iRoot_; // Keeps track of child to swap with

      short comparisonResult {compareElements(iSwap_, iChild)};
      if (comparisonResult < 0)
        iSwap_ = iChild;

      // If there is a right child and that child is greater
      if (iChild + 1 <= iEnd_) {
        comparisonResult = compareElements(iSwap_, iChild + 1);
        if (comparisonResult < 0)
          iSwap_ = iChild + 1;
      }

      if (iSwap_ == iRoot_)
        // The root holds the largest element. Since we assume the heaps rooted
        // at the children are valid, this means that we are done.
        return;
      else {
        swapElements(iRoot_, iSwap_);
        iRoot_ = iSwap_; // Repeat to continue sifting down the child now
        // Child is checked for existence by loop condition
        iChild = calculateIndexOfLeftChild(iRoot_);
      }
    }
  }



  template <class T>
  void Heapsort<T>::siftDownDescending()
  {
    iRoot_ = iStart_;
    int iChild = calculateIndexOfLeftChild(iRoot_); // Left child of root

    while (iChild <= iEnd_) {
      iSwap_ = iRoot_; // Keeps track of child to swap with

      short comparisonResult {compareElements(iSwap_, iChild)};
      if (comparisonResult > 0)
        iSwap_ = iChild;

      // If there is a right child and that child is greater
      if (iChild + 1 <= iEnd_) {
        comparisonResult = compareElements(iSwap_, iChild + 1);
        if (comparisonResult > 0)
          iSwap_ = iChild + 1;
      }

      if (iSwap_ == iRoot_)
        // The root holds the largest element. Since we assume the heaps rooted
        // at the children are valid, this means that we are done.
        return;
      else {
        swapElements(iRoot_, iSwap_);
        iRoot_ = iSwap_; // Repeat to continue sifting down the child now
        // Child is checked for existence by loop condition
        iChild = calculateIndexOfLeftChild(iRoot_);
      }
    }
  }



  template <class T>
  int Heapsort<T>::calculateIndexOfParent(int iNode) const
  {
    return static_cast<int>(std::floor((iNode - 1) / 2.0));
  }



  template <class T>
  int Heapsort<T>::calculateIndexOfLeftChild(int iNode) const
  {
    return 2 * iNode + 1;
  }



  template <class T>
  int Heapsort<T>::calculateIndexOfRightChild(int iNode) const
  {
    return 2 * iNode + 2;
  }



  template <class T>
  void Heapsort<T>::swapElements(int iElement1, int iElement2)
  {
    T temp{sortContainer_[iElement1]};
    sortContainer_[iElement1] = sortContainer_[iElement2];
    sortContainer_[iElement2] = temp;
  }



  template <>
  short Heapsort<Reflection>::compareElements(const Reflection& element1,
                                              const Reflection& element2) const
  {
    if (element1.d < element2.d)
      return -1;
    else if (element1.d > element2.d)
      return 1;
    else
      return 0;
  }



  template <>
  short Heapsort<Peak>::compareElements(const Peak& element1,
                                        const Peak& element2) const
  {
    if (element1.twoTheta < element2.twoTheta)
      return -1;
    else if (element1.twoTheta > element2.twoTheta)
      return 1;
    else
      return 0;
  }



  // Explicit instantiations of supported templates
  template class Heapsort<Reflection>;
  template class Heapsort<Peak>;
}
