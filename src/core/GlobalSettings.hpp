/** \file GlobalSettings.hpp
 *  \brief Declares centralized settings class GlobalSettings.
 */
#ifndef GLOBALSETTINGS_HPP_INCLUDED
#define GLOBALSETTINGS_HPP_INCLUDED

#include <string>
#include <sstream>

#include "CrystalSystem.hpp"



namespace mcc
{

  /** \brief A centralized settings class that stores global settings read
   *         from the settings file. Also provides setting evaluation methods.
   */
  class GlobalSettings
  {
  private:
    /** \brief Array of strings containing valid group names. Last element of
     *         the array is an empty string.
     */
    static const std::string VALID_GROUPS[];
    /** \brief Array of strings containing valid input data formats. The last
     *         element of the array is an empty string.
     */
    static const std::string VALID_FORMATS[];
    /** \brief Array of strings containing valid calculated list module names.
     *         Last element of the array is an empty string.
     */
    static const std::string VALID_CALCULATED_LIST_MODULES[];
    /** \brief Array of strings containing valid observed list module names.
     *         Last element of the array is an empty string.
     */
    static const std::string VALID_OBSERVED_LIST_MODULES[];
    /** \brief Array of strings containing valid energy calculator module
     *         names. Last element of the array is an empty string.
     */
    static const std::string VALID_ENERGY_MODULES[];
    /** \brief Array of strings containing valid peak generator module names.
     *         Last element of the array is an empty string.
     */
    static const std::string VALID_PEAK_GENERATOR_MODULES[];
    /** \brief Array of strings containing valid root module names.
     *         Last element of the array is an empty string.
     */
    static const std::string VALID_ROOT_MODULES[];
    /** \brief Array of strings containing valid temperature module names.
     *         Last element of the array is an empty string.
     */
    static const std::string VALID_TEMPERATURE_MODULES[];
    /** \brief Array of strings containing valid step optimizer module names.
     *         Last element of the array is an empty string.
     */
    static const std::string VALID_STEP_OPTIMIZER_MODULES[];
    /** \brief Minimum allowed angle setting */
    static const double MINIMUM_ANGLE;
    /** \brief Maximum allowed angle setting */
    static const double MAXIMUM_ANGLE;

    // Common variables

    /** \brief Used in setting parsing and evaluation (used to create warning
               and error messages) */
    std::stringstream message_;
    /** \brief Used in setting parsing and evaluation (holds information
               about whether the most recent parse was successful) */
    bool result_;
    /** \brief Used in setting parsing and evaluation (holds the most recently
     *         parsed double value)
     */
    double dValue_;
    /** \brief Used in setting parsing and evaluation (holds the most recently
     *         parsed integer value)
     */
    int iValue_;
    /** \brief Used in setting parsing and evaluation (holds the most recently
     *         parsed group name)
     */
    std::string group_;
    /** \brief Used in setting parsing and evaluation (holds the most recently
     *         parsed setting name)
     */
    std::string setting_;
    /** \brief Used in setting parsing and evaluation (holds the most recently
     *         parsed setting value) */
    std::string value_;

  public:
    // Settings (categorized by their groups)

    // [Main]
    /** \brief Path to the input data file */
    std::string main_dataFile;
    /** \brief Format of the input file */
    std::string main_dataFormat;
    /** \brief Wavelength used in data acquisition [Å] */
    double main_wavelength;
    /** \brief Zero shift of input data [°2Theta]. It is added to observed
     *         2Theta positions.
     */
    double main_zeroShift;
    /** \brief Number of stored best unit cells (solutions) */
    int main_nStoredCells;
    /** \brief Tolerace in [°2Theta] used to determine whether the peak is
     *         indexed or not. Also used in determining number of possible
     *         peaks. Should always be zero or greater.
     */
    double main_twoThetaTolerance;

    // [Cell]
    /** \brief Minimum cell parameter length [Å]. Overrides individual
     *         parameter limits when it is greater than zero.
     */
    double cell_minLength;
    /** \brief Maximum cell parameter length [Å]. Overrides individual
     *         parameter limits when it is greater than zero.
     */
    double cell_maxLength;
    /** \brief Minimum cell parameter angle [°]. Overrides individual
     *         parameter limits when it is greater than zero.
     */
    double cell_minAngle;
    /** \brief Maximum cell parameter angle [°]. Overrides individual
     *         parameter limits when it is greater than zero.
     */
    double cell_maxAngle;
    /** \brief Minimum cell parameter a [Å] */
    double cell_minA;
    /** \brief Minimum cell parameter b [Å] */
    double cell_minB;
    /** \brief Minimum cell parameter c [Å] */
    double cell_minC;
    /** \brief Minimum cell parameter alpha [°] */
    double cell_minAlpha;
    /** \brief Minimum cell parameter beta [°] */
    double cell_minBeta;
    /** \brief Minimum cell parameter gamma [°] */
    double cell_minGamma;
    /** \brief Maximum cell parameter a [Å] */
    double cell_maxA;
    /** \brief Maximum cell parameter b [Å] */
    double cell_maxB;
    /** \brief Maximum cell parameter c [Å] */
    double cell_maxC;
    /** \brief Maximum cell parameter alpha [°] */
    double cell_maxAlpha;
    /** \brief Maximum cell parameter beta [°] */
    double cell_maxBeta;
    /** \brief Maximum cell parameter gamma [°] */
    double cell_maxGamma;
    /** \brief Crystal system of the cell */
    CrystalSystem cell_crystalSystem;

    // [PeakGeneration]
    /** \brief Minimum Miller index h (OBSOLETE) */
    int peakGeneration_hMin;
    /** \brief Minimum Miller index k (OBSOLETE) */
    int peakGeneration_kMin;
    /** \brief Minimum Miller index l (OBSOLETE) */
    int peakGeneration_lMin;
    /** \brief Maximum Miller index h (OBSOLETE) */
    int peakGeneration_hMax;
    /** \brief Maximum Miller index k (OBSOLETE) */
    int peakGeneration_kMax;
    /** \brief Maximum Miller index l (OBSOLETE) */
    int peakGeneration_lMax;
    /** \brief Minimum Miller index, overrides individual index limits
     *         when it is zero (OBSOLETE) .
     */
    int peakGeneration_minIndex;
    /** \brief Maximum Miller index, overrides individual index limits
     *         when it is zero (OBSOLETE) .
     */
    int peakGeneration_maxIndex;
    /** \brief Maximum 2Theta of generated peaks [°] (OBSOLETE) */
    double peakGeneration_twoThetaMax;

    // [GridSearch]
    /** \brief Grid search step for lengths [Å] */
    double gridSearch_lengthStep;
    /** \brief Grid search step for angles [°] */
    double gridSearch_angleStep;

    // [MonteCarlo]
    /** \brief Number of maximum Monte Carlo steps per simulation */
    int monteCarlo_nMaxSteps;
    /** \brief Number of maximum Monte Carlo simulations */
    int monteCarlo_nMaxSimulations;
    /** \brief Ideal ratio of accepted versus all steps in Monte Carlo
     *         simulations.
     */
    double monteCarlo_acceptedStepRatio;
    /** \brief Number of times all length cell parameters are changed before
     *         proceeding to angles. */
    int monteCarlo_nLengthStepCycles;
    /** \brief Number of times all angle cell parameters are changed before
     *         proceeding back to lengths. */
    int monteCarlo_nAngleStepCycles;

    // [Temperature]
    /** \brief Minimum temperature returned by temperature controllers */
    double temperature_minimumTemperature;
    /** \brief Intercept value for linear temperature controller */
    double temperature_linearIntercept;
    /** \brief Slope value for linear temperature controller */
    double temperature_linearSlope;
    /** \brief Maximum temperature for automatic temperature parameter
     *         calculation */
    double temperature_autoMaxTemperature;
    /** \brief Minimum temperature for automatic temperature parameter
     *         calculation */
    double temperature_autoMinTemperature;
    /** \brief Number of steps temperature should remain constant at the
     *         beginning of simulation. For automatic temperature parameter
     *         calculation. */
    int temperature_autoStartConstantSteps;
    /** \brief Number of steps temperature should remain constant at the end of
     *         simulation. For automatic temperature parameter calculation.
     */
    int temperature_autoEndConstantSteps;
    /** \brief Number of temperature periods per one simulation.
     *         Only applicable to auto temperature controllers with periodic
     *         temperature functions.
     */
    double temperature_autoPeriodsPerSimulation;

    // [Energy]
    /** \brief Scale factor for (N(possible) - N)-based energy penalty
     *         calculation in module SmithSnyderEnergyEx.
     */
    double energy_SmithSnyderExFactor;
    /** \brief Base of exponential function used in EmeraldEnergyEx energy
     *         calculator to penalize excess calcualted reflections. Greater
     *         base forces smaller unit cells.
     */
    double energy_EmeraldExpBase;
    /** \brief Factor used in EmeraldEnergyEx to adjust sensitivity to
     *         unindexed peaks. Greater than 1.0 increases sensitivity while
     *         factor between 0.0 and 1.0 lowers sensitivity.
     */
    double energy_EmeraldUnindexedFactor;


    // [Modules]
    /** \brief Calculated peak list module */
    std::string modules_calculatedList;
    /** \brief Observed peak list module */
    std::string modules_observedList;
    /** \brief Energy calculation module */
    std::string modules_energyCalculator;
    /** \brief Calculated peak generator module */
    std::string modules_peakGenerator;
    /** \brief Main controller (root) module */
    std::string modules_rootController;
    /** \brief Temperature controller module for Monte Carlo simulations */
    std::string modules_temperatureController;
    /** \brief Step optimization module for Monte Carlo simulations */
    std::string modules_stepOptimizer;

    // [Parallel]
    /** \brief Number of threads to spawn */
    int parallel_nThreads;
    /** \brief Used to divide each parameter area to make work plans, which are
     *         picked up by individual threads.
     */
    int parallel_workAreaDivider;

    // [Solutions]
    /** \brief Length parameter tolerance to treat solutions as identical [Å]
     */
    double solutions_lengthTolerance;
    /** \brief Angle parameter tolerance to treat solutions as identical [°] */
    double solutions_angleTolerance;
    /** \brief Energy tolerance to treat solutions as identical */
    double solutions_energyTolerance;

    // [Logging]
    /** \brief If greater than zero, tracking each unit cell step and output
     *         to files is enabled.
     */
    int logging_cellTracker;


  public:
    /** \brief Initializes settings to their default values.
     *
     */
    GlobalSettings();

    /** \brief Evaluates tuple of setting group, name, and value. Outputs
     *         error and warning messages for invalid settings. Saves
     *         setting in the object if it's valid.
     *
     *  \param group Group of the setting
     *  \param setting Name of the setting
     *  \param value Value of the setting
     *  \return True if setting is valid and saved into the object,
     *          false otherwise
     *
     */
    bool evaluateSetting(const std::string& group, const std::string& setting,
                         const std::string& value);

    /** \brief Evaluates group name by checking it against the list of valid
     *         groups (VALID_GROUPS).
     *
     *  \param group Name of the group
     *  \return True if group name is valid, false otherwise
     *
     */
    bool evaluateGroup(const std::string& group) const;

    /** \brief Performs checks and changes that should be done after
     *         all the settings are in place. Outputs errors and warnings to
     *         the console. Meant to be called after all user settings are
     *         saved in the object.
     *
     * \return True if post evaluation succeeded, false otherwise
     */
    bool performPostEvaluation();


  private:
    /** \brief Outputs an error message to the console with object-specific
     *         information. Terminates the line.
     *
     *  \param message Message to be written
     */
    void reportError(const std::string& message) const;

    /** \brief Outputs a warning message to the console with object-specific
     *         information. Terminates the line.
     *
     *  \param message Message to be written
     */
    void reportWarning(const std::string& message) const;

    /** \brief Parses a floating point number from given string while catching
     *         possible conversion exceptions.
     *
     *  \param value String to be converted
     *  \param result Reference to the variable to store conversion
     *                result (not modified if exceptions are thrown)
     *  \return True if conversion was successful, false otherwise
     */
    bool parseDouble(const std::string& value, double& result) const;

    /** \brief Parses an integer from given string while catching
     *         possible conversion exceptions.
     *
     *  \param value String to be converted
     *  \param result Reference to the variable to store conversion
     *                result (not modified if exceptions are thrown)
     *  \return True if conversion was successful, false otherwise
     */
    bool parseInteger(const std::string& value, int& result) const;

    /** \brief Evaluates and parses any valid integer.
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateAnyInteger(int& outVariable);

    /** \brief Evaluates and parses any valid double.
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateAnyDouble(double& outVariable);

    /** \brief Evaluates and parses integer that is greater than zero.
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateGreaterThanZeroInteger(int& outVariable);

    /** \brief Evaluates and parses integer that is zero or greater than zero.
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateZeroOrGreaterInteger(int& outVariable);

    /** \brief Evaluates and parses double that is greater than zero.
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateGreaterThanZeroDouble(double& outVariable);

    /** \brief Evaluates and parses double that is zero or greater than zero.
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateZeroOrGreaterDouble(double& outVariable);

    /** \brief Evaluates and parses an angle (a double between MINIMUM_ANGLE
     *         and MAXIMUM_ANGLE).
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateAngle(double& outVariable);

    /** \brief Evaluates and parses an angle that can also be zero or negative
     *         in addition to being between MINIMUM_ANGLE and MAXIMUM_ANGLE.
     *
     *  \param outVariable Where to store the result if it is valid
     *  \return True if evaluation was successful, otherwise false
     */
    bool evaluateZeroableAngle(double& outVariable);

    /** \brief Evaluates module name against an array of valid names.
     *         Stores the name into the target variable if valid.
     *
     * \param validNames Array of strings to validate against (terminated by
     *                   an empty string)
     * \param outVariable Target variable to write evaluated name to
     * \return True if name is valid, false otherwise
     */
    bool evaluateModuleName(const std::string validNames[],
                            std::string& outVariable);
  };
}



#endif // GLOBALSETTINGS_HPP_INCLUDED
