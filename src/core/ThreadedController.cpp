#include "ThreadedController.hpp"

#include <string>

#include "../peak_list/DynamicCalculatedList.hpp"
#include "../peak_generation/AllowedPeakGenerator.hpp"
#include "../peak_list/DynamicObservedList.hpp"
#include "../energy/SmithSnyderEnergy.hpp"
#include "../energy/SmithSnyderEnergyEx.hpp"
#include "../energy/EmeraldEnergy.hpp"
#include "../energy/EmeraldEnergyEx.hpp"



namespace mcc
{
  ThreadedController::ThreadedController(const GlobalSettings& settings,
                                         const std::string& threadName,
                                         const ObservedPeakList& sourceList) :
                      Module{settings},
                      calculatedList_{nullptr},
                      observedList_{nullptr},
                      peakGenerator_{nullptr},
                      energyCalculator_{nullptr},
                      modulesInitialized_{false},
                      threadName_{threadName},
                      noInterruptReceived_{true}
  {
    modulesInitialized_ = initializeModules();

    if (modulesInitialized_) {
      observedList_->copyData(sourceList);
    }
  }



  ThreadedController::~ThreadedController()
  {
    delete calculatedList_;
    delete observedList_;
    delete peakGenerator_;
    delete energyCalculator_;
  }



  std::string ThreadedController::getModuleName() const
  {
    return "ThreadedController";
  }



  void ThreadedController::signalInterrupt()
  {
    noInterruptReceived_ = false;
  }



  bool ThreadedController::initializeModules()
  {
    bool noErrors {true};

    // Observed list
    std::string observedListModule = globalSettings_.modules_observedList;
    if (observedListModule == "Dynamic") {
      observedList_ = new DynamicObservedList{globalSettings_};
    }
    else {
      reportError("Observed list module " + observedListModule +
                  " not found!");
      noErrors = false;
    }

    // Peak generator
    std::string peakGenModule = globalSettings_.modules_peakGenerator;
    if (observedList_ == nullptr) {
      reportError("Cannot initialize peak generator without observed peak "
                  "list object.");
      noErrors = false;
    }
    if (peakGenModule == "Allowed") {
      // Unit cell is set later
      peakGenerator_ = new AllowedPeakGenerator(globalSettings_,
                                                observedList_);
    }
    else {
      reportError("Peak generator module " + peakGenModule + " not found!");
      noErrors = false;
    }

    // Calculated list
    std::string calculatedListModule = globalSettings_.modules_calculatedList;
    if (peakGenerator_ == nullptr) {
      reportError("Cannot initialize calculated peak list without a peak "
                  "generator object.");
      noErrors = false;
    }
    if (calculatedListModule == "Dynamic") {
      calculatedList_ = new DynamicCalculatedList(globalSettings_);
    }
    else {
      reportError("Calculated list module " + calculatedListModule +
                  " not found!");
      noErrors = false;
    }

    // Energy calculator
    std::string energyModule = globalSettings_.modules_energyCalculator;
    if (energyModule == "SmithSnyder")
      energyCalculator_ = new SmithSnyderEnergy(globalSettings_,
                                                *calculatedList_,
                                                *observedList_);
    else if (energyModule == "SmithSnyderEx")
      energyCalculator_ = new SmithSnyderEnergyEx(globalSettings_,
                                                  *calculatedList_,
                                                  *observedList_);
    else if (energyModule == "Emerald")
      energyCalculator_ = new EmeraldEnergy(globalSettings_,
                                            *calculatedList_,
                                            *observedList_);
    else if (energyModule == "EmeraldEx")
      energyCalculator_ = new EmeraldEnergyEx(globalSettings_,
                                              *calculatedList_,
                                              *observedList_);
    else {
      reportError("Energy module " + peakGenModule + " not found!");
      noErrors = false;
    }


    // Check modules
    if (calculatedList_ == nullptr ||
        observedList_ == nullptr ||
        energyCalculator_ == nullptr ||
        peakGenerator_ == nullptr) {
      reportError("One or more modules are uninitialized!");
      noErrors = false;
    }

    return noErrors;
  }
}
