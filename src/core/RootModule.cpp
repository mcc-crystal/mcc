#include "RootModule.hpp"

#include "../solution/DynamicSolutionStorage.hpp"



namespace mcc
{
  // Static initialization
  const int RootModule::INTERFACE_UPDATE_INTERVAL = 1300;



  RootModule::RootModule(const GlobalSettings& settings) :
              globalSettings_{settings},
              solutions_{nullptr}
  {
    // Initialize from settings
    nThreads_ = globalSettings_.parallel_nThreads;

    // Initialize modules
    initializeModules();
  }



  RootModule::~RootModule()
  {
    delete solutions_;
  }



  double RootModule::getWorstEnergy() const
  {
    return solutions_->getWorstEnergy();
  }



  void RootModule::submitSolution(const Solution& solution)
  {
    solutionsMutex_.lock();
    solutions_->submitSolution(solution);
    solutionsMutex_.unlock();
  }



  int RootModule::getSolutionCount() const
  {
    return solutions_->getSolutionCount();
  }



  int RootModule::getSolutionCountSafe()
  {
    solutionsMutex_.lock();
    int n = getSolutionCount();
    solutionsMutex_.unlock();
    return n;
  }



  Solution RootModule::getSolution(int i) const
  {
    return solutions_->getSolution(i);
  }



  Solution RootModule::getBestSolution()
  {
    solutionsMutex_.lock();
    Solution s(getSolution(0));
    solutionsMutex_.unlock();
    return s;
  }



  const SolutionStorage& RootModule::getSolutionStorage() const
  {
    return *solutions_;
  }



  bool RootModule::initializeModules()
  {
    solutions_ = new DynamicSolutionStorage(globalSettings_);
    return true;
  }

}
