#include "UnitCell.hpp"

#include <cmath>

// For compatibility with other compilers
#ifndef M_PI
#define M_PI 3.14159265358979323846d
#endif // M_PI

#include "CrystalSystem.hpp"



namespace mcc
{
  UnitCell::UnitCell() :
            a{0.0},
            b{0.0},
            c{0.0},
            alpha{0.0},
            beta{0.0},
            gamma{0.0}
  { }



  void UnitCell::copyData(const UnitCell& source)
  {
    a = source.a;
    b = source.b;
    c = source.c;
    alpha = source.alpha;
    beta = source.beta;
    gamma = source.gamma;
  }



  double CubicCell::calculateD(int h, int k, int l) const
  {
    return a / std::sqrt(h*h + k*k + l*l);
  }



  double CubicCell::calculateVolume() const
  {
    return a * a * a;
  }



  CrystalSystem CubicCell::getCrystalSystem() const
  {
    return CrystalSystem::CUBIC;
  }



  double HexagonalCell::calculateD(int h, int k, int l) const
  {
    return 1.0 / std::sqrt((4.0/3.0 / (a*a)) *
                           (h*h + k*k + h*k) + (l*l / (c*c)));
  }



  double HexagonalCell::calculateVolume() const
  {
    // V = a² * c * sin(60°)
    return a * a * c * 0.8660254037844386467637231707529361834714;
  }



  CrystalSystem HexagonalCell::getCrystalSystem() const
  {
    return CrystalSystem::HEXAGONAL;
  }



  double TetragonalCell::calculateD(int h, int k, int l) const
  {
    double aSquared {a * a};
    return 1.0 / std::sqrt((h*h / aSquared) +
                           (k*k / aSquared) +
                           (l*l / (c*c)));
  }



  double TetragonalCell::calculateVolume() const
  {
    return a * a * c;
  }



  CrystalSystem TetragonalCell::getCrystalSystem() const
  {
    return CrystalSystem::TETRAGONAL;
  }



  double OrthorhombicCell::calculateD(int h, int k, int l) const
  {
    return 1.0 / std::sqrt((h*h / (a*a)) + (k*k / (b*b)) + (l*l / (c*c)));
  }



  double OrthorhombicCell::calculateVolume() const
  {
    return a * b * c;
  }



  CrystalSystem OrthorhombicCell::getCrystalSystem() const
  {
    return CrystalSystem::ORTHORHOMBIC;
  }



  double MonoclinicCell::calculateD(int h, int k, int l) const
  {
    double d;
    double betaRad = beta * M_PI / 180.0; // [rad]
    double cosBeta = std::cos(betaRad);
    double sinBeta = std::sin(betaRad);
    d = (h*h / (a*a)) + (l*l / (c*c)) - (2.0 * h*l / (a*c)) * cosBeta;
    d /= (sinBeta * sinBeta);
    d += (k*k) / (b*b);
    d = 1.0 / std::sqrt(d);
    return d;
  }



  double MonoclinicCell::calculateVolume() const
  {
    // V = a * b * c * sin(beta)
    return a * b * c * std::sin(beta * M_PI / 180.0);
  }



  CrystalSystem MonoclinicCell::getCrystalSystem() const
  {
    return CrystalSystem::MONOCLINIC;
  }



  double TriclinicCell::calculateD(int h, int k, int l) const
  {
    double d;
    double upper;
    double lower;

    double alphaRad = alpha * M_PI / 180.0; // [rad]
    double betaRad = beta * M_PI / 180.0; // [rad]
    double gammaRad = gamma * M_PI / 180.0; // [rad]
    double cosAlpha = std::cos(alphaRad);
    double cosBeta = std::cos(betaRad);
    double cosGamma = std::cos(gammaRad);

    upper = (h/a) * (h/a * (1 - cosAlpha * cosAlpha) +
                     l/c * (cosAlpha * cosGamma - cosBeta) +
                     k/b * (cosAlpha * cosBeta - cosGamma)) +
            (k/b) * (k/b * (1 - cosBeta * cosBeta) +
                     h/a * (cosAlpha * cosBeta - cosGamma) +
                     l/c * (cosBeta * cosGamma - cosAlpha)) +
            (l/c) * (l/c * (1 - cosGamma * cosGamma) +
                     k/b * (cosBeta * cosGamma - cosAlpha) +
                     h/a * (cosAlpha * cosGamma - cosBeta));

    lower = 1.0 + 2.0 * cosAlpha * cosBeta * cosGamma -
                  cosAlpha * cosAlpha -
                  cosBeta * cosBeta -
                  cosGamma * cosGamma;

    d = std::sqrt(lower / upper);

    return d;
  }



  double TriclinicCell::calculateVolume() const
  {
    double v;

    double alphaRad = alpha * M_PI / 180.0; // [rad]
    double betaRad = beta * M_PI / 180.0; // [rad]
    double gammaRad = gamma * M_PI / 180.0; // [rad]
    double cosAlpha = std::cos(alphaRad);
    double cosBeta = std::cos(betaRad);
    double cosGamma = std::cos(gammaRad);

    /*
    v = a * b * c *
        (1.0 - cosAlpha * cosAlpha - cosBeta * cosBeta - cosGamma * cosGamma) +
        2.0 * sqrt(cosAlpha * cosBeta * cosGamma);
    */
    v = a * b * c *
        std::sqrt(1.0 + 2.0 * cosAlpha * cosBeta * cosGamma -
                  cosAlpha * cosAlpha -
                  cosBeta * cosBeta -
                  cosGamma * cosGamma);

    return v;
  }



  CrystalSystem TriclinicCell::getCrystalSystem() const
  {
    return CrystalSystem::TRICLINIC;
  }

}
