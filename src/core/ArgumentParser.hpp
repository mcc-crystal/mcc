/** \file ArgumentParser.hpp
 *  \brief Declares a class to parse command line parameters.
 */

#ifndef ARGUMENTPARSER_HPP_INCLUDED
#define ARGUMENTPARSER_HPP_INCLUDED

#include <string>



namespace mcc
{
  /** \brief Parses command line parameters and provides MCC-specific parse
   *         results.
   */
  class ArgumentParser
  {
  public:
    /** \brief Parses command line arguments and returns parse result.
     * 
     *  Potential parsing issues are printed to the console. If parse result
     *  is false, program should be terminated.
     * 
     *  \param argc Argument count
     *  \param argv Argument vector
     *  \param outResult Reference to the variable to write parse result to;
     *         true in case of a successful parse, false otherwise
     */
    ArgumentParser(int argc, const char* argv[], bool& outResult);

    /** \brief Get parsed value of command with which the program was run
     *         (argv[0]).
     * 
     *  \return Command with which the program was run
     */
    std::string getCommandName() const;

    /** \brief Get parsed value for whether to write default configuration
     *         file, controlled by switch -c.
     * 
     *  \return True if default configuration file should be written
     */
    bool getWriteConfigurationFile() const;

    /** \brief Get path to alternate configuration file that should be used
     *         instead of the default one.
     * 
     *  \return Path to alternate configuration file or empty string if one
     *          was not provided
     */
    std::string getConfigurationFile() const;

    
  private:
    /** \brief Argument count as received from main function */
    int argc_;
    /** \brief Argument vector as received from main function */
    const char** argv_;
    /** \brief Command with which the program was run (argv[0]) */
    std::string commandName_;
    /** \brief Whether to write default configuration file. Controlled by
     *         switch -c.
     */
    bool writeConfigurationFile_;
    /** \brief Path to configuration file to be use instead of the
     *         default one
     */
    std::string configurationFile_;

    /** \brief Parses command line arguments and sets object's result
     *         variables. Prints potential errors to console.
     * 
     *  \return True if parse completed with no error, false otherwise
     */
    bool parseArguments();

    /** \brief Prints a warning in context of argument parsing to the
     *         console. Includes a line terminator.
     * 
     *  \param message Message to be written
     */
    void reportWarning(const std::string& message) const;

    /** \brief Prints an error in context of argument parsing to the
     *         console. Includes a line terminator.
     * 
     *  \param message Message to be written
     */
    void reportError(const std::string& message) const;

    /** \brief Checks whether given command line parameter is a switch (begins
     *         with '-').
     * 
     *  \param parameter Command line parameter to check
     *  \return True if command line parameter is a switch, false otherwise
     */
    bool isSwitch(const std::string& parameter) const;
  };
}



#endif // ARGUMENTPARSER_HPP_INCLUDED