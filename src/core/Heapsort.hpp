/** \file Heapsort.hpp
 *  \brief Declares base template for sorting vectors of custom objects with
 *         heapsort method.
 */

#ifndef HEAPSORT_HPP_INCLUDED
#define HEAPSORT_HPP_INCLUDED

#include <vector>

#include "Reflection.hpp"
#include "Peak.hpp"



namespace mcc
{
  /** \brief Base template for sorting vectors of custom objects with
   *         heapsort method.
   *
   *  Method implementation based on the following article:
   *  https://en.wikipedia.org/wiki/Heapsort
   */
  template<class T>
  class Heapsort
  {
  public:
    /** \brief Creates a new heapsort object and stores the reference to the
     *         container to be sorted.
     *  
     *  \param sortContainer Reference to the container to be sorted
     */
    explicit Heapsort(std::vector<T>& sortContainer);

    /** \brief Sorts elements in the target container in ascending order by
     *         swapping them directly (modifying the container).
     */
    void sortAscending();

    /** \brief Sorts elements in the target container in descending order by
     *         swapping them directly (modifying the container).
     */
    void sortDescending();

  protected:
    /** \brief Compares two elements of the container (by reference). Throws
     *         an exception if unsupported type T is used.
     *
     *  \param element1 First element to be compared
     *  \param element2 Second element to be compared
     *  \return -1 if first element is smaller than the second,
     *          +1 if first element is greater than the second,
     *          or 0 if elements are equal.
     *
     */
    short compareElements(const T& element1, const T& element2) const;

    /** \brief Compares two elements of the container (by index).
     *
     * \param iElement1 Index that points to the first element
     * \param iElement2 Index that points to the second element
     * \return -1 if first element is smaller than the second,
     *         +1 if first element is greater than the second,
     *         or 0 if elements are equal.
     *
     */
    short compareElements(int iElement1, int iElement2) const;

  private:
    /** \brief Reference to the container to sort (also referred to as heap) */
    std::vector<T>& sortContainer_;
    /** \brief Number of elements in the original container */
    int nElements_;
    /** \brief Index pointing to the root element of the heap */
    int iStart_;
    /** \brief Variable used in sorting process */
    int iEnd_;
    /** \brief Variable used in sorting process */
    int iRoot_;
    /** \brief Variable used in sorting process */
    int iSwap_;

    /** \brief Puts elements of the container in heap order, in-place.
     *         Ascending sort version.
     *
     * The only difference between ascending and descending versions is call
     * of the proper sift down method.
     */
    void heapifyAscending();

    /** \brief Puts elements of the container in heap order, in-place.
     *         Descending sort version.
     *
     * The only difference between ascending and descending versions is call
     * of the proper sift down method.
     */
    void heapifyDescending();

    /** \brief Repairs the heap whose root element is at index iStart_,
     *         assuming the heaps rooted at its children are valid.
     *         Ascending version.
     *
     * The difference between the ascending and descending version is
     * inequality check of compare result.
     */
    void siftDownAscending();

    /** \brief Repairs the heap whose root element is at index iStart_,
     *         assuming the heaps rooted at its children are valid.
     *         Descending version.
     *
     * The difference between the ascending and descending version is
     * inequality check of compare result.
     */
    void siftDownDescending();

    /** \brief Calculates index of parent of node at index iNode.
     *
     * \param iNode Index of child node
     * \return Index of parent node
     */
    int calculateIndexOfParent(int iNode) const;

    /** \brief Calculates index of left child of node at index iNode.
     *
     * \param iNode Index of parent node
     * \return Index of left child of parent node
     */
    int calculateIndexOfLeftChild(int iNode) const;

    /** \brief Calculates index of right child of node at index iNode.
     *
     * \param iNode Index of parent node
     * \return Index of right child of parent node
     */
    int calculateIndexOfRightChild(int iNode) const;

    /** \brief Swaps elements pointed two by given indices.
     *
     * \param iElement1 Index of the first element
     * \param iElement2 Index of the second element
     */
    void swapElements(int iElement1, int iElement2);
  };



  // Template specializations

  /** \brief Comparison specialization for class Reflection.
   *
   * \param element1 First element to be compared
   * \param element2 Second element to be compared
   * \return -1 if first element is smaller than the second,
   *         +1 if first element is greater than the second,
   *         or 0 if elements are equal.
   */
  template <>
  short Heapsort<Reflection>::compareElements(const Reflection& element1,
                                             const Reflection& element2) const;

  /** \brief Comparison specialization for class Peak.
   *
   * \param element1 First element to be compared
   * \param element2 Second element to be compared
   * \return -1 if first element is smaller than the second,
   *         +1 if first element is greater than the second,
   *         or 0 if elements are equal.
   */
  template <>
  short Heapsort<Peak>::compareElements(const Peak& element1,
                                        const Peak& element2) const;
}



#endif // HEAPSORT_HPP_INCLUDED
