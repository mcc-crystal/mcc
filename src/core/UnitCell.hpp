/** \file UnitCell.hpp
 *  \brief Declares base class for unit cells and all specialized classes for
 *         unit cells of certain symmetry.
 */

#ifndef UNITCELL_HPP_INCLUDED
#define UNITCELL_HPP_INCLUDED

#include "CrystalSystem.hpp"



namespace mcc
{
  /** \brief Represents a crystallographic unit cell with its parameters.
   *         Abstract base class for unit cells.
   */
  class UnitCell
  {
  public:
    /** \brief Parameter a [Å] */
    double a;
    /** \brief Parameter b [Å] */
    double b;
    /** \brief Parameter c [Å] */
    double c;
    /** \brief Parameter alpha [°] */
    double alpha;
    /** \brief Parameter beta [°] */
    double beta;
    /** \brief Parameter gamma [°] */
    double gamma;

    /** Initializes all unit cell parameters to 0.0.
     */
    UnitCell();

    virtual ~UnitCell() = default;

    /** \brief Calculates distance between crystal planes.
     *
     * \param h index h
     * \param k index k
     * \param l index l
     * \return interplanar distance [Å]
     */
    virtual double calculateD(int h, int k, int l) const = 0;

    /** \brief Calculates volume of the unit cell.
     *
     *  \return Unit cell volume [Å³]
     */
    virtual double calculateVolume() const = 0;

    /** \brief Returns crystal system of the cell.
     *
     *  \return Crystal system of the cell
     */
    virtual CrystalSystem getCrystalSystem() const = 0;

    /** \brief Copies all member data from source UnitCell object.
     *
     *  \param source Source object
     */
    void copyData(const UnitCell& source);
  };



  /** \brief Represents a cubic unit cell.
   */
  class CubicCell : public UnitCell
  {
  public:
    double calculateD(int h, int k, int l) const override;
    double calculateVolume() const override;
    CrystalSystem getCrystalSystem() const override;
  };



  /** \brief Represents a hexagonal unit cell.
   */
  class HexagonalCell : public UnitCell
  {
  public:
    double calculateD(int h, int k, int l) const override;
    double calculateVolume() const override;
    CrystalSystem getCrystalSystem() const override;
  };



  /** \brief Represents a tetragonal unit cell.
   */
  class TetragonalCell : public UnitCell
  {
  public:
    double calculateD(int h, int k, int l) const override;
    double calculateVolume() const override;
    CrystalSystem getCrystalSystem() const override;
  };



  /** \brief Represents an orthorhombic unit cell.
   */
  class OrthorhombicCell : public UnitCell
  {
  public:
    double calculateD(int h, int k, int l) const override;
    double calculateVolume() const override;
    CrystalSystem getCrystalSystem() const override;
  };



  /** \brief Represents a monoclinic unit cell.
   */
  class MonoclinicCell : public UnitCell
  {
  public:
    double calculateD(int h, int k, int l) const override;
    double calculateVolume() const override;
    CrystalSystem getCrystalSystem() const override;
  };



  /** \brief Represents a triclinic unit cell.
   */
  class TriclinicCell : public UnitCell
  {
  public:
    double calculateD(int h, int k, int l) const override;
    double calculateVolume() const override;
    CrystalSystem getCrystalSystem() const override;
  };
}



#endif // UNITCELL_HPP_INCLUDED
