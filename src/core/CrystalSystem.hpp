/** \file CrystalSystem.hpp
 *  \brief Declares CrystalSystem enum class and crystal system strings.
 */


#ifndef CRYSTALSYSTEM_HPP_INCLUDED
#define CRYSTALSYSTEM_HPP_INCLUDED

#include <string>



namespace mcc
{
  /** \brief Contains values that represent crystal systems.
   */
  enum class CrystalSystem : int
  {
    CUBIC = 0,
    HEXAGONAL = 1,
    TETRAGONAL = 2,
    ORTHORHOMBIC = 3,
    MONOCLINIC = 4,
    TRICLINIC = 5
  };

  /** \brief An array of strings that (by index) correspond to CrystalSystem
   *         enum class. The last element is an empty string.
   *
   */
  const std::string CrystalSystemStrings[] = {
              "Cubic",
              "Hexagonal",
              "Tetragonal",
              "Orthorhombic",
              "Monoclinic",
              "Triclinic",
              ""
              };
}



#endif // CRYSTALSYSTEM_HPP_INCLUDED
