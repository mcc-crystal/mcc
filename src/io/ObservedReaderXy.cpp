#include "ObservedReaderXy.hpp"

#include <exception>
#include <fstream>
#include <sstream>
#include <iostream>



namespace mcc
{
  ObservedReaderXy::ObservedReaderXy(const GlobalSettings& settings) :
                    ObservedFileReader{settings},
                    twoTheta_{0.0},
                    intensity_{0.0}
  { }



  bool ObservedReaderXy::extractData(ObservedPeakList& observedList)
  {
    noErrors_ = true;
    lineCount_ = 0;
    std::ifstream inputFile;

    try {
      inputFile.open(filePath_);

      if (!inputFile.is_open()) {
        std::stringstream message;
        message << "Could not open data file '" << filePath_ << "'.";
        reportError(message.str());
        return false;
      }

      while (std::getline(inputFile, line_)) {
        bool result;
        lineCount_++;
        parseIndex_ = 0;
        intensity_ = 0.0;

        // 2Theta
        skipWhitespace();
        result = checkAndReportInvalidCharacter(line_[parseIndex_]);
        if (result) continue;
        // Skip empty line
        if (line_[parseIndex_] == '\0') continue;
        // Capture 2Theta
        captureNumber();
        twoTheta_ = parseDouble();
        // Ignore peaks with intentional negative 2Theta or at 0.0 °2Theta
        if (twoTheta_ <= 0.0) continue;
        // Apply zero shift
        twoTheta_ += zeroShift_;
        // Ignore peaks with negative or zero 2Theta because of zero shift
        if (twoTheta_ <= 0.0) continue;

        // Intensity
        skipWhitespace();
        result = checkAndReportInvalidCharacter(line_[parseIndex_]);
        if (result) continue;
        // Check if no intensity information follows
        if (line_[parseIndex_] == '\0') {
          submitPeak(observedList);
          continue;
        }
        // Capture intensity
        captureNumber();
        intensity_ = parseDouble();
        submitPeak(observedList);

        // Check for trailing garbage
        skipWhitespace();
        if (line_[parseIndex_] == '\0') continue;
        result = checkAndReportInvalidCharacter(line_[parseIndex_]);
        if (result) continue;
        else {
          reportWarning("Line contains unnecessary trailing characters.");
        }
      }
    }
    catch (const std::exception& e) {
      std::stringstream message;
      message << "Failure while reading file: " << e.what();
      reportError(message.str());
    }

    if (inputFile.is_open()) inputFile.close();

    return noErrors_;
  }



  bool ObservedReaderXy::checkAndReportInvalidCharacter(char c)
  {
    bool result {isInvalidCharacter(c)};

    if (result) {
      std::stringstream message;
      message << "Encountered invalid character '" << c << "'. " <<
                 "Line ignored.";
      reportError(message.str());
    }

    return result;
  }



  void ObservedReaderXy::submitPeak(ObservedPeakList& observedList)
  {
    Peak p;
    p.twoTheta = twoTheta_;
    p.intensity = intensity_;
    observedList.addPeak(p);
  }
}
