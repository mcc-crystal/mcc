/** \file FileReaderBase.hpp
 *  \brief Declares base class for file reader objects.
 */

#ifndef FILEREADERBASE_HPP_INCLUDED
#define FILEREADERBASE_HPP_INCLUDED

#include <string>



namespace mcc
{
  /** \brief Base class that provides fundamental common utilities for parsing
   *         text files.
   */
  class FileReaderBase
  {
  public:
    // Constants
    /** \brief Characters treated as whitespace. Array is terminated by '\0'
     *         for iteration purposes.
    */
    static const char WHITESPACE[];

    /** Initializes the object with a file path.
     * 
     *  \param filePath Path to the file to read
     */
    explicit FileReaderBase(const std::string& filePath);

  protected:
    /** \brief Path to the file to read */
    std::string filePath_;
    /** \brief True if no errors occur during file parsing */
    bool noErrors_;
    /** \brief Current line read from the settings file */
    std::string line_;
    /** \brief Number of lines read */
    int lineCount_;
    /** \brief Points to the current character in a line of text. Used when
     *         parsing a line
     */
    int parseIndex_;

    /** \brief Increases parse index until it no longer points to a whitespace
     *         character or reaches end of string.
     * 
     *  \warning Method does not check if string line_ is empty.
     */
    void skipWhitespace();

    /** \brief Checks if given character is whitespace. Whitespace characters
     *         are defined by static variable WHITESPACE.
     *
     *  \param c Character to check
     *  \return True if the character is whitespace, else otherwise
     */
    bool isWhitespace(char c) const;

    /** \brief Checks whether parse index points at a whitespace character as
     *         defined by WHITESPACE.
     * 
     *  This is a shorthand and calls isWhitespace(char) method.
     *
     *  \return True if parse index points at a whitespace character
     */
    bool isAtWhitespace() const;

    /** \brief Outputs an error message on the screen with line number
     *         information and sets noErrors member to false.
     * 
     *  \param message Message to be printed
     */
    void reportError(const std::string& message);

    /** \brief Outputs a warning message on the screen with line number
     *         information.
     * 
     *  \param message Message to be printed
     */
    void reportWarning(const std::string& message) const;

    /** \brief Prints given message on the screen with line number information.
     *
     *  \param message Message to be printed
     */
    virtual void debugMessage(const std::string& message) const;
  };
}



#endif // FILEREADERBASE_HPP_INCLUDED
