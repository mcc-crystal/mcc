#include "ObservedFileReader.hpp"

#include <exception>
#include <string>
#include <sstream>



namespace mcc
{
  // Static constant initialization
  const char ObservedFileReader::NUMBER_CHARACTERS[] =
        {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', '+', '-',
         'e', 'E', '\0' };



  ObservedFileReader::ObservedFileReader(const GlobalSettings& settings) :
                          FileReaderBase{settings.main_dataFile},
                          globalSettings_{settings}
  {
    // Initialize from settings
    zeroShift_ = globalSettings_.main_zeroShift;
  }



  bool ObservedFileReader::isNumberCharacter(char c) const
  {
    for (int i = 0; NUMBER_CHARACTERS[i] != '\0'; ++i) {
      if (c == NUMBER_CHARACTERS[i]) {
        return true;
      }
    }

    return false;
  }



  bool ObservedFileReader::isInvalidCharacter(char c) const
  {
    // Expanded form for easier debugging
    bool isNumber = isNumberCharacter(c);
    bool isNull = (c == '\0');
    bool result = !isNumber && !isNull;
    return result;
  }



  void ObservedFileReader::captureNumber()
  {
    number_.clear();

    while (true) {
      char c {line_[parseIndex_]};
      if (!isNumberCharacter(c) || c == '\0') break;
      number_ += c;
      parseIndex_++;
    }
  }



  double ObservedFileReader::parseDouble()
  {
    double result;

    try {
      result = std::stod(number_);
    }
    catch (const std::exception& e) {
      std::stringstream message;
      message << "Failed to parse float '" << number_ << "'.";
      reportError(message.str());
      result = 0.0;
    }

    return result;
  }
}
