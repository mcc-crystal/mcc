/** \file SettingsReader.hpp
 *  \brief Declares class for reading the settings file.
 */
#ifndef SETTINGSREADER_HPP_INCLUDED
#define SETTINGSREADER_HPP_INCLUDED

#include <string>

#include "FileReaderBase.hpp"
#include "../core/GlobalSettings.hpp"



namespace mcc
{

  /** \brief Reads settings from file and saves them in a GlobalSettings
   *         object.
   */
  class SettingsReader : public FileReaderBase
  {
  public:
    // Constants
    /** \brief Sequence of characters that initiate a comment to be ignored */
    static const char COMMENT_INITIATOR[];
    /** \brief Character treated as quotation mark */
    static const char QUOTE;
    /** \brief Character separating setting and its value */
    static const char ASSIGNMENT_OPERATOR;
    /** \brief Character that precedes group name */
    static const char GROUP_INITIATOR;
    /** \brief Character that follows group name */
    static const char GROUP_TERMINATOR;

  private:
    /** \brief Reference to shared global settings object. Settings read are
     *         written to it.
     */
    GlobalSettings& globalSettings_;
    /** \brief Group name that is not cleared until next group is found */
    std::string currentGroup_;
    /** \brief Group of the current setting */
    std::string group_;
    /** \brief Name of the current setting */
    std::string setting_;
    /** \brief Unparsed value of the current setting */
    std::string value_;

    /** \brief Checks whether parse index points at the beginning of a comment
     *         initiator string.
     *
     *  \return True if parse index points at comment initiator sequence
     */
    bool isAtCommentInitiator() const;

    /** \brief Checks whether parse index points at a quote character
     *         (defined by constant QUOTE).
     *
     *  \return True if parse index points at a quote character
     */
    bool isAtQuote() const;

    /** \brief Returns series of characters from the current position
     *         determined by parse index to the next whitespace or comment
     *         initiator. If parse index points at a quite, it reads until the
     *         next quote.
     *
     *  \return Captured word
     */
    std::string captureWord();

    /** \brief Captures a group name preceeded by group initiator character
     *         and terminated by group terminator character. Parse index
     *         must point at a group initiator character.
     *
     * \return Captured group name or empty string in case of errors
     */
    std::string captureGroup();

    void debugMessage(const std::string& message) const override;


  public:
    /** \brief Initializes the base class and reference to the shared global
     *         settings object.
     * 
     *  \param settingsFile Path to the settings file
     *  \param globalSettings Reference shared global settings object
     */
    SettingsReader(const std::string& settingsFile,
                   GlobalSettings& globalSettings);

    /** \brief Reads settings from the settings file. Returns true if no errors
     *         were encountered or false otherwise.
     *
     *  \return True if no errors were encountered
     */
    bool readFile();
  };
}



#endif // SETTINGSREADER_HPP_INCLUDED
