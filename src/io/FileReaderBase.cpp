#include "FileReaderBase.hpp"

#include <iostream>



namespace mcc
{
  // Static initialization
  // WHITESPACE must be terminated by zero character
  const char FileReaderBase::WHITESPACE[] = {' ', '\t', '\0'};



  FileReaderBase::FileReaderBase(const std::string& filePath) :
                  filePath_{filePath},
                  noErrors_{true},
                  lineCount_{0},
                  parseIndex_{0}
  { }



  void FileReaderBase::skipWhitespace()
  {
    while (line_[parseIndex_] != '\0') {
      if (isWhitespace(line_[parseIndex_])) {
        parseIndex_++;
      }
      else {
        break;
      }
    }
  }



  bool FileReaderBase::isWhitespace(char c) const
  {
    for (int i = 0; WHITESPACE[i] != '\0'; ++i) {
      if (c == WHITESPACE[i]) {
        return true;
      }
    }

    return false;
  }



  bool FileReaderBase::isAtWhitespace() const
  {
    return isWhitespace(line_[parseIndex_]);
  }



  void FileReaderBase::reportError(const std::string& message)
  {
    std::cout << "Error while reading line " << lineCount_ << " of file '" <<
                 filePath_ << "': " << message << std::endl;
    noErrors_ = false;
  }



  void FileReaderBase::reportWarning(const std::string& message) const
  {
    std::cout << "Warning while reading line " << lineCount_ << " of file '" <<
                 filePath_ << "': " << message << std::endl;
  }



  void FileReaderBase::debugMessage(const std::string& message) const
  {
    std::cout << "File reader line " << lineCount_ << ": " << message <<
                 std::endl;
  }
}
