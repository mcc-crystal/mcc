/** \file ObservedFileReader.hpp
 *  \brief Declares base class for observed data file readers.
 */
#ifndef OBSERVEDFILEREADERBASE_HPP_INCLUDED
#define OBSERVEDFILEREADERBASE_HPP_INCLUDED

#include "FileReaderBase.hpp"

#include <string>

#include "../core/GlobalSettings.hpp"
#include "../peak_list/ObservedPeakList.hpp"



namespace mcc
{
  /** \brief Base class for file readers reading input files with
   *         observed data.
   */
  class ObservedFileReader : public FileReaderBase
  {
  public:
    // Static constants
    /** \brief Array of characters which can appear in a number. Last element
      *        is a null character to make iteration easier.
      */
    static const char NUMBER_CHARACTERS[];

    /** \brief Initializes base class FileReaderBase and this object. Also
     *         copies relevant settings from shared object.
     * 
     *  \param settings Reference to shared global settings object
     *  \param observedList Observed peak list to populate with read peaks
     */
    ObservedFileReader(const GlobalSettings& settings);

    /** \brief Reads observed data file and applies zero shift to 2Theta
     *         positions. Peaks with negative 2Theta are ignored. Saves
     *         peaks in provided observed peak list.
     *
     *  \param observedList Observed peak list to populate
     *  \return True if no errors were encountered
     */
    virtual bool extractData(ObservedPeakList& observedList) = 0;

  protected:
    /** \brief Reference to the global settings object */
    const GlobalSettings& globalSettings_;
    /** \brief Buffer for capturing numbers */
    std::string number_;
    /** \brief Observed peaks zero shift from settings */
    double zeroShift_;

    /** \brief Checks whether passed character is in the list of characters,
     *         that can appear in a number, as defined by NUMBER_CHARACTERS.
     *
     *  \param c Character to check
     *  \return True if character belongs to the number group
     */
    bool isNumberCharacter(char c) const;

    /** \brief Checks whether passed character is not a number character nor a
     *         null character, and therefore should not appear in a data line.
     *
     * \param c Character to check
     * \return True if character is invalid
     */
    bool isInvalidCharacter(char c) const;

    /** \brief Captures a number consisting of characters in array
     *         NUMBER_CHARACTERS.
     *
     *  Number in string format is kept in member number_, which is cleared
     *  before capture.
     * 
     *  \warning Method does not check if string line_ is empty.
     */
    void captureNumber();

    /** \brief Tries to convert number in buffer number_ to double. Error is
     *         reported if the conversion fails. In case of failure 0.0 is
     *         returned.
     * 
     *  \return Parsed number or 0.0 in case of errors
     */
    double parseDouble();
  };
}



#endif // OBSERVEDFILEREADERBASE_HPP_INCLUDED
