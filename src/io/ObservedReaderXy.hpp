/** \file ObservedReaderXy.hpp
 *  \brief Declares specialized input file reader for format XY.
 */
#ifndef OBSERVEDREADERXY_HPP_INCLUDED
#define OBSERVEDREADERXY_HPP_INCLUDED

#include "ObservedFileReader.hpp"

#include <string>

#include "../peak_list/ObservedPeakList.hpp"



namespace mcc
{
  /** \brief Specialized input file reader for format XY.
   * 
   *  Format XY consists of 2Theta peak positions and intensities. Each line
   *  contains 2Theta peak position and optional intensity. Numbers are
   *  separated by whitespace. If no intensity is found, it is assumed to
   *  be 0.0.
   */
  class ObservedReaderXy : public ObservedFileReader
  {
  public:
    /** Initializes base ObservedFileReader class.
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit ObservedReaderXy(const GlobalSettings& settings);

    /** \brief Reads observed data file and applies zero shift to 2Theta
     *         positions. Peaks with negative 2Theta are ignored. Saves
     *         peaks in provided observed peak list.
     *
     *  \param observedList Observed peak list to populate
     *  \return True if no errors were encountered
     */
    bool extractData(ObservedPeakList& observedList) override;

  protected:
    /** \brief Parsed angle 2Theta [°] */
    double twoTheta_;
    /** \brief Parsed intensity (unitless) */
    double intensity_;

    /** \brief Checks whether character is invalid and reports and error in
     *         case it is. Also returns true in this case so line parsing
     *         can be terminated.
     *
     *  \param c Character to be checked
     *  \return True if character is invalid, false otherwise
     */
    bool checkAndReportInvalidCharacter(char c);

    /** \brief Saves the peak data in members twoTheta_ and intensity_ into
     *         the observed peak list.
     *  
     *  \param observedList Observed peak list to populate
     */
    void submitPeak(ObservedPeakList& observedList);
  };
}



#endif // OBSERVEDREADERXY_HPP_INCLUDED
