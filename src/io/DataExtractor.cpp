#include "DataExtractor.hpp"

#include "../io/ObservedReaderXy.hpp"



namespace mcc
{
  DataExtractor::DataExtractor(const GlobalSettings& settings) :
                 Module{settings}
  {
    // Initialize from settings
    dataFileFormat_ = globalSettings_.main_dataFormat;
  }



  bool DataExtractor::extractData(ObservedPeakList& peakList) const
  {
    bool result {false};

    if (dataFileFormat_ == "xy") {
      ObservedReaderXy reader{globalSettings_};
      result = reader.extractData(peakList);
    }
    else {
      std::stringstream message;
      message << "Unknown data file format: '" << dataFileFormat_ << "'";
      reportError(message.str());
    }

    return result;
  }



  std::string DataExtractor::getModuleName() const
  {
    return "DataExtractor";
  }
}