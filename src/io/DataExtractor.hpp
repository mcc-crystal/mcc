/** \file DataExtractor.hpp
 *  \brief Declares module used to select appropriate data reader and populate
 *         observed peak lists.
 */

#ifndef DATAREADER_HPP_INCLUDED
#define DATAREADER_HPP_INCLUDED

#include "../core/Module.hpp"

#include "../peak_list/ObservedPeakList.hpp"



namespace mcc
{
  /** \brief Module used to select appropriate data reader based on settings
   *         and populates observed peak lists.
   */
  class DataExtractor : public Module
  {
  public:
    /** \brief Initializes base class and copies relevant setting from
     *         shared global settings object.
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit DataExtractor(const GlobalSettings& settings);

    /** \brief Instantiates appropriate file reader and reads data file
     *         (path and format provided in settings). Saves observed peaks
     *         to provided peak lists.
     * 
     *  \param peakList Observed peak list to save data to
     *  \return True if no errors were encountered, false otherwise
     */
    bool extractData(ObservedPeakList& peakList) const;

    std::string getModuleName() const override;

    
  private:
    /** \brief Data file format from settings */
    std::string dataFileFormat_;
  };
}



#endif // DATAREADER_HPP_INCLUDED