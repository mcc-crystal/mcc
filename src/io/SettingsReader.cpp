#include "SettingsReader.hpp"

#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <exception>

#include "../core/GlobalSettings.hpp"


namespace mcc
{
  // Static initialization
  // COMMENT_INITIATOR relies on zero character termination
  // String is automatically terminated by '\0'
  const char SettingsReader::COMMENT_INITIATOR[] = "#";
  const char SettingsReader::QUOTE = '"';
  const char SettingsReader::ASSIGNMENT_OPERATOR = '=';
  const char SettingsReader::GROUP_INITIATOR = '[';
  const char SettingsReader::GROUP_TERMINATOR = ']';



  SettingsReader::SettingsReader(const std::string& settingsFile,
                                 GlobalSettings& globalSettings) :
                  FileReaderBase {settingsFile},
                  globalSettings_ {globalSettings}
  { }



  bool SettingsReader::readFile()
  {
    noErrors_ = true;
    lineCount_ = 0;
    std::ifstream settingsFile;

    try {
      settingsFile.open(filePath_);

      if (!settingsFile.is_open()) {
        reportError("Failed to open the settings file!");
        return false;
      }

      // Main reading loop
      while (getline(settingsFile, line_)) {
        lineCount_++;
        parseIndex_ = 0;
        group_.clear();
        setting_.clear();
        value_.clear();

        // Skip initial whitespace and ignore empty lines and lines beginning
        // with a comment initiator
        skipWhitespace();
        if (line_[parseIndex_] == '\0')
          continue;
        if (isAtCommentInitiator())
          continue;

        // Check for group declaration
        if (line_[parseIndex_] == GROUP_INITIATOR) {
          group_ = captureGroup();
          // debugMessage("Found group '" + group_ + "'");
          // Check group validity
          if (globalSettings_.evaluateGroup(group_)) {
            currentGroup_ = group_;
          }
          else {
            std::stringstream message;
            message << "Group [" << group_ << "] is invalid.";
            reportError(message.str());
            break;
          }

          // Check for trailing garbage
          skipWhitespace();
          if (line_[parseIndex_] != '\0' && !isAtCommentInitiator()) {
            reportError("Invalid content after group specification!");
          }
          continue;
        }

        // Get setting name
        setting_ = captureWord();
        // Setting name is checked later

        // Check for assignment operator
        skipWhitespace();
        if (line_[parseIndex_] != ASSIGNMENT_OPERATOR) {
          std::stringstream message;
          message << "No assignment operator found after setting '" <<
                     setting_ << "'!";
          reportError(message.str());
          break;
        }
        parseIndex_++; // Skip assignment operator

        // Get value
        skipWhitespace();
        value_ = captureWord();
        // Value validity is checked later

        // Check for trailing garbage
        skipWhitespace();
        if (line_[parseIndex_] != '\0' && !isAtCommentInitiator()) {
          reportError("Invalid content after setting-value pair.");
        }

        // Evaluate setting and save it
        // debugMessage("Found pair: '" + setting_ + "' = '" + value_ + "'");
        bool result {globalSettings_.evaluateSetting(currentGroup_,
                                                     setting_,
                                                     value_)};
        if (result == false){
          std::stringstream message;
          message << "Setting [" << currentGroup_ << "] '" << setting_ <<
                     "' = '" << value_ << "' ignored.";
          reportError(message.str());
        }
      }
    }
    catch (std::exception& e) {
      noErrors_ = false;
      std::cout << "Error while reading settings file '" << filePath_ <<
                   "': " << e.what() << std::endl;
    }

    if (settingsFile.is_open()) {
      settingsFile.close();
    }

    return noErrors_;
  }



  bool SettingsReader::isAtCommentInitiator() const
  {
    int iSource {0};
    int iCheck {parseIndex_};

    while (COMMENT_INITIATOR[iSource] != '\0') {
      char c {line_[iCheck]};
      if (c == '\0') {
        return false;
      }
      if (c != COMMENT_INITIATOR[iSource]) {
        return false;
      }

      iSource++;
      iCheck++;
    }

    return true;
  }



  bool SettingsReader::isAtQuote() const
  {
    if (line_[parseIndex_] == QUOTE) {
      return true;
    }
    return false;
  }



  std::string SettingsReader::captureWord()
  {
    std::string word;
    bool wordQuoted {false};
    bool foundUnquote {false};

    if (isAtQuote()) {
      wordQuoted = true;
      parseIndex_++;
    }

    while (line_[parseIndex_] != '\0') {
      if (wordQuoted) {
        if (isAtQuote()) {
          foundUnquote = true;
          parseIndex_++;
          break;
        }
      }
      else {
        if (isAtWhitespace() ||
            isAtCommentInitiator() ||
            line_[parseIndex_] == ASSIGNMENT_OPERATOR)
          break;
      }

      // Default action
      word += line_[parseIndex_];
      parseIndex_++;
    }

    // Check for syntax errors
    if (wordQuoted && !foundUnquote) {
      std::stringstream message;
      message << "Quoted word '" << word << "' is not terminated by "
                 "another quote!";
      reportError(message.str());
    }

    return word;
  }



  std::string SettingsReader::captureGroup()
  {
    std::string group;

    if (line_[parseIndex_] != GROUP_INITIATOR) {
      reportError("Parse index must point at a group initiator when "
                  "parsing group name!");
      return "";
    }

    parseIndex_++; // Skip group initiator character
    while (line_[parseIndex_] != '\0') {
      if (isAtCommentInitiator())
        break;
      if (line_[parseIndex_] == GROUP_TERMINATOR)
        break;

      // Default action
      group += line_[parseIndex_];
      parseIndex_++;
    }

    // Check for syntax errors
    if (line_[parseIndex_] != GROUP_TERMINATOR) {
      std::stringstream message;
      message << "Group name '" << group << "' is not terminated by group "
                 "terminator '" << GROUP_TERMINATOR << "'!";
      reportError(message.str());
    }
    else {
      parseIndex_++; // Skip group terminator character
    }

    return group;
  }



  void SettingsReader::debugMessage(const std::string& message) const
  {
    std::cout << "SettingsReader line " << lineCount_ << ": " << message <<
                 std::endl;
  }
}
