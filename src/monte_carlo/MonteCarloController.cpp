#include "MonteCarloController.hpp"

#include <sstream>
#include <cmath>
#include <chrono>

// For compatibility with other compilers
#ifndef M_PI
#define M_PI 3.14159265358979323846d
#endif // M_PI

#include "../temperature/LinearTemperatureController.hpp"
#include "../temperature/AutoLinearTemperatureController.hpp"
#include "../temperature/AutoCosineTemperatureController.hpp"

#include "../step_optimization/LinearStepOptimizer.hpp"
#include "../step_optimization/LogarithmicStepOptimizer.hpp"



namespace mcc
{
  // Static initialization
  const double MonteCarloController::INITIAL_LENGTH_STEP = 0.40;
  const double MonteCarloController::INITIAL_ANGLE_STEP = 0.50;
  const int MonteCarloController::STEP_OPTIMIZATION_INTERVAL = 300;



  MonteCarloController::MonteCarloController(
                                          MonteCarloRootModule& mcRoot,
                                          const GlobalSettings& settings,
                                          const std::string& threadName,
                                          const ObservedPeakList& sourceList) :
                        ThreadedController{settings, threadName, sourceList},
                        root_{mcRoot},
                        isThreadActive_{false},
                        step_{0},
                        simulation_{0},
                        temperature_{0.0},
                        totalLengthSteps_{0},
                        totalAngleSteps_{0},
                        worstEnergy_{0.0},
                        maxLengthStep_{0.0},
                        maxAngleStep_{0.0},
                        oldEnergy_{0.0},
                        newEnergy_{0.0},
                        acceptedAngleSteps_{0},
                        acceptedLengthSteps_{0},
                        oldCell_{nullptr},
                        newCell_{nullptr},
                        temperatureController_{nullptr},
                        stepOptimizer_{nullptr},
                        random_{static_cast<unsigned int>(
                                             std::chrono::system_clock::now()
                                                 .time_since_epoch().count())},
                        step_distribution_{-1.0, 1.0},
                        eval_distribution_{0.0, 1.0},
                        cellStats_{globalSettings_,
                                   *calculatedList_,
                                   *observedList_}
  {
    // Initialize modules
    bool result {initializeModules()};
    // Merge child modules initialization result with threaded controller's
    // inherited modules initialization result
    modulesInitialized_ &= result;

    // Initialize from settings
    cellTrackerEnabled_ = globalSettings_.logging_cellTracker;
    crystalSystem_ = globalSettings_.cell_crystalSystem;
    nLengthStepCycles_ = globalSettings_.monteCarlo_nLengthStepCycles;
    nAngleStepCycles_ = globalSettings_.monteCarlo_nAngleStepCycles;
    nMaxSteps_ = globalSettings_.monteCarlo_nMaxSteps;
    idealAcceptedRatio_ = globalSettings_.monteCarlo_acceptedStepRatio;
    minA_ = globalSettings_.cell_minA;
    minB_ = globalSettings_.cell_minB;
    minC_ = globalSettings_.cell_minC;
    minAlpha_ = globalSettings_.cell_minAlpha;
    minBeta_ = globalSettings_.cell_minBeta;
    minGamma_ = globalSettings_.cell_minGamma;
    maxA_ = globalSettings_.cell_maxA;
    maxB_ = globalSettings_.cell_maxB;
    maxC_ = globalSettings_.cell_maxC;
    maxAlpha_ = globalSettings_.cell_maxAlpha;
    maxBeta_ = globalSettings_.cell_maxBeta;
    maxGamma_ = globalSettings_.cell_maxGamma;

    // Construct correct UnitCell object according to settings
    switch (crystalSystem_) {
      case CrystalSystem::CUBIC:
        oldCell_ = new CubicCell;
        newCell_ = new CubicCell;
        break;
      case CrystalSystem::HEXAGONAL:
        oldCell_ = new HexagonalCell;
        newCell_ = new HexagonalCell;
        break;
      case CrystalSystem::TETRAGONAL:
        oldCell_ = new TetragonalCell;
        newCell_ = new TetragonalCell;
        break;
      case CrystalSystem::ORTHORHOMBIC:
        oldCell_ = new OrthorhombicCell;
        newCell_ = new OrthorhombicCell;
        break;
      case CrystalSystem::MONOCLINIC:
        oldCell_ = new MonoclinicCell;
        newCell_ = new MonoclinicCell;
        break;
      case CrystalSystem::TRICLINIC:
        oldCell_ = new TriclinicCell;
        newCell_ = new TriclinicCell;
        break;
    }
  }



  MonteCarloController::~MonteCarloController()
  {
    delete temperatureController_;
    delete stepOptimizer_;
    delete oldCell_;
    delete newCell_;
  }



  void MonteCarloController::beginWork()
  {
    isThreadActive_ = true;
    beginCalculations();
    isThreadActive_ = false;
  }



  std::thread MonteCarloController::spawnThread()
  {
    return std::thread(&MonteCarloController::beginWork, this);
  }



  bool MonteCarloController::isThreadActive() const
  {
    return isThreadActive_;
  }



  std::string MonteCarloController::getModuleName() const
  {
    return "MonteCarloController";
  }



  int MonteCarloController::getStepNumber() const
  {
    return step_;
  }



  const McCellTracker& MonteCarloController::getMcCellTrackerReference() const
  {
    return cellTracker_;
  }



  McCellTracker& MonteCarloController::getMcCellTrackerReference()
  {
    return cellTracker_;
  }



  void MonteCarloController::signalInterrupt()
  {
    noInterruptReceived_ = false;
    // Setting max steps below current step number will end the simulation
    // quickly without waiting for interrupt flag check
    nMaxSteps_ = 0;
  }



  void MonteCarloController::beginCalculations()
  {
    if (modulesInitialized_ == false) {
      std::stringstream message;
      message << "Modules not initialized, thread " << threadName_ <<
                 " terminating.";
      reportError(message.str());
      return;
    }

    // Check if observed peak list is populated
    if (observedList_->getPeakCount() == 0) {
      std::stringstream message;
      message << "Observed peak list is empty, thread " << threadName_ <<
                 " terminating.";
      reportError(message.str());
      return;
    }

    while (true) {
      // Check for interrupt signal
      if (noInterruptReceived_ == false) {
        reportMessage("Thread '" + threadName_ +
                      "' received interrupt signal. Exiting.");
        break;
      }
      // Initialize stuff
      maxLengthStep_ = INITIAL_LENGTH_STEP;
      maxAngleStep_ = INITIAL_ANGLE_STEP;
      step_ = 0;
      simulation_++;
      worstEnergy_ = root_.getWorstEnergy();
      // Set first temperature
      temperatureController_->Reset();
      temperature_ = temperatureController_->getNextTemperature();

      // Get next initial unit cell from root
      bool result {root_.generateNextUnitCell(*oldCell_)};
      if (result == false) {
        reportMessage("Thread '" + threadName_ + "' out of work. Finishing "
                      "up.");
        break;
      }
      newCell_->copyData(*oldCell_);

      // Calculate first energy
      peakGenerator_->setUnitCell(oldCell_);
      peakGenerator_->populatePeakList(*calculatedList_);
      calculatedList_->sortPeaks();
      calculatedList_->removeDuplicates();
      oldEnergy_ = energyCalculator_->calculateEnergy();

      // Cell tracking if enabled
      if (cellTrackerEnabled_ > 0) {
        McTrackedData mcData = generateMcTrackedData();
        cellTracker_.submitCell(*oldCell_, oldEnergy_, mcData);
      }

      // Begin simulation
      peakGenerator_->setUnitCell(newCell_);
      performMonteCarloSimulation();
    }

    peakGenerator_->setUnitCell(nullptr);
  }



  void MonteCarloController::calculateCellEnergy()
  {
    peakGenerator_->populatePeakList(*calculatedList_);
    calculatedList_->sortPeaks();
    calculatedList_->removeDuplicates();
    newEnergy_ = energyCalculator_->calculateEnergy();
  }



  void MonteCarloController::performMonteCarloSimulation()
  {
    while (true) {
      // Change lengths
      for (int i = 0; i < nLengthStepCycles_; ++i) {
        switch (crystalSystem_) {
          case CrystalSystem::TRICLINIC:
          case CrystalSystem::MONOCLINIC:
          case CrystalSystem::ORTHORHOMBIC:
            performLengthStep(newCell_->b, minB_, maxB_);
            // Fall-through
          case CrystalSystem::TETRAGONAL:
          case CrystalSystem::HEXAGONAL:
            performLengthStep(newCell_->c, minC_, maxC_);
            // Fall-through
          case CrystalSystem::CUBIC:
            performLengthStep(newCell_->a, minA_, maxA_);
            break;
        }
      }

      // Change angles
      for (int i = 0; i < nAngleStepCycles_; ++i) {
        switch (crystalSystem_) {
          case CrystalSystem::TRICLINIC:
            performAngleStep(newCell_->alpha, minAlpha_, maxAlpha_);
            performAngleStep(newCell_->gamma, minGamma_, maxGamma_);
            // Fall-through
          case CrystalSystem::MONOCLINIC:
            performAngleStep(newCell_->beta, minBeta_, maxBeta_);
            // Fall-through
          case CrystalSystem::ORTHORHOMBIC:
          case CrystalSystem::TETRAGONAL:
          case CrystalSystem::HEXAGONAL:
          case CrystalSystem::CUBIC:
            break;
        }
      }

      // Check if it's time for step optimization
      if (totalLengthSteps_ >= STEP_OPTIMIZATION_INTERVAL)
        performLengthStepOptimization();
      if (totalAngleSteps_ >= STEP_OPTIMIZATION_INTERVAL)
        performAngleStepOptimization();

      // End simulation if max steps reached
      if (step_ > nMaxSteps_) {
        break;
      }
      // DEBUG
      /*
      else {
        reportMessage(std::to_string(step_) + "  " +
                      std::to_string(maxLengthStep_) + "    " +
                      std::to_string(maxAngleStep_) + "    " +
                      std::to_string(oldCell_->beta));
      }
      */
    }
  }



  double MonteCarloController::generateStepRandomNumber()
  {
    return step_distribution_(random_);
  }



  double MonteCarloController::generateEvalRandomNumber()
  {
    return eval_distribution_(random_);
  }



  bool MonteCarloController::performLengthStep(double& outParameter,
                                               double paramMin,
                                               double paramMax)
  {
    double parameterCopy {outParameter};
    double random {generateStepRandomNumber()};
    outParameter += random * maxLengthStep_;

    // Keep parameter in given boundaries
    if (outParameter < paramMin)
      outParameter = paramMin;
    else if (outParameter > paramMax)
      outParameter = paramMax;

    calculateCellEnergy();
    bool stepAccepted = evaluateStep();

    totalLengthSteps_++;
    if (stepAccepted) {
      acceptedLengthSteps_++;
    }
    else {
      outParameter = parameterCopy;
    }

    return stepAccepted;
  }



  bool MonteCarloController::performAngleStep(double& outParameter,
                                              double paramMin,
                                              double paramMax)
  {
    double parameterCopy {outParameter};
    double random {generateStepRandomNumber()};
    outParameter += random * maxAngleStep_;

    // Keep parameter in given boundaries
    if (outParameter < paramMin)
      outParameter = paramMin;
    else if (outParameter > paramMax)
      outParameter = paramMax;

    calculateCellEnergy();
    bool stepAccepted = evaluateStep();

    totalAngleSteps_++;
    if (stepAccepted) {
      acceptedAngleSteps_++;
    }
    else {
      outParameter = parameterCopy;
    }

    return stepAccepted;
  }



  bool MonteCarloController::evaluateStep()
  {
    step_++;
    double deltaE {newEnergy_ - oldEnergy_};

    // If energy lowered, accept in any case ...
    if (deltaE < 0.0) {
      acceptStep();
      return true;
    }

    // ... else weight with a random number
    double factor {std::exp(-deltaE / temperature_)};
    double random {generateEvalRandomNumber()};

    // Update temperature (after the old value is no longer needed and before
    // the function returns)
    temperature_ = temperatureController_->getNextTemperature();

    if (random < factor) {
      acceptStep();
      return true;
    }
    else {
      rejectStep();
      return false;
    }
  }



  void MonteCarloController::acceptStep()
  {
    oldEnergy_ = newEnergy_;
    oldCell_->copyData(*newCell_);

    if (newEnergy_ < worstEnergy_) {
      cellStats_.calculateStats();
      Solution s{*oldCell_, oldEnergy_, &cellStats_,
                 observedList_->getPeakCount()};

      root_.submitSolution(s);
      worstEnergy_ = root_.getWorstEnergy();
    }

    if (cellTrackerEnabled_ > 0) {
      McTrackedData mcData {generateMcTrackedData()};
      cellTracker_.submitCell(*oldCell_, oldEnergy_, mcData);
    }
  }



  void MonteCarloController::rejectStep()
  {
    // Do nothing?
  }



  void MonteCarloController::performLengthStepOptimization()
  {
    stepOptimizer_->performLengthStepOptimization(acceptedLengthSteps_,
                                                  totalLengthSteps_,
                                                  maxLengthStep_);
  }



  void MonteCarloController::performAngleStepOptimization()
  {
    stepOptimizer_->performAngleStepOptimization(acceptedAngleSteps_,
                                                 totalAngleSteps_,
                                                 maxAngleStep_);
  }



  McTrackedData MonteCarloController::generateMcTrackedData() const
  {
    McTrackedData data;
    data.step = step_;
    data.temperature = temperature_;
    data.maxLengthStep = maxLengthStep_;
    data.maxAngleStep = maxAngleStep_;

    return data;
  }



  bool MonteCarloController::initializeModules()
  {
    bool initSuccessful = true;

    // Temperature module
    std::string temperatureModule {
                                globalSettings_.modules_temperatureController};

    if (temperatureModule == "Linear")
      temperatureController_ = new LinearTemperatureController{
                                                     globalSettings_, *this};
    else if (temperatureModule == "AutoLinear")
      temperatureController_ = new AutoLinearTemperatureController{
                                                     globalSettings_, *this};
    else if (temperatureModule == "AutoCosine")
      temperatureController_ = new AutoCosineTemperatureController{
                                                     globalSettings_, *this};
    else {
      std::stringstream message;
      message << "Temperature control module '" << temperatureModule << "' "
                 "not found.";
      reportError(message.str());
      initSuccessful = false;
    }

    // Step optimizer module
    std::string stepOptModule {globalSettings_.modules_stepOptimizer};

    if (stepOptModule == "Linear")
      stepOptimizer_ = new LinearStepOptimizer{globalSettings_, *this};
    else if (stepOptModule == "Logarithmic")
      stepOptimizer_ = new LogarithmicStepOptimizer{globalSettings_, *this};
    else {
      std::stringstream message;
      message << "Step optimization module '" << stepOptModule << "' "
                 "not found.";
      reportError(message.str());
      initSuccessful = false;
    }

    return initSuccessful;
  }
}
