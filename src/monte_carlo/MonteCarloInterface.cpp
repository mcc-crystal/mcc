#include "MonteCarloInterface.hpp"

#include <iostream>
#include <sstream>
#include <iomanip>



namespace mcc
{
  MonteCarloInterface::MonteCarloInterface(MonteCarloRootModule& mcRoot) :
                       mcRoot_{mcRoot}
  { }



  void MonteCarloInterface::initializeInterface()
  {
    std::cout << "Monte Carlo simulation progress:" << std::endl;
  }



  void MonteCarloInterface::updateInterface()
  {
    double simulationCount;
    double nMaxSimulations;
    mcRoot_.getSimulationProgress(simulationCount, nMaxSimulations);

    if (mcRoot_.getSolutionCountSafe() > 0) {
      Solution best {mcRoot_.getBestSolution()};

      std::stringstream update;
      update << "[" << simulationCount << "/" << nMaxSimulations << "]" <<
                "    Emin = " << std::fixed << std::setprecision(3) <<
                best.energy << "    I: " << std::setw(3) << best.nIndexed <<
                "   UI: " << std::setw(3) << best.nUnindexed <<
                "    C/O: " << std::setw(3) << best.nCalculated << " /" <<
                std::setw(3) << best.nObserved << "    V = " <<
                std::setprecision(0) << std::setw(5) << best.volume <<
                std::endl;
      std::cout << update.str();
    }
    else {
      std::cout << "Waiting for solutions ..." << std::endl;
    }
  }
}
