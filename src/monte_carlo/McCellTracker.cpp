#include "McCellTracker.hpp"

#include <iostream>
#include <iomanip>
#include <stdexcept>



namespace mcc
{
  void McCellTracker::submitCell(const UnitCell& cell,
                                 double energy,
                                 const McTrackedData& mcData)
  {
    // Cell
    Solution s {cell, energy};
    solutions_.push_back(s);
    // Monte Carlo
    mcData_.push_back(mcData);
  }



  const std::vector<McTrackedData>& McCellTracker::getMcStorageReference()
                                    const
  {
    return mcData_;
  }



  void McCellTracker::mergeData(McCellTracker& source)
  {
    copySolutions(source);
    copyMcData(source);
    source.clearData();
  }



  void McCellTracker::copyMcData(const McCellTracker& source)
  {
    const std::vector<McTrackedData>& sourceMcData =
                                             source.getMcStorageReference();
    // Copy solutions
    mcData_.reserve(mcData_.size() + sourceMcData.size());
    mcData_.insert(mcData_.end(),
                   sourceMcData.begin(),
                   sourceMcData.end());
  }



  void McCellTracker::clearData()
  {
    solutions_.clear();
    mcData_.clear();
  }



  void McCellTracker::writeSolutionLine(int i, std::ostream& stream) const
  {
    const Solution& s = solutions_[i];
    const McTrackedData& mc = mcData_[i];

    const double* parameters[] = {
                                  // Cell parameters
                                  &s.a, &s.b, &s.c, &s.alpha, &s.beta,
                                  &s.gamma, &s.energy,
                                  // Monte Carlo parameters
                                  &mc.step, &mc.temperature, &mc.maxLengthStep,
                                  &mc.maxAngleStep,
                                  // Array terminator
                                  nullptr};

    for (int i = 0; parameters[i] != nullptr; ++i) {
      stream << std::fixed;
      stream << std::setw(PARAM_WIDTH) << std::setprecision(PARAM_PRECISION) <<
                *parameters[i] << " ";
    }
    stream << std::endl;
  }



  void McCellTracker::writeHeaderLine(std::ostream& stream) const
  {
    const std::string parameters[] = {"a [A]", "b [A]", "c [A]", "alpha",
                                      "beta", "gamma", "E", "Step", "T",
                                      "Len step", "Ang step", ""};

    for (int i = 0; parameters[i].empty() == false; ++i) {
      stream << std::fixed;
      stream << std::setw(PARAM_WIDTH) << parameters[i] << " ";
    }
    stream << std::endl;
  }



  void McCellTracker::submitCell(const UnitCell& cell, double energy)
  {
    throw std::logic_error("This method is hidden in Monte Carlo version "
                           "of CellTracker!");
  }



  void McCellTracker::mergeData(CellTracker& source)
  {
    throw std::logic_error("This method is hidden in Monte Carlo version "
                           "of CellTracker!");
  }
}
