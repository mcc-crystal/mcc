#include "MonteCarloRootModule.hpp"

#include <chrono>
#include <vector>
#include <iostream>

#include "MonteCarloInterface.hpp"
#include "../core/constants.hpp"
#include "../io/DataExtractor.hpp"
#include "../peak_list/DynamicObservedList.hpp"



namespace mcc
{
  MonteCarloRootModule::MonteCarloRootModule(const GlobalSettings& settings) :
                        RootModule{settings},
                        simulation_{0},
                        aArea_{0.0},
                        bArea_{0.0},
                        cArea_{0.0},
                        alphaArea_{0.0},
                        betaArea_{0.0},
                        gammaArea_{0.0},
                        random_{static_cast<unsigned int>(
                                            std::chrono::system_clock::now()
                                            .time_since_epoch().count())},
                        distribution_{0.0, 1.0}
  {
    // Initialize from settings
    minA_ = globalSettings_.cell_minA;
    minB_ = globalSettings_.cell_minB;
    minC_ = globalSettings_.cell_minC;
    minAlpha_ = globalSettings_.cell_minAlpha;
    minBeta_ = globalSettings_.cell_minBeta;
    minGamma_ = globalSettings_.cell_minGamma;
    maxA_ = globalSettings_.cell_maxA;
    maxB_ = globalSettings_.cell_maxB;
    maxC_ = globalSettings_.cell_maxC;
    maxAlpha_ = globalSettings_.cell_maxAlpha;
    maxBeta_ = globalSettings_.cell_maxBeta;
    maxGamma_ = globalSettings_.cell_maxGamma;
    cellTrackerEnabled_ = globalSettings_.logging_cellTracker;
    nMaxSimulations_ = globalSettings_.monteCarlo_nMaxSimulations;
    crystalSystem_ = globalSettings_.cell_crystalSystem;

    // Calculate parameter areas
    aArea_ = maxA_ - minA_;
    bArea_ = maxB_ - minB_;
    cArea_ = maxC_ - minC_;
    alphaArea_ = maxAlpha_ - minAlpha_;
    betaArea_ = maxBeta_ - minBeta_;
    gammaArea_ = maxGamma_ - minGamma_;
  }



  MonteCarloRootModule::~MonteCarloRootModule()
  {
    const long unsigned int N {mcControllers_.size()};
    for (long unsigned int i = 0; i < N; ++i) {
      delete mcControllers_[i];
    }
  }



  bool MonteCarloRootModule::generateNextUnitCell(UnitCell& outUnitCell)
  {
    std::lock_guard<std::mutex> lock{cellGenerationMutex_};

    if (simulation_ >= nMaxSimulations_)
      return false;

    switch (crystalSystem_) {
      case CrystalSystem::TRICLINIC:
        outUnitCell.alpha = minAlpha_ + generateRandomNumber() * alphaArea_;
        outUnitCell.gamma = minGamma_ + generateRandomNumber() * gammaArea_;
        // Fall-through
      case CrystalSystem::MONOCLINIC:
        outUnitCell.beta = minBeta_ + generateRandomNumber() * betaArea_;
        // Fall-through
      case CrystalSystem::ORTHORHOMBIC:
        outUnitCell.b = minB_ + generateRandomNumber() * bArea_;
        // Fall-through
      case CrystalSystem::TETRAGONAL:
      case CrystalSystem::HEXAGONAL:
        outUnitCell.c = minC_ + generateRandomNumber() * cArea_;
        // Fall-through
      case CrystalSystem::CUBIC:
        outUnitCell.a = minA_ + generateRandomNumber() * aArea_;
        break;
    }

    simulation_++;
    return true;
  }



  void MonteCarloRootModule::spawnThreads()
  {
    simulation_ = 0;
    MonteCarloInterface interface(*this);

    // Extract observed peaks
    DynamicObservedList observedList{globalSettings_};
    {
      bool result;
      DataExtractor extractor{globalSettings_};
      result = extractor.extractData(observedList);

      if (result == false) {
        std::cout << "Extracting observed peaks failed. Calculations can not "
                     "begin." << std::endl;
        return;        
      }
    }

    // Start threads
    std::vector<std::thread> threads;
    for (int i = 0; i < nThreads_; ++i) {
      std::string threadName = "thread" + std::to_string(i);
      MonteCarloController* controller =
                new MonteCarloController(*this,
                                         globalSettings_,
                                         threadName,
                                         observedList);
      mcControllers_.push_back(controller);
      threads.push_back(controller->spawnThread());
    }

    // Display progress
    while (true) {
      std::this_thread::sleep_for(std::chrono::milliseconds(
                                                   INTERFACE_UPDATE_INTERVAL));

      bool threadsInactive {true};
      // Continue updating if at least one thread is still working
      for (const MonteCarloController* mcController : mcControllers_) {
        if (mcController->isThreadActive()) {
          threadsInactive = false;
          break;
        }
      }

      if (threadsInactive)
        break;
      else {
        interface.updateInterface();
      }
    }

    // Join all threads
    for (std::thread& thread : threads) {
      thread.join();
    }

    // Write solutions to file
    solutions_->writeToFile(Constants::solutionFilePath);

    // Cell tracker merge, sort and write to file
    if (cellTrackerEnabled_ > 0) {
      McCellTracker ct;

      std::cout << "Merging thread cell tracker data ..." << std::endl;
      for (MonteCarloController* mcController : mcControllers_) {
        McCellTracker& ctThread {mcController->getMcCellTrackerReference()};
        ct.mergeData(ctThread);
      }

      std::cout << "Writing cell tracker data to file ..." << std::endl;
      ct.writeToFile(Constants::trackedDataFilePath);
    }
  }



  void MonteCarloRootModule::getSimulationProgress(double& outSimulationCount,
                                                   double& outMaxSimulations)
                                                   const
  {
    outSimulationCount = simulation_;
    outMaxSimulations = nMaxSimulations_;
  }



  void MonteCarloRootModule::signalInterrupt()
  {
    for (MonteCarloController* mcController : mcControllers_) {
      mcController->signalInterrupt();
    }
  }



  double MonteCarloRootModule::generateRandomNumber()
  {
    return distribution_(random_);
  }
}
