/** \file MonteCarloController.hpp
 *  \brief Declares controller for Monte Carlo calculations meant to be run as
 *         a single thread.
 */

#ifndef MONTECARLOCONTROLLER_HPP_INCLUDED
#define MONTECARLOCONTROLLER_HPP_INCLUDED

#include "../core/ThreadedController.hpp"

#include <random>

#include "MonteCarloRootModule.hpp"
#include "McCellTracker.hpp"
#include "../temperature/TemperatureController.hpp"
#include "../core/UnitCell.hpp"
#include "../core/CellStats.hpp"
#include "../step_optimization/StepOptimizer.hpp"



namespace mcc
{
  // Forward declarations
  class MonteCarloRootModule;
  class TemperatureController;
  class StepOptimizer;



  /** \brief Controller for Monte Carlo cell search meant to be run as a single
   *         thread. Performs one Monte Carlo simulation at a time.
   */
  class MonteCarloController : public ThreadedController
  {
  public:
    // Static constants
    /** \brief Initial maximum step for length parameters [Å] */
    static const double INITIAL_LENGTH_STEP;
    /** \brief Initial maximum step for angle parameters [°] */
    static const double INITIAL_ANGLE_STEP;
    /** \brief Minimum number of steps after which step optimization
     *         should be performed. */
    static const int STEP_OPTIMIZATION_INTERVAL;

    /** Initializes base class and this object's fields. Initializes child
     *  modules with appropriate module implementations and initializes
     *  the object with appropriate unit cell type.
     * 
     *  \param mcRoot Reference to root module
     *  \param settings Reference to shared global settings object
     *  \param threadName Name to identify the thread by
     *  \param sourceList Observed peak list to copy peaks from
     */
    MonteCarloController(MonteCarloRootModule& mcRoot,
                         const GlobalSettings& settings,
                         const std::string& threadName,
                         const ObservedPeakList& sourceList);
    
    virtual ~MonteCarloController();

    // Delete default members to avoid trouble with pointers
    MonteCarloController(const MonteCarloController&) = delete;
    MonteCarloController& operator= (const MonteCarloController&) = delete;

    void beginWork() override;

    std::thread spawnThread() override;

    bool isThreadActive() const override;

    std::string getModuleName() const override;

    /** \brief Returns number of the current simulation step.
     *
     * \return Current simulation step
     */
    int getStepNumber() const;

    /** \brief Returns const reference to this thread's McCellTracker object.
     *
     *  \return Const reference to this object's McCellTracker object
     */
    const McCellTracker& getMcCellTrackerReference() const;

    /** \brief Returns read-write reference to this thread's McCellTracker
     *         object.
     *
     *  \return Read-write reference to this object's McCellTracker object
     */
    McCellTracker& getMcCellTrackerReference();

    void signalInterrupt() override;


  protected:
    /** \brief Root module to submit solutions to */
    MonteCarloRootModule& root_;
    /** \brief Energy of the worst solution in storage of root object.
     *         Saved so there is no need to check on a shared object every
     *         time.
     */
    double worstEnergy_;
    /** \brief Max simulation step for lengths [Å] */
    double maxLengthStep_;
    /** \brief Max simulation step for angles [°] */
    double maxAngleStep_;
    /** \brief Energy from previous step */
    double oldEnergy_;
    /** \brief Energy from current step */
    double newEnergy_;
    /** \brief Cell tracking enabled if > 0 */
    int cellTrackerEnabled_;
    /** \brief Whether thread is doing calculations */
    bool isThreadActive_;
    /** \brief Number of current simulation step */
    int step_;
    /** \brief Number of current simulation */
    int simulation_;
    /** \brief Crystal system from settings */
    CrystalSystem crystalSystem_;
    /** \brief Max number of steps from settings */
    int nMaxSteps_;
    /** \brief Number of length step cycles from settings */
    int nLengthStepCycles_;
    /** \brief Number of angle step cycles from settings */
    int nAngleStepCycles_;
    /** \brief Desired accepted versus total step ratio from settings */
    double idealAcceptedRatio_;
    /** \brief Current temperature */
    double temperature_;
    /** \brief Accepted steps counter for length step optimization */
    int acceptedLengthSteps_;
    /** \brief Total steps counter for length step optimization */
    int totalLengthSteps_;
    /** \brief Accepted steps counter for angle step optimization */
    int acceptedAngleSteps_;
    /** \brief Total steps counter for angle step optimization */
    int totalAngleSteps_;
    /** \brief Minimum parameter a from settings [Å] */
    double minA_;
    /** \brief Minimum parameter b from settings [Å] */
    double minB_;
    /** \brief Minimum parameter c from settings [Å] */
    double minC_;
    /** \brief Minimum parameter alpha from settings [°] */
    double minAlpha_;
    /** \brief Minimum parameter beta from settings [°] */
    double minBeta_;
    /** \brief Minimum parameter gamma from settings [°] */
    double minGamma_;
    /** \brief Maximum parameter a from settings [Å] */
    double maxA_;
    /** \brief Maximum parameter b from settings [Å] */
    double maxB_;
    /** \brief Maximum parameter c from settings [Å] */
    double maxC_;
    /** \brief Maximum parameter alpha from settings [°] */
    double maxAlpha_;
    /** \brief Maximum parameter beta from settings [°] */
    double maxBeta_;
    /** \brief Maximum parameter gamma from settings [°] */
    double maxGamma_;

    /** \brief Unchanged unit cell in case step is rejected */
    UnitCell* oldCell_;
    /** \brief Unit cell changed by Monte Carlo steps */
    UnitCell* newCell_;
    /** \brief Temperature control module */
    TemperatureController* temperatureController_;
    /** \brief Step optimizer module */
    StepOptimizer* stepOptimizer_;
    /** \brief Performs cell tracking if enabled in settings */
    McCellTracker cellTracker_;
    /** \brief Random number generator */
    std::mt19937 random_;
    /** \brief Distribution in range from -1 to 1*/
    std::uniform_real_distribution<double> step_distribution_;
    /** \brief Distribution in range from 0 to 1 */
    std::uniform_real_distribution<double> eval_distribution_;
    /** \brief Object for calculation of number of indexed and unindexed peaks
               used in solution submition. */
    CellStats cellStats_;

    /** \brief A protected wrapper for functionality that should be offered by
     *         beginWork(). It allows beginWork() to easily control status of
     *         isThreadActive_.
     */
    void beginCalculations();

    /** \brief Calculates new peaks and cell energy. Submits a solution if
     *         energy is low enough. Call when new cell is generated.
     */
    void calculateCellEnergy();

    /** \brief Handles Monte Carlo simulation from the point where everything
     *         is already initialized and starting energy is calculated.
     */
    void performMonteCarloSimulation();

    /** \brief Generates a random number between -1.0 and 1.0 using member
     *         generator and distribution.
     *
     * \return Random number between -1.0 and 1.0
     */
    double generateStepRandomNumber();

    /** \brief Generates a random number between 0.0 and 1.0 using member
     *         generator and distribution.
     *
     * \return Random number between 0.0 and 1.0
     */
    double generateEvalRandomNumber();

    /** \brief Performs a Monte Carlo step on a length cell parameter. Step is
     *         evaluated and accepted or rejected.
     *
     *  Only newCell_ members should be modified with this method. If step is
     *  rejected, modified parameter is reverted to old value.
     *
     *  Parameter can not step outside provided bounds.
     *
     *  \param outParameter Reference to the cell parameter to modify
     *  \param paramMin Minimum value of parameter
     *  \param paramMax Maximum value of parameter
     *  \return True if step was accepted, false otherwise
     */
    bool performLengthStep(double& outParameter,
                           double paramMin,
                           double paramMax);

    /** \brief Performs a Monte Carlo step on an angle cell parameter. Step is
     *         evaluated and accepted or rejected.
     *
     *  Only newCell_ members should be modified with this method. If step is
     *  rejected, modified parameter is reverted to old value.
     *
     *  Parameter can not step outside provided bounds.
     *
     * \param outParameter Reference to the cell parameter to modify
     * \param paramMin Minimum value of parameter
     * \param paramMax Maximum value of parameter
     * \return True if step was accepted, false otherwise
     */
    bool performAngleStep(double& outParameter,
                          double paramMin,
                          double paramMax);

    /** \brief Evaluates step according to Metropolis method and accepts or
     *         rejects it. Also performs step-related actions common to
     *         accepted and rejected steps (like step counting and updating
     *         temperature).
     *
     * \return True if step was accepted, false otherwise
     */
    bool evaluateStep();

    /** \brief Copies new temperature and cell to old values, gets new
     *         temperature and submits new cell if its energy is low enough.
     */
    void acceptStep();

    /** \brief Does nothing for now.
     */
    void rejectStep();

    /** \brief Performs max length step optimization so that accepted length
     *         step ratio would be closer to ideal ratio set in settings.
     */
    void performLengthStepOptimization();

    /** \brief Performs max angle step optimization so that accepted angle
     *         step ratio would be closer to ideal ratio set in settings.
     */
    void performAngleStepOptimization();

    /** \brief Generates new Monte Carlo tracked data objects and assigns
     *         all required properties to it.
     *
     *  \return Tracked data object with assigned properties
     */
    McTrackedData generateMcTrackedData() const;


  private:
    /** \brief Initializes contained modules based on global settings.
     *
     *  \return True if initialization was successful, false otherwise
     */
    bool initializeModules();
  };
}



#endif // MONTECARLOCONTROLLER_HPP_INCLUDED
