/** \file MonteCarloRootModule.hpp
 *  \brief Declares root module for multi-threaded Monte Carlo cell search.
 */

#ifndef MONTECARLOROOTMODULE_HPP_INCLUDED
#define MONTECARLOROOTMODULE_HPP_INCLUDED

#include "../core/RootModule.hpp"

#include <random>
#include <mutex>

#include "MonteCarloController.hpp"
#include "../core/UnitCell.hpp"
#include "../core/CrystalSystem.hpp"



namespace mcc
{
  // Forward declaration
  class MonteCarloController;



  /** \brief Root module for multi-threaded Monte Carlo cell search.
   */
  class MonteCarloRootModule : public RootModule
  {
  public:
    /** \brief Initializes base class and object's own members. Copies
     *         relevant settings form shared settings object and calculates
     *         parameter areas (used to generate random starting unit cells).
     * 
     *  \param settings Reference to shared global settings object
     */
    explicit MonteCarloRootModule(const GlobalSettings& settings);

    virtual ~MonteCarloRootModule();

    /** \brief Randomly generates a unit cell to be used as starting
     *         configuration for next Monte Carlo simulation.
     *
     *  \param outUnitCell Reference to UnitCell object that that will
     *                     contain the result of this method
     *  \return True if new unit cell is generated, false if thread should
     *          stop performing simulations
     */
    bool generateNextUnitCell(UnitCell& outUnitCell);

    void spawnThreads() override;

    /** \brief Returns simulation progress data through provided references.
     *         Meant to be used by interfaces displaying simulation progress.
     *
     *  \param outSimulationCount Number of performed (or in progress)
     *         simulations
     *  \param outMaxSimulations Maximum number of simulations to be
     *         performed
     */
    void getSimulationProgress(double& outSimulationCount,
                               double& outMaxSimulations) const;

    void signalInterrupt() override;


  protected:
    /** \brief Collection of all Monte Carlo controllers created by the
     *         root module
     */
    std::vector<MonteCarloController*> mcControllers_;
    /** \brief Minimum cell parameter a from settings [Å] */
    double minA_;
    /** \brief Minimum cell parameter b from settings [Å] */
    double minB_;
    /** \brief Minimum cell parameter c from settings [Å] */
    double minC_;
    /** \brief Minimum cell parameter alpha from settings [°] */
    double minAlpha_;
    /** \brief Minimum cell parameter beta from settings [°] */
    double minBeta_;
    /** \brief Minimum cell parameter gamma from settings [°] */
    double minGamma_;
    /** \brief Maximum cell parameter a from settings [Å] */
    double maxA_;
    /** \brief Maximum cell parameter b from settings [Å] */
    double maxB_;
    /** \brief Maximum cell parameter c from settings [Å] */
    double maxC_;
    /** \brief Maximum cell parameter alpha from settings [°] */
    double maxAlpha_;
    /** \brief Maximum cell parameter beta from settings [°] */
    double maxBeta_;
    /** \brief Maximum cell parameter gamma from settings [°] */
    double maxGamma_;
    /** \brief Cell tracking enabled if > 0 (copied from settings) */
    int cellTrackerEnabled_;
    /** \brief Maximum number of simulation from settings */
    int nMaxSimulations_;
    /** \brief Crystal system from settings */
    CrystalSystem crystalSystem_;

    /** \brief Number of simulations initiated (number of starting unit cells 
     *         generated).
     */
    int simulation_;
    /** \brief Difference between max and min parameter a [Å] */
    double aArea_;
    /** \brief Difference between max and min parameter b [Å] */
    double bArea_;
    /** \brief Difference between max and min parameter c [Å] */
    double cArea_;
    /** \brief Difference between max and min parameter alpha [°] */
    double alphaArea_;
    /** \brief Difference between max and min parameter beta [°] */
    double betaArea_;
    /** \brief Difference between max and min parameter gamma [°] */
    double gammaArea_;

    /** \brief Mutex for locking initial unit cell generation (controls
     *         access to random generator).
     */
    std::mutex cellGenerationMutex_;

    /** \brief Random generator used for initial unit cell generation */
    std::mt19937 random_;
    /** \brief Distribution in range from 0.0 to 1.0 */
    std::uniform_real_distribution<double> distribution_;

    /** \brief Generates a random number in range from 0.0 to 1.0 using member
     *         generator and distribution.
     *
     *  \return Random number in range from 0.0 to 1.0
     */
    double generateRandomNumber();
  };
}



#endif // MONTECARLOROOTMODULE_HPP_INCLUDED
