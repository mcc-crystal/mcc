/** \file McTrackedData.hpp
 *  \brief Declares container for Monte Carlo simulation-specific tracked data.
 */

#ifndef MCTRACKEDDATA_HPP_INCLUDED
#define MCTRACKEDDATA_HPP_INCLUDED



namespace mcc
{
  /** \brief Container for Monte Carlo simulation-specific data. Used when
   *         cell tracking is enabled.
   */
  class McTrackedData
  {
  public:
    /** \brief Current simulation step number */
    double step;
    /** \brief Current simulation temperature */
    double temperature;
    /** \brief Current maximum length step */
    double maxLengthStep;
    /** \brief Current maximum angle step */
    double maxAngleStep;
  };
}



#endif // MCTRACKEDDATA_HPP_INCLUDED
