/** \file McCellTracker.hpp
 *  \brief Declares a cell tracker specialized for tracking Monte Carlo
 *         simulations.
 */

#ifndef MCCELLTRACKER_HPP_INCLUDED
#define MCCELLTRACKER_HPP_INCLUDED

#include "../grid_search/CellTracker.hpp"

#include "McTrackedData.hpp"



namespace mcc
{
  /** \brief Cell tracker modified to contain Monte Carlo simulation data
   *         in addition to cell data.
   */
  class McCellTracker : public CellTracker
  {
  public:
    /** \brief Stores unit cell, its energy and related Monte Carlo data.
     *
     *  \param cell Unit cell to store
     *  \param energy Energy of the unit cell
     *  \param mcData Monte Carlo tracked data
     */
    void submitCell(const UnitCell& cell,
                    double energy,
                    const McTrackedData& mcData);

    /** \brief Returns a const reference to collection of Monte Carlo
     *         tracked data.
     * 
     *  \return Const reference to McTrackedData colletion
     */
    const std::vector<McTrackedData>& getMcStorageReference() const;

    /** Copies both solution and Monte Carlo data from source and appends it
     *  to this object's data. Deletes data from source to release memory.
     * 
     *  \param source Source object to copy and delete data from
     */
    void mergeData(McCellTracker& source);

    /** \brief Copies Monte Carlo data from another McCellTracker object into
     *         this one. Data is appended to any existing data in this object.
     *
     *  \param source Object to copy data from
     */
    void copyMcData(const McCellTracker& source);

    /** \brief Deletes tracked cell data, releasing memory.
     */
    void clearData() override;


  protected:
    /** \brief Stores Monte Carlo step data */
    std::vector<McTrackedData> mcData_;

    void writeSolutionLine(int i, std::ostream& stream) const override;

    void writeHeaderLine(std::ostream& stream) const override;

  private:
    // Hide method from base class (has no McTrackedData)
    void submitCell(const UnitCell& cell, double energy) override;
    // Hide method from base class (can not copy McTrackedData)
    void mergeData(CellTracker& source) override;
  };
}



#endif // MCCELLTRACKER_HPP_INCLUDED
