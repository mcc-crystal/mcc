/** \file MonteCarloInterface.hpp
 *  \brief Declares text interface to display Monte Carlo progress and results.
 */

#ifndef MONTECARLOINTERFACE_HPP_INCLUDED
#define MONTECARLOINTERFACE_HPP_INCLUDED

#include "../core/TextInterface.hpp"

#include "MonteCarloRootModule.hpp"



namespace mcc
{
  /** \brief Console text interface to display progress and results of Monte
   *         Carlo simulations.
   */
  class MonteCarloInterface : public TextInterface
  {
  public:
    /** \brief Initializes the object with reference to Monte Carlo root
     *         module.
     * 
     *  \param mcRoot Reference to Monte Carlo root module for data retrieval.
     */
    explicit MonteCarloInterface(MonteCarloRootModule&  mcRoot);

    void initializeInterface() override;

    void updateInterface() override;

  protected:
    /** \brief Reference to Monte Carlo root module to get data from. */
    MonteCarloRootModule& mcRoot_;
  };
}



#endif // MONTECARLOINTERFACE_HPP_INCLUDED
