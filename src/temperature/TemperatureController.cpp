#include "TemperatureController.hpp"



namespace mcc
{
  TemperatureController::TemperatureController(
                                    const GlobalSettings& settings,
                                    const MonteCarloController& mcController) :
                         Module{settings},
                         mcController_{mcController}
  { }
}
