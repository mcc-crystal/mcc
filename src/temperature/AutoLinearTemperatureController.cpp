#include "AutoLinearTemperatureController.hpp"



namespace mcc
{
  AutoLinearTemperatureController::AutoLinearTemperatureController(
                                    const GlobalSettings& settings,
                                    const MonteCarloController& mcController) :
                                   TemperatureController(settings,
                                                         mcController)
  {
    // Initialize from settings
    constBegin_ = globalSettings_.temperature_autoStartConstantSteps;
    constEnd_ = globalSettings_.temperature_autoEndConstantSteps;
    minT_ = globalSettings_.temperature_autoMinTemperature;
    maxT_ = globalSettings_.temperature_autoMaxTemperature;
    nSteps_ = globalSettings_.monteCarlo_nMaxSteps;

    autoCalculateParameters();
  }



  void AutoLinearTemperatureController::Reset()
  { }



  double AutoLinearTemperatureController::getNextTemperature()
  {
    const int step {mcController_.getStepNumber()};
    double temperature;

    if (step <= constBegin_)
      temperature = maxT_;
    else if (nSteps_ - step <= constEnd_)
      temperature = minT_;
    else {
      temperature = slope_ * step + intercept_;

      if (temperature < minT_) {
        return minT_;
      }
    }

    return temperature;
  }



  std::string AutoLinearTemperatureController::getModuleName() const
  {
    return "AutoLinearTemperatureController";
  }



  void AutoLinearTemperatureController::autoCalculateParameters()
  {
    int curveLength {nSteps_ - (constBegin_ + constEnd_)};
    double deltaT {minT_ - maxT_};

    // k = deltaY / deltaX
    slope_ = deltaT / curveLength;
    // n = y - kx
    // point (constBegin, maxT)
    intercept_ = maxT_ - slope_ * constBegin_;
  }
}
