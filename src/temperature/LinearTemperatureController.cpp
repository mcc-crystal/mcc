#include "LinearTemperatureController.hpp"

#include <string>



namespace mcc
{
  LinearTemperatureController::LinearTemperatureController(
                                  const GlobalSettings& settings,
                                  const MonteCarloController& mcController) :
                               TemperatureController(settings, mcController)
  {
    // Initialize from settings
    intercept_ = globalSettings_.temperature_linearIntercept;
    slope_ = globalSettings_.temperature_linearSlope;
    minimumTemperature_ = globalSettings_.temperature_minimumTemperature;
  }



  void LinearTemperatureController::Reset()
  { }



  double LinearTemperatureController::getNextTemperature()
  {
    const int step {mcController_.getStepNumber()};
    double temperature {slope_ * step + intercept_};

    if (temperature < minimumTemperature_) {
      return minimumTemperature_;
    }

    return temperature;
  }



  std::string LinearTemperatureController::getModuleName() const
  {
    return "LinearTemperatureController";
  }
}
