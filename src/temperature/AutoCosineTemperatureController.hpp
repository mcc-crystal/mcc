/** \file AutoCosineTemperatureController.hpp
 *  \brief Declares cosine temperature controller that calculates its
 *         parameters automatically using auto bounds from settings.
 */

#ifndef AUTOCOSINETEMPERATURECONTROLLER_HPP_INCLUDED
#define AUTOCOSINETEMPERATURECONTROLLER_HPP_INCLUDED

#include "TemperatureController.hpp"



namespace mcc
{
  /** \brief Temperature controller using cosine as a temperature function.
   *         Parameters are calculated automatically using temperature auto
   *         bounds from settings.
   *
   * Temperature is calculated using the following function:
   *    T = A * cos(x * S) + V
   * where A is amplitude parameter, x is step (not counting beginning constant
   * temperature steps), S horizontal scaling factor, and V vertical shift
   * parameter.
   *
   */
  class AutoCosineTemperatureController : public TemperatureController
  {
    public:
    /** \brief Initializes base class, copies relevant settings from shared
     *         object and calculates temperature function parameters from
     *         auto bounds.
     * 
     *  \param settings Reference to shared global settings object
     *  \param mcController Reference to parent Monte Carlo controller module
     *                      which owns this object
     */
    AutoCosineTemperatureController(const GlobalSettings& settings,
                                    const MonteCarloController& mcController);

    void Reset() override;

    double getNextTemperature() override;

    std::string getModuleName() const override;


  protected:
    /** \brief Amplitude parameter for cosine function */
    double paramA_;
    /** \brief Horizontal scale (stretch/shrink) parameter for cosine function
     */
    double paramS_;
    /** \brief Vertical shift parameter for cosine function */
    double paramV_;
    /** \brief Minimum temperature from settings */
    double minT_;
    /** \brief Maximum temperature from settings */
    double maxT_;
    /** \brief Number of steps with constant temperature at the beginning of
     *         simulation from settings */
    int constBegin_;
    /** \brief Number of steps with constant temperature at the end of
     *         simulation from settings */
    int constEnd_;
    /** \brief Number of simulation steps from settings */
    int nSteps_;
    /** \brief Number of periods per simulation from settings */
    double nPeriods_;

    /** \brief Calculates linear line parameters from auto parameters supplied
     *         in the settings.
     */
    void autoCalculateParameters();
  };
}



#endif // AUTOCOSINETEMPERATURECONTROLLER_HPP_INCLUDED
