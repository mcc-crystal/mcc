/** \file TemperatureController.hpp
 *  \brief Declares base class for modules that control temperature in Monte
 *         Carlo simulations.
 */

#ifndef TEMPERATURECONTROLLER_HPP_INCLUDED
#define TEMPERATURECONTROLLER_HPP_INCLUDED

#include "../core/Module.hpp"

#include "../monte_carlo/MonteCarloController.hpp"



namespace mcc
{
  // Forward declaration
  class MonteCarloController;



  /** \brief Abstract base class for modules that control temperature in
   *         Monte Carlo simulations.
   */
  class TemperatureController : public Module
  {
  public:
    /** \brief Initializes base class and reference to Monte Carlo controller.
     * 
     *  \param settings Reference to shared global settings object
     *  \param mcController Reference to parent Monte Carlo controller module
     *                      which owns this object
     */
    TemperatureController(const GlobalSettings& settings,
                          const MonteCarloController& mcController);

    /** \brief Returns temperature that should be used for evaluation of the
     *         next Monte Carlo step.
     *
     * \return Temperature
     */
    virtual double getNextTemperature() = 0;

    /** \brief Resets the object so that it is prepared for another Monte
     *         Carlo simulation.
     */
    virtual void Reset() = 0;


  protected:
    /** \brief Reference to controller which owns this object. Used to get
     *         simulation data relevant for temperature determination.
     */
    const MonteCarloController& mcController_;
  };
}



#endif // TEMPERATURECONTROLLER_HPP_INCLUDED
