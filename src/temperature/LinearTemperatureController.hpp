/** \file LinearTemperatureController.hpp
 *  \brief Declares linear temperature controller which needs direct
 *         specification of linear line parameters.
 */

#ifndef LINEARTEMPERATURECONTROLLER_HPP_INCLUDED
#define LINEARTEMPERATURECONTROLLER_HPP_INCLUDED

#include "TemperatureController.hpp"



namespace mcc
{
  /** \brief Temperature controller using linear line to determine temperature.
   *         This version of linear controller needs direct specification of
   *         linear line parameters.
   */
  class LinearTemperatureController : public TemperatureController
  {
  public:
    /** \brief Initializes base class and copies relevant settings from shared
     *         object.
     * 
     *  \param settings Reference to shared global settings object
     *  \param mcController Reference to parent Monte Carlo controller module
     *                      which owns this object
     */
    LinearTemperatureController(const GlobalSettings& settings,
                                const MonteCarloController& mcController);

    void Reset() override;

    double getNextTemperature() override;

    std::string getModuleName() const override;


  protected:
    /** \brief Intercept of linear function from settings */
    double intercept_;
    /** \brief Slope of linear function from settings */
    double slope_;
    /** \brief Minimum temperature settings */
    double minimumTemperature_;
  };
}



#endif // LINEARTEMPERATURECONTROLLER_HPP_INCLUDED
