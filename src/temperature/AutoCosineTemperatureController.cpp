#include "AutoCosineTemperatureController.hpp"

#include <cmath>

// For compatibility with other compilers
#ifndef M_PI
#define M_PI 3.14159265358979323846d
#endif // M_PI



namespace mcc
{
  AutoCosineTemperatureController::AutoCosineTemperatureController(
                                    const GlobalSettings& settings,
                                    const MonteCarloController& mcController) :
                                   TemperatureController{settings,
                                                         mcController}
  {
    // Initialize from settings
    constBegin_ = globalSettings_.temperature_autoStartConstantSteps;
    constEnd_ = globalSettings_.temperature_autoEndConstantSteps;
    minT_ = globalSettings_.temperature_autoMinTemperature;
    maxT_ = globalSettings_.temperature_autoMaxTemperature;
    nSteps_ = globalSettings_.monteCarlo_nMaxSteps;
    nPeriods_ = globalSettings_.temperature_autoPeriodsPerSimulation;

    autoCalculateParameters();
  }



  void AutoCosineTemperatureController::Reset()
  { }



  double AutoCosineTemperatureController::getNextTemperature()
  {
    const int step {mcController_.getStepNumber()};
    double temperature;

    if (step <= constBegin_)
      temperature = maxT_;
    else if (nSteps_ - step <= constEnd_)
      temperature = minT_;
    else {
      double cosine = std::cos((step - constBegin_) * paramS_);
      temperature = paramA_ * cosine + paramV_;

      // Temperature check not necessary here since it's a periodic function
      /*
      if (temperature < minT_) {
        return minT_;
      }
      */
    }

    return temperature;
  }



  std::string AutoCosineTemperatureController::getModuleName() const
  {
    return "AutoCosineTemperatureController";
  }



  void AutoCosineTemperatureController::autoCalculateParameters()
  {
    double deltaT {minT_ - maxT_};
    int areaWidth {nSteps_ - (constBegin_ + constEnd_)};
    double desiredPeriod {areaWidth / nPeriods_};

    paramA_ = std::fabs(deltaT) / 2.0;
    paramS_ = 2.0 * M_PI / desiredPeriod;
    paramV_ = minT_ + paramA_;
  }
}
