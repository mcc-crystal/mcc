/** \file AutoLinearTemperatureController.hpp
 *  \brief Declares linear temperature controller that automatically calculates
 *         linear line parameters.
 */

#ifndef AUTOLINEARTEMPERATURECONTROLLER_HPP_INCLUDED
#define AUTOLINEARTEMPERATURECONTROLLER_HPP_INCLUDED

#include "TemperatureController.hpp"



namespace mcc
{
  /** \brief Linear temperature controller that support automatic parameter
   *         calculation from more human-readable settings. Also supports
   *         constant temperature at beginning and end of simulation.
   */
  class AutoLinearTemperatureController : public TemperatureController
  {
  public:
    /** \brief Initializes base class, copies relevant settings from shared
     *         object and calculates temperature function parameters from
     *         auto bounds.
     * 
     *  \param settings Reference to shared global settings object
     *  \param mcController Reference to parent Monte Carlo controller module
     *                      which owns this object
     */
    AutoLinearTemperatureController(const GlobalSettings& settings,
                                    const MonteCarloController& mcController);

    void Reset() override;

    double getNextTemperature() override;

    std::string getModuleName() const override;


  protected:
    /** \brief Intercept of linear function */
    double intercept_;
    /** \brief Slope of linear function */
    double slope_;
    /** \brief Minimum temperature from settings */
    double minT_;
    /** \brief Maximum temperature from settings */
    double maxT_;
    /** \brief Number of steps with constant temperature at the beginning of
     *         simulation from settings */
    int constBegin_;
    /** \brief Number of steps with constant temperature at the end of
     *         simulation from settings */
    int constEnd_;
    /** \brief Number of simulation steps from settings */
    int nSteps_;

    /** \brief Calculates linear line parameters from auto parameters supplied
     *         in the settings.
     *
     */
    void autoCalculateParameters();
  };
}



#endif // AUTOLINEARTEMPERATURECONTROLLER_HPP_INCLUDED
